(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('ResultDialogController', ResultDialogController);

    ResultDialogController.$inject = ['$scope', '$rootScope', '$stateParams', '$uibModalInstance','Fixture','$http'];

    function ResultDialogController ($scope, $rootScope, $stateParams, $uibModalInstance,Fixture,$http) {
        var vm = this;

        $scope.resultData = [];

        /***
            $http.get() used to temporary load data from the JSON file

            This code must be removed once the Resulting page is finished and be replaced by code:
                $scope.resultData = fixtureResult;
                Where 'fixtureResult' is an object to be 'resolve' in the state.js
         ***/
        $http.get('app/entities/dummy_data/resulting.json').success(function (tableData) {
            $scope.resultData = tableData[0];
            console.log("JSON LOADED");
            console.log($scope.resultData);
        });


        var array = [];
        var dataChanged = [];
        vm.load = load;
        vm.load();
        
        function load() { 
            
            vm.Status = fixtureStatus;
            vm.oddFeed = oddFeed;
            vm.dataFeed = dataFeed;

            var data = ($rootScope.listScope.length > 0) ? $rootScope.listScope:$rootScope.changedData;
            if($rootScope.changedData.length == 0){
                angular.forEach($rootScope.listScope, function (item) { 
                    if (item.Selected) {
                        array.push(item);
                    }
                });
            }else {
                angular.forEach($rootScope.changedData, function (item) { 
                    dataChanged.push(item); 
                });
                angular.forEach($rootScope.listScope, function (item, key) {  
                    angular.forEach($rootScope.changedData, function (changedItem) {  
                        if(item.id==changedItem.id) {
                            array.push(item); 
                        }
                    });
                }); 
                vm.changedData=dataChanged;
            }


        }

        var onSaveSuccess = function (result) {
            $scope.$emit('fixtureApp:fixture  Update', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () { 
            if (array.length != 0) {
                angular.forEach(array, function (item,key) { 
                    if($rootScope.changedData.length == 0){
                        if (item.Selected) {
                            if(vm.fixtures.odd_feed == null && vm.fixtures.data_feed == null ){
                                item.status      = vm.fixtures.status
                            }
                            else if(vm.fixtures.odd_feed == null && vm.fixtures.status == null){
                                item.data_feed   = vm.fixtures.data_feed;
                            }
                            else if(vm.fixtures.data_feed == null && vm.fixtures.status  == null){
                                item.odd_feed   = vm.fixtures.odd_feed;
                            }

                            else if(vm.fixtures.odd_feed == null){
                                item.data_feed   = vm.fixtures.data_feed;
                                item.status      = vm.fixtures.status
                            } else if (vm.fixtures.data_feed == null){
                                item.odd_feed    = vm.fixtures.odd_feed;
                                item.status      = vm.fixtures.status
                            } else if (vm.fixtures.status == null){
                                item.odd_feed    = vm.fixtures.odd_feed;
                                item.data_feed   = vm.fixtures.data_feed;
                            } else {
                                item.odd_feed    = vm.fixtures.odd_feed;
                                item.data_feed   = vm.fixtures.data_feed;
                                item.status      = vm.fixtures.status
                            }
                        }
                        $uibModalInstance.dismiss('cancel');
                    }else { 
                        $uibModalInstance.dismiss('cancel');
                    }
                });
            } else {
                alert("Please select items to update first.");
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };



    }

})();
