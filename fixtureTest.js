'use strict';

describe('account', function () {

    beforeAll(function () {
        browser.get('/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));

    });

    //Fixture Create Tests
    it('should login successfully and open create fixture page', function () {
        element(by.id('account-menu')).click();
        element(by.css('[ui-sref="login"]')).click();

        expect(element.all(by.css('h1')).first().getText()).toMatch(/Sign in/);

        element(by.model('username')).clear().sendKeys('dummy');
        element(by.model('password')).clear().sendKeys('123456q!');
        element(by.css('button[type=submit]')).click();

        expect(element(by.css('.alert-success')).getText()).toMatch(/You are logged in as user "dummy"/);

        element(by.css('span[translate="global.menu.fixture.main"]')).click();
        element(by.css('span[translate="global.menu.fixture.create"]')).click();

        expect(element(by.id('myFixtureLabel')).getText()).toMatch(/Create Fixture/);

        //Checks if fields are disabled
        expect(element(by.id('field_sports')).isEnabled()).toBe(true);
        expect(element(by.id('field_category')).isEnabled()).toBe(false);
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);


    });

    afterAll(function () {
        element(by.id('account-menu')).click();
        element(by.id('logout')).click();
    });
});
