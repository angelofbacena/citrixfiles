'use strict';

describe('Fixture Listing Tests', function () {

    beforeAll(function () {
        browser.get('/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));

    });


    //Fixture Listing Tests
    it('should open the Fixture Listing page', function () {
        element(by.id('account-menu')).click();
        element(by.css('[ui-sref="login"]')).click();

        expect(element.all(by.css('h1')).first().getText()).toMatch(/Sign in/);

        element(by.model('username')).clear().sendKeys('dummy');
        element(by.model('password')).clear().sendKeys('123456q!');
        element(by.css('button[type=submit]')).click();

        expect(element(by.css('.alert-success')).getText()).toMatch(/You are logged in as user "dummy"/);

        element(by.css('span[translate="global.menu.fixture.main"]')).click();
        element(by.css('span[translate="global.menu.fixture.listing"]')).click().then(function () {
            expect(element(by.css('h2')).getText()).toMatch(/Fixture Listings/);

        });
        
    });

    it('should filter the Fixture listing by Status', function () {
        element(by.id('filter_status')).element(by.cssContainingText('option', 'Active')).click();
        element(by.css('button[type=submit]')).click();

        expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.homeTeam.description')).getText()).toMatch(/1Hawaii/);
        expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.awayTeam.description')).getText()).toMatch(/1Ohio/);
        expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.status')).getText()).toMatch(/Active/);
        expect(element.all(by.repeater('fixture in vm.fixtures')).count()).toEqual(7);

    });

    it('should filter the Fixture listing by Date Range', function () {
        element(by.model('vm.filter.datePicker')).clear().sendKeys('2016-06-01 - 2016-06-30');
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.homeTeam.description')).getText()).toMatch(/1Hawaii/);
            expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.awayTeam.description')).getText()).toMatch(/1Ohio/)
            expect(element.all(by.repeater('fixture in vm.fixtures')).count()).toEqual(9);

        });
    });

    it('should filter the Fixture listing by Sport', function () {
        element(by.id('field_sports')).element(by.cssContainingText('option', 'Football')).click();
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.homeTeam.description')).getText()).toMatch(/1Hawaii/);
            expect(element(by.repeater('fixture in vm.fixtures').row(0).column('fixture.awayTeam.description')).getText()).toMatch(/1Ohio/)
            expect(element.all(by.repeater('fixture in vm.fixtures')).count()).toEqual(9);

        });
    });

    it('should filter the Fixture listing by Tournament SINGLE SELECT', function () {
        element(by.css('button[ng-class="settings.buttonClasses"]')).click().then(function () {
            element(by.cssContainingText('a', ' SQ1')).click();
            element(by.css('button[ng-class="settings.buttonClasses"]')).click();
        });
        
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element.all(by.repeater('fixture in vm.fixtures')).count()).toEqual(3);

        });

    });

    it('should filter the Fixture listing by Tournament MULTI SELECT', function () {
         element(by.css('button[ng-class="settings.buttonClasses"]')).click().then(function () {
            element(by.css('a[data-ng-click="selectAll()"]')).click();
            element(by.css('button[ng-class="settings.buttonClasses"]')).click();
         });
       
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element.all(by.repeater('fixture in vm.fixtures')).count()).toEqual(7);

        });
    });

    afterAll(function () {
        element(by.id('account-menu')).click();
        element(by.id('logout')).click();
    });
});
