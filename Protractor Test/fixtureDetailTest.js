'use strict';

describe('Fixture Detail Tests', function () {
	var today, testDate;
	var mm, dd, yyyy, HH, MM;

    beforeAll(function () {
        browser.get('/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));
		
		today = new Date();
		dd = today.getDate();
		mm = today.getMonth() + 1;
		yyyy = today.getFullYear();
		HH = today.getHours();
		MM = today.getSeconds();
		
		if(dd < 10) {
			dd = '0' + dd;
		}
		
		if(mm < 10) {
			mm = '0' + mm;
		}
		
		if(HH < 10) {
			HH = '0' + HH;
		}
		
		if(MM < 10) {
			MM = '0' + MM;
		}

		testDate = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM;

    });

    //Fixture Detail Tests
    it('should login successfully and open a detail page', function () {
        element(by.id('account-menu')).click();
        element(by.css('[ui-sref="login"]')).click();

        expect(element.all(by.css('h1')).first().getText()).toMatch(/Sign in/);

        element(by.model('username')).clear().sendKeys('dummy');
        element(by.model('password')).clear().sendKeys('123456q!');
        element(by.css('button[type=submit]')).click();

        expect(element(by.css('.alert-success')).getText()).toMatch(/You are logged in as user "dummy"/);

        element(by.css('span[translate="global.menu.fixture.main"]')).click();
        element(by.css('span[translate="global.menu.fixture.listing"]')).click().then(function () {
            expect(element(by.css('h2')).getText()).toMatch(/Fixture Listings/);
            
        });

        element(by.id('filter_status')).element(by.cssContainingText('option', 'Active')).click();
        element(by.css('button[type=submit]')).click().then(function () {
            element(by.cssContainingText('a[href="#/fixture/134fdsg343dsft"]', '2')).click().then(function () {
             expect(element(by.css('h2')).getText()).toMatch(/1Hawaii vs 1Ohio/);

            });
        });


    });
	
	it('should display Markets and Odds', function () {
		element(by.buttonText('Expand All')).click().then(function () {
			expect(element.all(by.repeater('(key, market) in fixture.markets')).count()).toEqual(2);
			
			expect(element(by.repeater('(key, market) in fixture.markets').row(0).column('market.status')).isDisplayed()).toBe(true);
			expect(element(by.repeater('(key, market) in fixture.markets').row(0).column('market.status')).getText()).toMatch(/Started/);
			
			expect(element(by.repeater('(key, market) in fixture.markets').row(1).column('market.status')).isDisplayed()).toBe(true);
			expect(element(by.repeater('(key, market) in fixture.markets').row(1).column('market.status')).getText()).toMatch(/Blocked/);
		});
		
		var Odds1 = element(by.repeater('(key, market) in fixture.markets').row(0)).all(by.repeater('marketCode in market.codes'));
		var Odds2 = element(by.repeater('(key, market) in fixture.markets').row(1)).all(by.repeater('marketCode in market.codes'));
		
		Odds1.then(function (arr) {
			expect(arr[0].isDisplayed()).toBeTruthy();
			expect(arr[0].getText()).toMatch(/Lost/);
		});
		
		Odds2.then(function (arr) {
			expect(arr[0].isDisplayed()).toBeTruthy();
			expect(arr[0].getText()).toMatch(/Won/);
		})
	});
	
	
	/* it('should Edit the fixture details', function () {
		element(by.buttonText('Edit Mode')).click();
	}) */



    afterAll(function () {
        element(by.id('account-menu')).click();
        element(by.id('logout')).click();
    });
});
