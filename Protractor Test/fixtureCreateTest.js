'use strict';

describe('Fixture Create Tests', function () {
	var today, fixtureKickoff;
	var dd, mm, yyyy, HH, MM;
	var hasClass = function (element, cls) {
		return element.getAttribute('class').then(function (classes) {
			return classes.split(' ').indexOf(cls) !== -1;
		});
	};

    beforeAll(function () {
        browser.get('/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));
		today = new Date();
		dd = today.getDate();
		mm = today.getMonth() + 1;
		yyyy = today.getFullYear();
		HH = today.getHours();
		MM = today.getSeconds();
		
		if(dd < 10) {
			dd = '0' + dd;
		}
		
		if(mm < 10) {
			mm = '0' + mm;
		}
		
		if(HH < 10) {
			HH = '0' + HH;
		}
		
		if(MM < 10) {
			MM = '0' + MM;
		}

		fixtureKickoff = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM;
		
    });

    //Fixture Create Tests
    it('should login successfully and open create fixture page', function () {
        element(by.id('account-menu')).click();
        element(by.css('[ui-sref="login"]')).click();

        expect(element.all(by.css('h1')).first().getText()).toMatch(/Sign in/);

        element(by.model('username')).clear().sendKeys('dummy');
        element(by.model('password')).clear().sendKeys('123456q!');
        element(by.css('button[type=submit]')).click();

        expect(element(by.css('.alert-success')).getText()).toMatch(/You are logged in as user "dummy"/);

        element(by.css('span[translate="global.menu.fixture.main"]')).click();
        element(by.css('span[translate="global.menu.fixture.create"]')).click();

        expect(element(by.id('myFixtureLabel')).getText()).toMatch(/Create Fixture/);

        //Checks if fields are disabled
        expect(element(by.id('field_sports')).isEnabled()).toBe(true);
        expect(element(by.id('field_category')).isEnabled()).toBe(false);
        expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);


    });

    it('should save a Fixture', function () {
        element(by.id('field_sports')).element(by.cssContainingText('option', 'Volleyball')).click();
        element(by.id('field_category')).element(by.cssContainingText('option', 'France')).click();
        element(by.id('field_tournament')).element(by.cssContainingText('option', 'Ligue A')).click();
        element(by.id('field_template')).element(by.cssContainingText('option', 'Template Four Test')).click()
		element(by.id('field_kickOff')).clear().sendKeys(fixtureKickoff);
		console.log(fixtureKickoff);
        element(by.id('field_home')).sendKeys('Paris Volley').then(function () {
            element(by.css('a[tabindex="-1"]')).click();
        });
        element(by.id('field_away')).sendKeys('Paris Handball').then(function () {
            element(by.css('a[tabindex="-1"]')).click();
        });
        element(by.css('input[type="checkbox"]')).click();
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
        
        });
        
    });

    it('should not save a duplicate fixture', function () {
		element(by.id('field_sports')).element(by.cssContainingText('option', 'Volleyball')).click();
        element(by.id('field_category')).element(by.cssContainingText('option', 'France')).click();
        element(by.id('field_tournament')).element(by.cssContainingText('option', 'Ligue A')).click();
        element(by.id('field_template')).element(by.cssContainingText('option', 'Template Four Test')).click();
        element(by.id('field_kickOff')).clear().sendKeys(fixtureKickoff);
        element(by.id('field_home')).sendKeys('Paris Volley').then(function () {
            element(by.css('a[tabindex="-1"]')).click();
        });
        element(by.id('field_away')).sendKeys('Paris Handball').then(function () {
            element(by.css('a[tabindex="-1"]')).click();
        });
        element(by.css('input[type="checkbox"]')).click();
        element(by.css('button[type=submit]')).click().then(function () {
            expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
			expect(hasClass(element(by.id('field_home')), 'ng-invalid')).toBe(true);
			expect(hasClass(element(by.id('field_home')), 'ng-dirty')).toBe(true);
			expect(hasClass(element(by.id('field_away')), 'ng-invalid')).toBe(true);
			expect(hasClass(element(by.id('field_away')), 'ng-dirty')).toBe(true);
			
        });
	});
	

    afterAll(function () {
        element(by.id('account-menu')).click();
        element(by.id('logout')).click();
    });
});
