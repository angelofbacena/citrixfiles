'use strict';

describe('Controller Tests', function() {

    describe('Fixture Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockFixture;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockFixture = jasmine.createSpy('MockFixture');
			controller = $controler('FixtureCreatorController', {$scope : scope});
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Fixture': MockFixture
            };
            createController = function() {
                $injector.get('$controller')("FixtureCreatorController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'icontentApp:fixtureUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
		
		describe('FixtureCreatorController testing', function() {
			it('should fetch sports', function() {
				expect(controller.lexiconSports.length).toBe(26);
			});
			
			it('should fetch categories', function() {
				controller.fetchCategories(441); // SportID 441 - Football
				
				expect(controller.lexiconSportsCategories.length).toBe(92);
			});
			
			it('should fetch tournaments', function() {
				controller.fetchTournaments(2788); // CategoryID 2788 - Sweden
				
				expect(controller.tournamentsList.length).toBe(10);
			});
			
			it('should have test template list', function() {
				expect(controller.templateList.lenghtth).toBe(2);
			});
			
			it('should search team', function() {	
				expect(controller.searchTeam('san').length).toBe(706);
			});
		});
    });

});
