(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('AntepostDetailsController', AntepostDetailsController);

    AntepostDetailsController.$inject = ['$state','$scope', '$rootScope', '$stateParams', 'Fixture', 'tournamentTemplates', 
    'marketStatus', 'codeStatus', 'fixtureStatus', 'fixtureFeedTypes', 'fixtureMarketTypes'];

    function AntepostDetailsController ($state, $scope, $rootScope, $stateParams, Fixture, tournamentTemplates, marketStatus, 
    codeStatus, fixtureStatus, fixtureFeedTypes, fixtureMarketTypes) {

        var vm = this;
        vm.fixture = {};
        vm.currentAntepost = {};
        vm.newAntepost = {};
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.kickOff = false;
        vm.isEditMode = false;

        vm.loadFixture = loadFixture;
        vm.loadFixture();
        $scope.mindate = new Date();  
        vm.oneAtATime = true; 
 

        if($rootScope.listScope !=null) {
            $rootScope.listScope = null;
            vm.showTable = false;
            $state.reload();
        }

        vm.marketStatus = marketStatus;
        vm.marketType = null;
        vm.codeStatus = codeStatus;
        vm.fixtureStatus = fixtureStatus;
        vm.fixtureFeedTypes = fixtureFeedTypes;
        vm.fixtureMarketTypes = fixtureMarketTypes;
        
        vm.price_derivative = false;



        var toUpdateArray = [];
        vm.toggleEditMode = function() {
            if(vm.isEditMode == false) {
                vm.isEditMode = true;
            }
            else if (vm.isEditMode == true) {
                vm.isEditMode = false;
                vm.loadFixture();
            }

            vm.kickOff_new = vm.fixture.kickOff;
            vm.dataFd_new = vm.dataFd;
            vm.OddFd_new = vm.OddFd;
            vm.status_new=vm.status;
        };

        vm.changeMarketType = function(marketType){
            $rootScope.marketType = marketType;
        }

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };

        function loadFixture() {
            Fixture.getDetail(function(data) {
 
                if(data.id == $stateParams.id)
                {
                    vm.fixture = data;

                    $rootScope.fixture=vm.fixture;
                    vm.market=[];
                    var fixtureMarkets=vm.fixture.markets;
                    for(var i=0; i<fixtureMarkets.length; i++) { 
                        fixtureMarkets[i]['accordStatus']=false;
                        vm.market.push(fixtureMarkets[i]);
                    } 
                }

            });
        } 
        
        var onUpdateError = function () {
            vm.isSaving = false;
        };

        var onUpdateSuccess = function () {
            vm.isSaving = false;
            vm.isEditMode = false;
        }; 
  
        vm.toggleOpenAll = function (openStatus) { 
            for(var i=0; i<$rootScope.fixture.markets.length; i++) {  
                $rootScope.fixture.markets[i]['accordStatus']=openStatus; 
                $rootScope.fixture.markets[i]['accordCodeStatus']=openStatus; 
            }
        }

        //saving of fixture object
        vm.updateFixture = function () {
            vm.isSaving = true;
            Fixture.update({id:vm.fixture.id},{data:vm.fixture,price_derivative:vm.price_derivative}, onUpdateSuccess, onUpdateError);
        };

        vm.reCalcOverround = function(market) {
            var overround = 0;
            var sum = 0;
            for (var i = market.codes.length - 1; i>= 0; i--) {
                sum += 1/market.codes[i].odd;
            };
            overround = sum * 100
            overround = Math.round(overround * 100) / 100; //round to two decimal places
            market.overround = overround;
        }

        vm.searchMarket = function(item){
            if($scope.catFilter == undefined){
                return true;
            }else{
                if(item.status == $scope.catFilter){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }
})();
