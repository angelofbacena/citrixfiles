(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('antepost-details', {
            parent: 'fixture',
            url: '/antepost/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Fixture Details'
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/fixture/antepost/details/antepost_details.html',
                    controller: 'AntepostDetailsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                marketStatus: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchMarketStatus({}).$promise.$$state;
                    }
                ],
                codeStatus: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchCodeStatus({}).$promise.$$state;
                    }
                ],
                fixtureStatus: ['Fixture',
                    function(Fixture) {
                        var data=Fixture.fetchFixtureStatus({}).$promise.$$state;
                        return data;
                    }
                ], 
                fixtureFeedTypes: ['Fixture',
                    function(Fixture) {
                        var data=Fixture.fetchFixtureFeedTypes({}).$promise.$$state;
                        return data;
                    }
                ], 
                tournamentTemplates: ['TournamentTemplate',
                    function(TournamentTemplate) {
                        return TournamentTemplate.get({});
                    }
                ], 
                fixtureMarketTypes: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchMarketType({}).$promise.$$state;
                    }
                ]
            }
        })
        /*.state('update-template-modal', {
            parent: 'fixture-details',
            url: '/update-template',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/entities/fixture/modal/details_modal/update_template_modal.html',
                    controller: 'FixtureDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        lexiconTemplates: ['Fixture',
                            function(Fixture) {
                                return Fixture.fetchTemplates({});
                            }
                        ],
                    }
                }).result.then(function() {
                    $state.go('fixture-details', null, { reload: false });
                }, function() {
                    $state.go('fixture-details');
                });
            }]
        })
        .state('add-market-modal', {
            parent: 'fixture-details',
            url: '/add-market',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/entities/fixture/modal/details_modal/add_market_modal.html',
                    controller: 'FixtureDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg' 
                }).result.then(function() {
                    $state.go('fixture-details', null, { reload: false });
                }, function() {
                    $state.go('fixture-details');
                });
            }]
        })
        .state('market-info-modal', {
            parent: 'fixture-details',
            url: '/market-info',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/entities/fixture/modal/details_modal/market_info_modal.html',
                    controller: 'FixtureDetailsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg' 
                }).result.then(function() {
                    $state.go('fixture-details', null, { reload: false });
                }, function() {
                    $state.go('fixture-details');
                });
            }]
        }); */
    }

})();
