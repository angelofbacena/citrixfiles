package com.intralot.test;

import static org.junit.Assert.assertNotNull;

import java.time.ZoneId;

import org.junit.Test;

public class DateTimeTest {
	
	@Test
	public void testTimezone() {
		ZoneId zid = ZoneId.of("Europe/Sofia");
		assertNotNull(zid);
	}

}
