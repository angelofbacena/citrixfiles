package com.intralot.test.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.intralot.Application;
import com.intralot.config.Constants;
import com.intralot.service.LexiconService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(Constants.SPRING_PROFILE_TEST)
public class TranslationServiceTest {
	
	@Autowired
	LexiconService lexiconService;
	
	@Test
	public void testGetSport() {
		assertNotNull(lexiconService.getSportByProviderId(1, "betradar"));
		
	}

}
