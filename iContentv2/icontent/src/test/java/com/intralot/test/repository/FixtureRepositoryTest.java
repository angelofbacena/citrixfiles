package com.intralot.test.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.ZonedDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.intralot.Application;
import com.intralot.domain.Fixture;
import com.intralot.repository.FixtureRepository;
import com.intralot.test.TestUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FixtureRepositoryTest {
	

    @Inject
    private FixtureRepository fixtureRepository;
   

    @PostConstruct
    public void setup() {
 
    }

    @Before
    public void initTest() {
        fixtureRepository.deleteAll();
       
    }
    
    @Test
    public void testGetByKickOffAndHomeTeamIdAndAwayTeamId() {
    	Fixture f = TestUtil.createDummyFixture(fixtureRepository);
    	List<Fixture> fixtures =  fixtureRepository.findByKickOffAndHomeTeamIdAndAwayTeamId(f.getKickOff(),f.getHomeTeam().getTeamId(), f.getAwayTeam().getTeamId());
    	assertNotNull(fixtures);
    	assertEquals(1,fixtures.size());
    	
    }
    
    @Test
    public void testGetNonExistingNotNullList() {
    	Fixture f = TestUtil.createDummyFixture(null);
    	List<Fixture> fixtures =  fixtureRepository.findByKickOffAndHomeTeamIdAndAwayTeamId(f.getKickOff(),f.getHomeTeam().getTeamId(), f.getAwayTeam().getTeamId());
    	assertNotNull(fixtures);
    }
    
    @Test
    public void testGetByDateRangeAndTournamentId(){
    	Fixture f = TestUtil.createDummyFixture(null);
    	f.setKickOff(ZonedDateTime.now());
    	fixtureRepository.save(f);
    	Integer[] tids = new Integer[1];
    	tids[0] = f.getTournament().getTournamentId();
    	Page<Fixture> fixtures = fixtureRepository.findByKickOffRangeAndTournamentId(f.getKickOff().minusDays(1), f.getKickOff().plusDays(1), tids, new PageRequest(0, 2));
    	assertEquals(1, fixtures.getContent().size());
    	
    	fixtures = fixtureRepository.findByKickOffRangeAndTournamentId(f.getKickOff().plusDays(1), f.getKickOff().minusDays(1), tids, new PageRequest(0, 2));
    	assertEquals(0, fixtures.getContent().size() );
    	
    }
    

}
