package com.intralot.test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.intralot.domain.Fixture;
import com.intralot.domain.User;
import com.intralot.domain.enumeration.EventStatus;
import com.intralot.domain.util.JSR310DateTimeSerializer;
import com.intralot.domain.util.JSR310LocalDateDeserializer;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;

public class TestUtil {

	public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
			.withZone(ZoneId.of("Z"));

	public static final ZonedDateTime DEFAULT_ACTIVATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
			ZoneId.systemDefault());
	public static final ZonedDateTime UPDATED_ACTIVATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
	public static final String DEFAULT_ACTIVATION_DATE_STR = dateTimeFormatter.format(DEFAULT_ACTIVATION_DATE);

	public static final ZonedDateTime DEFAULT_KICK_OFF = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
			ZoneId.systemDefault());
	public static final ZonedDateTime UPDATED_KICK_OFF = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
	public static final String DEFAULT_KICK_OFF_STR = dateTimeFormatter.format(DEFAULT_KICK_OFF);

	public static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
			ZoneId.systemDefault());
	public static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
	public static final String DEFAULT_END_DATE_STR = dateTimeFormatter.format(DEFAULT_END_DATE);

	public static final EventStatus DEFAULT_STATUS = EventStatus.inactive;
	public static final EventStatus UPDATED_STATUS = EventStatus.partiallyPriced;
	
	public static final int DEFAULT_HOME_TEAM_ID = 1;
	public static final int DEFAULT_AWAY_TEAM_ID = 2;
	
	
	
	

	/** MediaType for JSON UTF8 */
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	/**
	 * Convert an object to JSON byte array.
	 *
	 * @param object
	 *            the object to convert
	 * @return the JSON byte array
	 * @throws IOException 
	 */
	public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		JavaTimeModule module = new JavaTimeModule();
		module.addSerializer(OffsetDateTime.class, JSR310DateTimeSerializer.INSTANCE);
		module.addSerializer(ZonedDateTime.class, JSR310DateTimeSerializer.INSTANCE);
		module.addSerializer(LocalDateTime.class, JSR310DateTimeSerializer.INSTANCE);
		module.addSerializer(Instant.class, JSR310DateTimeSerializer.INSTANCE);
		module.addDeserializer(LocalDate.class, JSR310LocalDateDeserializer.INSTANCE);
		mapper.registerModule(module);

		return mapper.writeValueAsBytes(object);
	}

	/**
	 * Create a byte array with a specific size filled with specified data.
	 *
	 * @param size
	 *            the size of the byte array
	 * @param data
	 *            the data to put in the byte array
	 */
	public static byte[] createByteArray(int size, String data) {
		byte[] byteArray = new byte[size];
		for (int i = 0; i < size; i++) {
			byteArray[i] = Byte.parseByte(data, 2);
		}
		return byteArray;
	}

	
	public static User createDummyUser(MongoRepository<User, String> repository) {
		User u = new User();
		u.setLogin("alex");
		u.setEmail("charos@intralot.com");
		u.setPassword("$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG");
		u.setFirstName("alex");
		u.setLastName("charos");
		u.setActivated(true);
		if (repository != null) {
			repository.save(u);
		}
		return u;
	}

	public static Fixture createDummyFixture(MongoRepository<Fixture, String> repository) {
		Fixture fixture;
		fixture = new Fixture();
		fixture.setActivationDate(TestUtil.DEFAULT_ACTIVATION_DATE);
		fixture.setKickOff(TestUtil.DEFAULT_KICK_OFF);
		fixture.setEndDate(TestUtil.DEFAULT_END_DATE);
		fixture.setStatus(TestUtil.DEFAULT_STATUS);
		
		Tournament trnt = new Tournament();
		trnt.setTournamentId(123);
		
		fixture.setTournament(trnt);
		
		Team ht = new Team();
		ht.setTeamId(DEFAULT_HOME_TEAM_ID);
		
		Team at = new Team();
		at.setTeamId(DEFAULT_AWAY_TEAM_ID);
		
		fixture.setHomeTeam(ht);
		fixture.setAwayTeam(at);
		
		if (repository != null) {
			repository.save(fixture);
		}
		return fixture;
	}

}
