package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.Subscriber;
import com.intralot.repository.SubscriberRepository;
import com.intralot.service.SubscriberService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.SubscriberDTO;
import com.intralot.web.rest.mapper.SubscriberMapper;


/**
 * Test class for the SubscriberResource REST controller.
 *
 * @see SubscriberResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SubscriberControllerIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";
    private static final String DEFAULT_ENDPOINT = "AAAAA";
    private static final String UPDATED_ENDPOINT = "BBBBB";

    @Inject
    private SubscriberRepository subscriberRepository;

    @Inject
    private SubscriberMapper subscriberMapper;

    @Inject
    private SubscriberService subscriberService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSubscriberMockMvc;

    private Subscriber subscriber;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SubscriberResource subscriberResource = new SubscriberResource();
        ReflectionTestUtils.setField(subscriberResource, "subscriberService", subscriberService);
        ReflectionTestUtils.setField(subscriberResource, "subscriberMapper", subscriberMapper);
        this.restSubscriberMockMvc = MockMvcBuilders.standaloneSetup(subscriberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        subscriberRepository.deleteAll();
        subscriber = new Subscriber();
        subscriber.setDescription(DEFAULT_DESCRIPTION);
        subscriber.setEndpoint(DEFAULT_ENDPOINT);
    }

    @Test
    public void createSubscriber() throws Exception {
        int databaseSizeBeforeCreate = subscriberRepository.findAll().size();

        // Create the Subscriber
        SubscriberDTO subscriberDTO = subscriberMapper.subscriberToSubscriberDTO(subscriber);

        restSubscriberMockMvc.perform(post("/api/subscribers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subscriberDTO)))
                .andExpect(status().isCreated());

        // Validate the Subscriber in the database
        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).hasSize(databaseSizeBeforeCreate + 1);
        Subscriber testSubscriber = subscribers.get(subscribers.size() - 1);
        assertThat(testSubscriber.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSubscriber.getEndpoint()).isEqualTo(DEFAULT_ENDPOINT);
    }

    @Test
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = subscriberRepository.findAll().size();
        // set the field null
        subscriber.setDescription(null);

        // Create the Subscriber, which fails.
        SubscriberDTO subscriberDTO = subscriberMapper.subscriberToSubscriberDTO(subscriber);

        restSubscriberMockMvc.perform(post("/api/subscribers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subscriberDTO)))
                .andExpect(status().isBadRequest());

        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkEndpointIsRequired() throws Exception {
        int databaseSizeBeforeTest = subscriberRepository.findAll().size();
        // set the field null
        subscriber.setEndpoint(null);

        // Create the Subscriber, which fails.
        SubscriberDTO subscriberDTO = subscriberMapper.subscriberToSubscriberDTO(subscriber);

        restSubscriberMockMvc.perform(post("/api/subscribers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subscriberDTO)))
                .andExpect(status().isBadRequest());

        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllSubscribers() throws Exception {
        // Initialize the database
        subscriberRepository.save(subscriber);

        // Get all the subscribers
        restSubscriberMockMvc.perform(get("/api/subscribers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(subscriber.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].endpoint").value(hasItem(DEFAULT_ENDPOINT.toString())));
    }

    @Test
    public void getSubscriber() throws Exception {
        // Initialize the database
        subscriberRepository.save(subscriber);

        // Get the subscriber
        restSubscriberMockMvc.perform(get("/api/subscribers/{id}", subscriber.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(subscriber.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.endpoint").value(DEFAULT_ENDPOINT.toString()));
    }

    @Test
    public void getNonExistingSubscriber() throws Exception {
        // Get the subscriber
        restSubscriberMockMvc.perform(get("/api/subscribers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateSubscriber() throws Exception {
        // Initialize the database
        subscriberRepository.save(subscriber);

		int databaseSizeBeforeUpdate = subscriberRepository.findAll().size();

        // Update the subscriber
        subscriber.setDescription(UPDATED_DESCRIPTION);
        subscriber.setEndpoint(UPDATED_ENDPOINT);
        SubscriberDTO subscriberDTO = subscriberMapper.subscriberToSubscriberDTO(subscriber);

        restSubscriberMockMvc.perform(put("/api/subscribers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subscriberDTO)))
                .andExpect(status().isOk());

        // Validate the Subscriber in the database
        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).hasSize(databaseSizeBeforeUpdate);
        Subscriber testSubscriber = subscribers.get(subscribers.size() - 1);
        assertThat(testSubscriber.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSubscriber.getEndpoint()).isEqualTo(UPDATED_ENDPOINT);
    }

    @Test
    public void deleteSubscriber() throws Exception {
        // Initialize the database
        subscriberRepository.save(subscriber);

		int databaseSizeBeforeDelete = subscriberRepository.findAll().size();

        // Get the subscriber
        restSubscriberMockMvc.perform(delete("/api/subscribers/{id}", subscriber.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Subscriber> subscribers = subscriberRepository.findAll();
        assertThat(subscribers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
