package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.MarketType;
import com.intralot.domain.enumeration.DerivativeType;
import com.intralot.repository.MarketTypeRepository;
import com.intralot.service.MarketTypeService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.MarketTypeDTO;
import com.intralot.web.rest.mapper.MarketTypeMapper;

/**
 * Test class for the MarketTypeResource REST controller.
 *
 * @see MarketTypeController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MarketTypeResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Boolean DEFAULT_CANCELLED_BY_CODES = false;
    private static final Boolean UPDATED_CANCELLED_BY_CODES = true;

    private static final Boolean DEFAULT_BASIC_DISPLAY = false;
    private static final Boolean UPDATED_BASIC_DISPLAY = true;

    private static final Boolean DEFAULT_MASTER_SELECTION = false;
    private static final Boolean UPDATED_MASTER_SELECTION = true;
    
    private static final DerivativeType DEFAULT_DERIVATIVE_TYPE = DerivativeType.derivativeTable;
    private static final DerivativeType UPDATED_DERIVATIVE_TYPE = DerivativeType.derivativeAlgorithm;

    @Inject
    private MarketTypeRepository marketTypeRepository;

    @Inject
    private MarketTypeMapper marketTypeMapper;

    @Inject
    private MarketTypeService marketTypeService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restMarketTypeMockMvc;

    private MarketType marketType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MarketTypeController marketTypeResource = new MarketTypeController();
        ReflectionTestUtils.setField(marketTypeResource, "marketTypeService", marketTypeService);
        ReflectionTestUtils.setField(marketTypeResource, "marketTypeMapper", marketTypeMapper);
        this.restMarketTypeMockMvc = MockMvcBuilders.standaloneSetup(marketTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        marketTypeRepository.deleteAll();
        marketType = new MarketType();
        marketType.setDescription(DEFAULT_DESCRIPTION);
        marketType.setCancelledByCodes(DEFAULT_CANCELLED_BY_CODES);
        marketType.setBasicDisplay(DEFAULT_BASIC_DISPLAY);
        marketType.setMasterSelection(DEFAULT_MASTER_SELECTION);
        marketType.setDerivativeType(DEFAULT_DERIVATIVE_TYPE);
    }

    @Test
    public void createMarketType() throws Exception {
        int databaseSizeBeforeCreate = marketTypeRepository.findAll().size();

        // Create the MarketType
        MarketTypeDTO marketTypeDTO = marketTypeMapper.marketTypeToMarketTypeDTO(marketType);

        restMarketTypeMockMvc.perform(post("/api/marketTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(marketTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the MarketType in the database
        List<MarketType> marketTypes = marketTypeRepository.findAll();
        assertThat(marketTypes).hasSize(databaseSizeBeforeCreate + 1);
        MarketType testMarketType = marketTypes.get(marketTypes.size() - 1);
        assertThat(testMarketType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testMarketType.getCancelledByCodes()).isEqualTo(DEFAULT_CANCELLED_BY_CODES);
        assertThat(testMarketType.getBasicDisplay()).isEqualTo(DEFAULT_BASIC_DISPLAY);
        assertThat(testMarketType.getMasterSelection()).isEqualTo(DEFAULT_MASTER_SELECTION);
        assertThat(testMarketType.getDerivativeType()).isEqualTo(DEFAULT_DERIVATIVE_TYPE);
    }

    @Test
    public void getAllMarketTypes() throws Exception {
        // Initialize the database
        marketTypeRepository.save(marketType);

        // Get all the marketTypes
        restMarketTypeMockMvc.perform(get("/api/marketTypes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(marketType.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].cancelledByCodes").value(hasItem(DEFAULT_CANCELLED_BY_CODES.booleanValue())))
                .andExpect(jsonPath("$.[*].basicDisplay").value(hasItem(DEFAULT_BASIC_DISPLAY.booleanValue())))
                .andExpect(jsonPath("$.[*].masterSelection").value(hasItem(DEFAULT_MASTER_SELECTION.booleanValue())))
                .andExpect(jsonPath("$.[*].derivativeType").value(hasItem(DEFAULT_DERIVATIVE_TYPE.toString())));
    }

    @Test
    public void getMarketType() throws Exception {
        // Initialize the database
        marketTypeRepository.save(marketType);

        // Get the marketType
        restMarketTypeMockMvc.perform(get("/api/marketTypes/{id}", marketType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(marketType.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.cancelledByCodes").value(DEFAULT_CANCELLED_BY_CODES.booleanValue()))
            .andExpect(jsonPath("$.basicDisplay").value(DEFAULT_BASIC_DISPLAY.booleanValue()))
            .andExpect(jsonPath("$.masterSelection").value(DEFAULT_MASTER_SELECTION.booleanValue()))
            .andExpect(jsonPath("$.derivativeType").value(DEFAULT_DERIVATIVE_TYPE.toString()));
    }

    @Test
    public void getNonExistingMarketType() throws Exception {
        // Get the marketType
        restMarketTypeMockMvc.perform(get("/api/marketTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateMarketType() throws Exception {
        // Initialize the database
        marketTypeRepository.save(marketType);

		int databaseSizeBeforeUpdate = marketTypeRepository.findAll().size();

        // Update the marketType
        marketType.setDescription(UPDATED_DESCRIPTION);
        marketType.setCancelledByCodes(UPDATED_CANCELLED_BY_CODES);
        marketType.setBasicDisplay(UPDATED_BASIC_DISPLAY);
        marketType.setMasterSelection(UPDATED_MASTER_SELECTION);
        marketType.setDerivativeType(UPDATED_DERIVATIVE_TYPE);
        MarketTypeDTO marketTypeDTO = marketTypeMapper.marketTypeToMarketTypeDTO(marketType);

        restMarketTypeMockMvc.perform(put("/api/marketTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(marketTypeDTO)))
                .andExpect(status().isOk());

        // Validate the MarketType in the database
        List<MarketType> marketTypes = marketTypeRepository.findAll();
        assertThat(marketTypes).hasSize(databaseSizeBeforeUpdate);
        MarketType testMarketType = marketTypes.get(marketTypes.size() - 1);
        assertThat(testMarketType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMarketType.getCancelledByCodes()).isEqualTo(UPDATED_CANCELLED_BY_CODES);
        assertThat(testMarketType.getBasicDisplay()).isEqualTo(UPDATED_BASIC_DISPLAY);
        assertThat(testMarketType.getMasterSelection()).isEqualTo(UPDATED_MASTER_SELECTION);
        assertThat(testMarketType.getDerivativeType()).isEqualTo(UPDATED_DERIVATIVE_TYPE);
    }

    @Test
    public void deleteMarketType() throws Exception {
        // Initialize the database
        marketTypeRepository.save(marketType);

		int databaseSizeBeforeDelete = marketTypeRepository.findAll().size();

        // Get the marketType
        restMarketTypeMockMvc.perform(delete("/api/marketTypes/{id}", marketType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<MarketType> marketTypes = marketTypeRepository.findAll();
        assertThat(marketTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
