package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.TranslationObj;
import com.intralot.repository.TranslationObjRepository;
import com.intralot.service.TranslationObjService;
import com.intralot.test.TestUtil;
 




/**
 * Test class for the TranslationObjResource REST controller.
 *
 * @see TranslationObjController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TranslationObjControllerIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));


    private static final Date DEFAULT_KICK_OFF = DateTime.now().toDate();
    private static final Date UPDATED_KICK_OFF = DateTime.now().plusDays(1).toDate();

    @Inject
    private TranslationObjRepository translationObjRepository;

    @Inject
    private TranslationObjService translationObjService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTranslationObjMockMvc;

    private TranslationObj translationObj;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TranslationObjController translationObjResource = new TranslationObjController();
        ReflectionTestUtils.setField(translationObjResource, "translationObjService", translationObjService);
        this.restTranslationObjMockMvc = MockMvcBuilders.standaloneSetup(translationObjResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        translationObjRepository.deleteAll();
        translationObj = new TranslationObj();
        translationObj.setKickOff(DEFAULT_KICK_OFF);
    }

    @Test
    public void createTranslationObj() throws Exception {
        int databaseSizeBeforeCreate = translationObjRepository.findAll().size();

        // Create the TranslationObj

        restTranslationObjMockMvc.perform(post("/api/translationObjs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(translationObj)))
                .andExpect(status().isCreated());

        // Validate the TranslationObj in the database
        List<TranslationObj> translationObjs = translationObjRepository.findAll();
        assertThat(translationObjs).hasSize(databaseSizeBeforeCreate + 1);
        TranslationObj testTranslationObj = translationObjs.get(translationObjs.size() - 1);
        assertThat(testTranslationObj.getKickOff()).isEqualTo(DEFAULT_KICK_OFF);
    }

    @Test
    public void getAllTranslationObjs() throws Exception {
        // Initialize the database
        translationObjRepository.save(translationObj);

        // Get all the translationObjs
        restTranslationObjMockMvc.perform(get("/api/translationObjs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(translationObj.getId())))/*
                .andExpect(jsonPath("$.[*].kickOff").value(hasItem(DEFAULT_KICK_OFF_STR)))*/;
    }

    @Test
    public void getTranslationObj() throws Exception {
        // Initialize the database
        translationObjRepository.save(translationObj);

        // Get the translationObj
        restTranslationObjMockMvc.perform(get("/api/translationObjs/{id}", translationObj.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(translationObj.getId()))/*
            .andExpect(jsonPath("$.kickOff").value(DEFAULT_KICK_OFF_STR))*/;
    }

    @Test
    public void getNonExistingTranslationObj() throws Exception {
        // Get the translationObj
        restTranslationObjMockMvc.perform(get("/api/translationObjs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateTranslationObj() throws Exception {
        // Initialize the database
        translationObjRepository.save(translationObj);

		int databaseSizeBeforeUpdate = translationObjRepository.findAll().size();

        // Update the translationObj
        translationObj.setKickOff(UPDATED_KICK_OFF);

        restTranslationObjMockMvc.perform(put("/api/translationObjs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(translationObj)))
                .andExpect(status().isOk()); 

        // Validate the TranslationObj in the database
        List<TranslationObj> translationObjs = translationObjRepository.findAll();
        assertThat(translationObjs).hasSize(databaseSizeBeforeUpdate);
        TranslationObj testTranslationObj = translationObjs.get(translationObjs.size() - 1);
        assertThat(testTranslationObj.getKickOff()).isEqualTo(UPDATED_KICK_OFF);
    }

    @Test
    public void deleteTranslationObj() throws Exception {
        // Initialize the database
        translationObjRepository.save(translationObj);

		int databaseSizeBeforeDelete = translationObjRepository.findAll().size();

        // Get the translationObj
        restTranslationObjMockMvc.perform(delete("/api/translationObjs/{id}", translationObj.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TranslationObj> translationObjs = translationObjRepository.findAll();
        assertThat(translationObjs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
