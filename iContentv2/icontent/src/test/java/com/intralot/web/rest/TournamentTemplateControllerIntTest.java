package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.TournamentTemplate;
import com.intralot.repository.TournamentTemplateRepository;
import com.intralot.service.TournamentTemplateService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.TournamentTemplateDTO;
import com.intralot.web.rest.mapper.TournamentTemplateMapper;


/**
 * Test class for the TournamentTemplateResource REST controller.
 *
 * @see TournamentTemplateResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TournamentTemplateControllerIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Boolean DEFAULT_AUTO_PAYMENT_SUSPENSION = false;
    private static final Boolean UPDATED_AUTO_PAYMENT_SUSPENSION = true;

    @Inject
    private TournamentTemplateRepository tournamentTemplateRepository;

    @Inject
    private TournamentTemplateMapper tournamentTemplateMapper;

    @Inject
    private TournamentTemplateService tournamentTemplateService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTournamentTemplateMockMvc;

    private TournamentTemplate tournamentTemplate;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TournamentTemplateController tournamentTemplateResource = new TournamentTemplateController();
        ReflectionTestUtils.setField(tournamentTemplateResource, "tournamentTemplateService", tournamentTemplateService);
        ReflectionTestUtils.setField(tournamentTemplateResource, "tournamentTemplateMapper", tournamentTemplateMapper);
        this.restTournamentTemplateMockMvc = MockMvcBuilders.standaloneSetup(tournamentTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tournamentTemplateRepository.deleteAll();
        tournamentTemplate = new TournamentTemplate();
        tournamentTemplate.setDescription(DEFAULT_DESCRIPTION);
        tournamentTemplate.setAutoPaymentSuspension(DEFAULT_AUTO_PAYMENT_SUSPENSION);
    }

    @Test
    public void createTournamentTemplate() throws Exception {
        int databaseSizeBeforeCreate = tournamentTemplateRepository.findAll().size();

        // Create the TournamentTemplate
        TournamentTemplateDTO tournamentTemplateDTO = tournamentTemplateMapper.tournamentTemplateToTournamentTemplateDTO(tournamentTemplate);

        restTournamentTemplateMockMvc.perform(post("/api/tournamentTemplates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tournamentTemplateDTO)))
                .andExpect(status().isCreated());

        // Validate the TournamentTemplate in the database
        List<TournamentTemplate> tournamentTemplates = tournamentTemplateRepository.findAll();
        assertThat(tournamentTemplates).hasSize(databaseSizeBeforeCreate + 1);
        TournamentTemplate testTournamentTemplate = tournamentTemplates.get(tournamentTemplates.size() - 1);
        assertThat(testTournamentTemplate.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTournamentTemplate.getAutoPaymentSuspension()).isEqualTo(DEFAULT_AUTO_PAYMENT_SUSPENSION);
    }

    @Test
    public void getAllTournamentTemplates() throws Exception {
        // Initialize the database
        tournamentTemplateRepository.save(tournamentTemplate);

        // Get all the tournamentTemplates
        restTournamentTemplateMockMvc.perform(get("/api/tournamentTemplates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tournamentTemplate.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].autoPaymentSuspension").value(hasItem(DEFAULT_AUTO_PAYMENT_SUSPENSION.booleanValue())));
    }
    
    @Test
    public void getAllTournamentTemplateOverviews() throws Exception {
        // Initialize the database
        tournamentTemplateRepository.save(tournamentTemplate);

        // Get all the tournamentTemplates
        restTournamentTemplateMockMvc.perform(get("/api/tournamentTemplatesOverview?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tournamentTemplate.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
               ;
    }

    @Test
    public void getTournamentTemplate() throws Exception {
        // Initialize the database
        tournamentTemplateRepository.save(tournamentTemplate);

        // Get the tournamentTemplate
        restTournamentTemplateMockMvc.perform(get("/api/tournamentTemplates/{id}", tournamentTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tournamentTemplate.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.autoPaymentSuspension").value(DEFAULT_AUTO_PAYMENT_SUSPENSION.booleanValue()));
    }

    @Test
    public void getNonExistingTournamentTemplate() throws Exception {
        // Get the tournamentTemplate
        restTournamentTemplateMockMvc.perform(get("/api/tournamentTemplates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateTournamentTemplate() throws Exception {
        // Initialize the database
        tournamentTemplateRepository.save(tournamentTemplate);

		int databaseSizeBeforeUpdate = tournamentTemplateRepository.findAll().size();

        // Update the tournamentTemplate
        tournamentTemplate.setDescription(UPDATED_DESCRIPTION);
        tournamentTemplate.setAutoPaymentSuspension(UPDATED_AUTO_PAYMENT_SUSPENSION);
        TournamentTemplateDTO tournamentTemplateDTO = tournamentTemplateMapper.tournamentTemplateToTournamentTemplateDTO(tournamentTemplate);

        restTournamentTemplateMockMvc.perform(put("/api/tournamentTemplates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tournamentTemplateDTO)))
                .andExpect(status().isOk());

        // Validate the TournamentTemplate in the database
        List<TournamentTemplate> tournamentTemplates = tournamentTemplateRepository.findAll();
        assertThat(tournamentTemplates).hasSize(databaseSizeBeforeUpdate);
        TournamentTemplate testTournamentTemplate = tournamentTemplates.get(tournamentTemplates.size() - 1);
        assertThat(testTournamentTemplate.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTournamentTemplate.getAutoPaymentSuspension()).isEqualTo(UPDATED_AUTO_PAYMENT_SUSPENSION);
    }

    @Test
    public void deleteTournamentTemplate() throws Exception {
        // Initialize the database
        tournamentTemplateRepository.save(tournamentTemplate);

		int databaseSizeBeforeDelete = tournamentTemplateRepository.findAll().size();

        // Get the tournamentTemplate
        restTournamentTemplateMockMvc.perform(delete("/api/tournamentTemplates/{id}", tournamentTemplate.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<TournamentTemplate> tournamentTemplates = tournamentTemplateRepository.findAll();
        assertThat(tournamentTemplates).hasSize(databaseSizeBeforeDelete - 1);
    }
}
