package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.ForecastType;
import com.intralot.repository.ForecastTypeRepository;
import com.intralot.service.ForecastTypeService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.ForecastTypeDTO;
import com.intralot.web.rest.mapper.ForecastTypeMapper;


/**
 * Test class for the ForecastTypeResource REST controller.
 *
 * @see ForecastTypeController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ForecastTypeResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Integer DEFAULT_SUM_OF_CODES = 1;
    private static final Integer UPDATED_SUM_OF_CODES = 2;

    private static final Boolean DEFAULT_WITH_HANDICAP_A = false;
    private static final Boolean UPDATED_WITH_HANDICAP_A = true;

    private static final Boolean DEFAULT_WITH_HANDICAP_B = false;
    private static final Boolean UPDATED_WITH_HANDICAP_B = true;

    private static final Boolean DEFAULT_HAS_LIMIT = false;
    private static final Boolean UPDATED_HAS_LIMIT = true;

    private static final Boolean DEFAULT_HAS_DOUBLE_CHANCE = false;
    private static final Boolean UPDATED_HAS_DOUBLE_CHANCE = true;

    @Inject
    private ForecastTypeRepository forecastTypeRepository;

    @Inject
    private ForecastTypeMapper forecastTypeMapper;

    @Inject
    private ForecastTypeService forecastTypeService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restForecastTypeMockMvc;

    private ForecastType forecastType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ForecastTypeController forecastTypeResource = new ForecastTypeController();
        ReflectionTestUtils.setField(forecastTypeResource, "forecastTypeService", forecastTypeService);
        ReflectionTestUtils.setField(forecastTypeResource, "forecastTypeMapper", forecastTypeMapper);
        this.restForecastTypeMockMvc = MockMvcBuilders.standaloneSetup(forecastTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        forecastTypeRepository.deleteAll();
        forecastType = new ForecastType();
        forecastType.setDescription(DEFAULT_DESCRIPTION);
        forecastType.setSumOfCodes(DEFAULT_SUM_OF_CODES);
        forecastType.setWithHandicapA(DEFAULT_WITH_HANDICAP_A);
        forecastType.setWithHandicapB(DEFAULT_WITH_HANDICAP_B);
        forecastType.setHasLimit(DEFAULT_HAS_LIMIT);
        forecastType.setHasDoubleChance(DEFAULT_HAS_DOUBLE_CHANCE);
    }

    @Test
    public void createForecastType() throws Exception {
        int databaseSizeBeforeCreate = forecastTypeRepository.findAll().size();

        // Create the ForecastType
        ForecastTypeDTO forecastTypeDTO = forecastTypeMapper.forecastTypeToForecastTypeDTO(forecastType);

        restForecastTypeMockMvc.perform(post("/api/forecastTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(forecastTypeDTO)))
                .andExpect(status().isCreated());

        // Validate the ForecastType in the database
        List<ForecastType> forecastTypes = forecastTypeRepository.findAll();
        assertThat(forecastTypes).hasSize(databaseSizeBeforeCreate + 1);
        ForecastType testForecastType = forecastTypes.get(forecastTypes.size() - 1);
        assertThat(testForecastType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testForecastType.getSumOfCodes()).isEqualTo(DEFAULT_SUM_OF_CODES);
        assertThat(testForecastType.getWithHandicapA()).isEqualTo(DEFAULT_WITH_HANDICAP_A);
        assertThat(testForecastType.getWithHandicapB()).isEqualTo(DEFAULT_WITH_HANDICAP_B);
        assertThat(testForecastType.getHasLimit()).isEqualTo(DEFAULT_HAS_LIMIT);
        assertThat(testForecastType.getHasDoubleChance()).isEqualTo(DEFAULT_HAS_DOUBLE_CHANCE);
    }

    @Test
    public void getAllForecastTypes() throws Exception {
        // Initialize the database
        forecastTypeRepository.save(forecastType);

        // Get all the forecastTypes
        restForecastTypeMockMvc.perform(get("/api/forecastTypes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(forecastType.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].sumOfCodes").value(hasItem(DEFAULT_SUM_OF_CODES)))
                .andExpect(jsonPath("$.[*].withHandicapA").value(hasItem(DEFAULT_WITH_HANDICAP_A.booleanValue())))
                .andExpect(jsonPath("$.[*].withHandicapB").value(hasItem(DEFAULT_WITH_HANDICAP_B.booleanValue())))
                .andExpect(jsonPath("$.[*].hasLimit").value(hasItem(DEFAULT_HAS_LIMIT.booleanValue())))
                .andExpect(jsonPath("$.[*].hasDoubleChance").value(hasItem(DEFAULT_HAS_DOUBLE_CHANCE.booleanValue())));
    }

    @Test
    public void getForecastType() throws Exception {
        // Initialize the database
        forecastTypeRepository.save(forecastType);

        // Get the forecastType
        restForecastTypeMockMvc.perform(get("/api/forecastTypes/{id}", forecastType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(forecastType.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.sumOfCodes").value(DEFAULT_SUM_OF_CODES))
            .andExpect(jsonPath("$.withHandicapA").value(DEFAULT_WITH_HANDICAP_A.booleanValue()))
            .andExpect(jsonPath("$.withHandicapB").value(DEFAULT_WITH_HANDICAP_B.booleanValue()))
            .andExpect(jsonPath("$.hasLimit").value(DEFAULT_HAS_LIMIT.booleanValue()))
            .andExpect(jsonPath("$.hasDoubleChance").value(DEFAULT_HAS_DOUBLE_CHANCE.booleanValue()));
    }

    @Test
    public void getNonExistingForecastType() throws Exception {
        // Get the forecastType
        restForecastTypeMockMvc.perform(get("/api/forecastTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateForecastType() throws Exception {
        // Initialize the database
        forecastTypeRepository.save(forecastType);

		int databaseSizeBeforeUpdate = forecastTypeRepository.findAll().size();

        // Update the forecastType
        forecastType.setDescription(UPDATED_DESCRIPTION);
        forecastType.setSumOfCodes(UPDATED_SUM_OF_CODES);
        forecastType.setWithHandicapA(UPDATED_WITH_HANDICAP_A);
        forecastType.setWithHandicapB(UPDATED_WITH_HANDICAP_B);
        forecastType.setHasLimit(UPDATED_HAS_LIMIT);
        forecastType.setHasDoubleChance(UPDATED_HAS_DOUBLE_CHANCE);
        ForecastTypeDTO forecastTypeDTO = forecastTypeMapper.forecastTypeToForecastTypeDTO(forecastType);

        restForecastTypeMockMvc.perform(put("/api/forecastTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(forecastTypeDTO)))
                .andExpect(status().isOk());

        // Validate the ForecastType in the database
        List<ForecastType> forecastTypes = forecastTypeRepository.findAll();
        assertThat(forecastTypes).hasSize(databaseSizeBeforeUpdate);
        ForecastType testForecastType = forecastTypes.get(forecastTypes.size() - 1);
        assertThat(testForecastType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testForecastType.getSumOfCodes()).isEqualTo(UPDATED_SUM_OF_CODES);
        assertThat(testForecastType.getWithHandicapA()).isEqualTo(UPDATED_WITH_HANDICAP_A);
        assertThat(testForecastType.getWithHandicapB()).isEqualTo(UPDATED_WITH_HANDICAP_B);
        assertThat(testForecastType.getHasLimit()).isEqualTo(UPDATED_HAS_LIMIT);
        assertThat(testForecastType.getHasDoubleChance()).isEqualTo(UPDATED_HAS_DOUBLE_CHANCE);
    }

    @Test
    public void deleteForecastType() throws Exception {
        // Initialize the database
        forecastTypeRepository.save(forecastType);

		int databaseSizeBeforeDelete = forecastTypeRepository.findAll().size();

        // Get the forecastType
        restForecastTypeMockMvc.perform(delete("/api/forecastTypes/{id}", forecastType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ForecastType> forecastTypes = forecastTypeRepository.findAll();
        assertThat(forecastTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
