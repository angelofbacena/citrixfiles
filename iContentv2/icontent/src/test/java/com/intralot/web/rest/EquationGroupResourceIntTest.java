package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.domain.EquationGroup;
import com.intralot.repository.EquationGroupRepository;
import com.intralot.service.EquationGroupService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.EquationGroupDTO;
import com.intralot.web.rest.mapper.EquationGroupMapper;


/**
 * Test class for the EquationGroupResource REST controller.
 *
 * @see EquationGroupController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EquationGroupResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private EquationGroupRepository equationGroupRepository;

    @Inject
    private EquationGroupMapper equationGroupMapper;

    @Inject
    private EquationGroupService equationGroupService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restEquationGroupMockMvc;

    private EquationGroup equationGroup;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EquationGroupController equationGroupResource = new EquationGroupController();
        ReflectionTestUtils.setField(equationGroupResource, "equationGroupService", equationGroupService);
        ReflectionTestUtils.setField(equationGroupResource, "equationGroupMapper", equationGroupMapper);
        this.restEquationGroupMockMvc = MockMvcBuilders.standaloneSetup(equationGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        equationGroupRepository.deleteAll();
        equationGroup = new EquationGroup();
        equationGroup.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createEquationGroup() throws Exception {
        int databaseSizeBeforeCreate = equationGroupRepository.findAll().size();

        // Create the EquationGroup
        EquationGroupDTO equationGroupDTO = equationGroupMapper.equationGroupToEquationGroupDTO(equationGroup);

        restEquationGroupMockMvc.perform(post("/api/equationGroups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(equationGroupDTO)))
                .andExpect(status().isCreated());

        // Validate the EquationGroup in the database
        List<EquationGroup> equationGroups = equationGroupRepository.findAll();
        assertThat(equationGroups).hasSize(databaseSizeBeforeCreate + 1);
        EquationGroup testEquationGroup = equationGroups.get(equationGroups.size() - 1);
        assertThat(testEquationGroup.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void getAllEquationGroups() throws Exception {
        // Initialize the database
        equationGroupRepository.save(equationGroup);

        // Get all the equationGroups
        restEquationGroupMockMvc.perform(get("/api/equationGroups?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(equationGroup.getId())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void getEquationGroup() throws Exception {
        // Initialize the database
        equationGroupRepository.save(equationGroup);

        // Get the equationGroup
        restEquationGroupMockMvc.perform(get("/api/equationGroups/{id}", equationGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(equationGroup.getId()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingEquationGroup() throws Exception {
        // Get the equationGroup
        restEquationGroupMockMvc.perform(get("/api/equationGroups/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateEquationGroup() throws Exception {
        // Initialize the database
        equationGroupRepository.save(equationGroup);

		int databaseSizeBeforeUpdate = equationGroupRepository.findAll().size();

        // Update the equationGroup
        equationGroup.setDescription(UPDATED_DESCRIPTION);
        EquationGroupDTO equationGroupDTO = equationGroupMapper.equationGroupToEquationGroupDTO(equationGroup);

        restEquationGroupMockMvc.perform(put("/api/equationGroups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(equationGroupDTO)))
                .andExpect(status().isOk());

        // Validate the EquationGroup in the database
        List<EquationGroup> equationGroups = equationGroupRepository.findAll();
        assertThat(equationGroups).hasSize(databaseSizeBeforeUpdate);
        EquationGroup testEquationGroup = equationGroups.get(equationGroups.size() - 1);
        assertThat(testEquationGroup.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void deleteEquationGroup() throws Exception {
        // Initialize the database
        equationGroupRepository.save(equationGroup);

		int databaseSizeBeforeDelete = equationGroupRepository.findAll().size();

        // Get the equationGroup
        restEquationGroupMockMvc.perform(delete("/api/equationGroups/{id}", equationGroup.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<EquationGroup> equationGroups = equationGroupRepository.findAll();
        assertThat(equationGroups).hasSize(databaseSizeBeforeDelete - 1);
    }
}
