package com.intralot.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.intralot.Application;
import com.intralot.config.Constants;
import com.intralot.domain.Fixture;
import com.intralot.repository.FixtureRepository;
import com.intralot.service.FixtureService;
import com.intralot.test.TestUtil;
import com.intralot.web.rest.dto.FixtureDTO;
import com.intralot.web.rest.mapper.FixtureMapper;

/**
 * Test class for the FixtureResource REST controller.
 *
 * @see FixtureController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(Constants.SPRING_PROFILE_TEST)
public class FixtureControllerIntTest {
	
	@Inject
    private FixtureRepository fixtureRepository;

    @Inject
    private FixtureMapper fixtureMapper;

    @Inject
    private FixtureService fixtureService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restFixtureMockMvc;

    private Fixture fixture;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FixtureController fixtureResource = new FixtureController();
        ReflectionTestUtils.setField(fixtureResource, "fixtureService", fixtureService);
        ReflectionTestUtils.setField(fixtureResource, "fixtureMapper", fixtureMapper);
        this.restFixtureMockMvc = MockMvcBuilders.standaloneSetup(fixtureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        fixtureRepository.deleteAll();
        fixture = TestUtil.createDummyFixture(null); 
        		/*new Fixture();
        fixture.setActivationDate(TestUtil.DEFAULT_ACTIVATION_DATE);
        fixture.setKickOff(TestUtil.DEFAULT_KICK_OFF);
        fixture.setEndDate(TestUtil.DEFAULT_END_DATE);
        fixture.setStatus(TestUtil.DEFAULT_STATUS);*/
        
    }

    @Test
    public void createFixture() throws Exception {
        int databaseSizeBeforeCreate = fixtureRepository.findAll().size();

        // Create the Fixture
        FixtureDTO fixtureDTO = fixtureMapper.fixtureToFixtureDTO(fixture);

        restFixtureMockMvc.perform(post("/api/fixtures")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fixtureDTO)))
                .andExpect(status().isCreated());

        // Validate the Fixture in the database
        List<Fixture> fixtures = fixtureRepository.findAll();
        assertThat(fixtures).hasSize(databaseSizeBeforeCreate + 1);
        Fixture testFixture = fixtures.get(fixtures.size() - 1);
        assertThat(testFixture.getActivationDate()).isEqualTo(TestUtil.DEFAULT_ACTIVATION_DATE);
        assertThat(testFixture.getKickOff()).isEqualTo(TestUtil.DEFAULT_KICK_OFF);
        assertThat(testFixture.getEndDate()).isEqualTo(TestUtil.DEFAULT_END_DATE);
        assertThat(testFixture.getStatus()).isEqualTo(TestUtil.DEFAULT_STATUS);
    }

    @Test
    public void getAllFixtures() throws Exception {
        // Initialize the database 
    	fixture.setKickOff( ZonedDateTime.now());
        fixtureRepository.save(fixture);
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        String  fromDate = fixture.getKickOff().minusDays(1).format(TestUtil.dateTimeFormatter);
        String  toDate = fixture.getKickOff().plusDays(1).format(TestUtil.dateTimeFormatter);
        
        // Get all the fixtures
        restFixtureMockMvc.perform(get("/api/fixtures?fromDate="+fromDate + "&toDate="+toDate+"&tournamentId="+fixture.getTournament().getTournamentId()+"&sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(fixture.getId())))
                .andExpect(jsonPath("$.[*].activationDate").value(hasItem(TestUtil.DEFAULT_ACTIVATION_DATE_STR)))
                .andExpect(jsonPath("$.[*].kickOff").value(hasItem(fixture.getKickOff().format((TestUtil.dateTimeFormatter)))))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(TestUtil.DEFAULT_END_DATE_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(TestUtil.DEFAULT_STATUS.toString())));
    }

    @Test
    public void getFixture() throws Exception {
        // Initialize the database
        fixtureRepository.save(fixture);

        // Get the fixture
        restFixtureMockMvc.perform(get("/api/fixtures/{id}", fixture.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(fixture.getId()))
            .andExpect(jsonPath("$.activationDate").value(TestUtil.DEFAULT_ACTIVATION_DATE_STR))
            .andExpect(jsonPath("$.kickOff").value(TestUtil.DEFAULT_KICK_OFF_STR))
            .andExpect(jsonPath("$.endDate").value(TestUtil.DEFAULT_END_DATE_STR))
            .andExpect(jsonPath("$.status").value(TestUtil.DEFAULT_STATUS.toString()));
    }

    @Test
    public void getNonExistingFixture() throws Exception {
        // Get the fixture
        restFixtureMockMvc.perform(get("/api/fixtures/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateFixture() throws Exception {
        // Initialize the database
        fixtureRepository.save(fixture);

		int databaseSizeBeforeUpdate = fixtureRepository.findAll().size();

        // Update the fixture
        fixture.setActivationDate(TestUtil.UPDATED_ACTIVATION_DATE);
        fixture.setKickOff(TestUtil.UPDATED_KICK_OFF);
        fixture.setEndDate(TestUtil.UPDATED_END_DATE);
        fixture.setStatus(TestUtil.UPDATED_STATUS);
        FixtureDTO fixtureDTO = fixtureMapper.fixtureToFixtureDTO(fixture);

        restFixtureMockMvc.perform(put("/api/fixtures")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fixtureDTO)))
                .andExpect(status().isOk());

        // Validate the Fixture in the database
        List<Fixture> fixtures = fixtureRepository.findAll();
        assertThat(fixtures).hasSize(databaseSizeBeforeUpdate);
        Fixture testFixture = fixtures.get(fixtures.size() - 1);
        assertThat(testFixture.getActivationDate()).isEqualTo(TestUtil.UPDATED_ACTIVATION_DATE);
        assertThat(testFixture.getKickOff()).isEqualTo(TestUtil.UPDATED_KICK_OFF);
        assertThat(testFixture.getEndDate()).isEqualTo(TestUtil.UPDATED_END_DATE);
        assertThat(testFixture.getStatus()).isEqualTo(TestUtil.UPDATED_STATUS);
    }

    @Test
    public void deleteFixture() throws Exception {
        // Initialize the database
        fixtureRepository.save(fixture);

		int databaseSizeBeforeDelete = fixtureRepository.findAll().size();

        // Get the fixture
        restFixtureMockMvc.perform(delete("/api/fixtures/{id}", fixture.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Fixture> fixtures = fixtureRepository.findAll();
        assertThat(fixtures).hasSize(databaseSizeBeforeDelete - 1);
    }
}
