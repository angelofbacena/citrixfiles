'use strict';

describe('Controller Tests', function() {

    describe('ForecastType Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockForecastType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockForecastType = jasmine.createSpy('MockForecastType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ForecastType': MockForecastType
            };
            createController = function() {
                $injector.get('$controller')("ForecastTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'icontentApp:forecastTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
