'use strict';

describe('Controller Tests', function() {

    describe('TournamentTemplate Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTournamentTemplate;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTournamentTemplate = jasmine.createSpy('MockTournamentTemplate');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TournamentTemplate': MockTournamentTemplate
            };
            createController = function() {
                $injector.get('$controller')("TournamentTemplateDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'icontentApp:tournamentTemplateUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
