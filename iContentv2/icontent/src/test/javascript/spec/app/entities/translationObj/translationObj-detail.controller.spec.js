'use strict';

describe('Controller Tests', function() {

    describe('TranslationObj Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTranslationObj;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTranslationObj = jasmine.createSpy('MockTranslationObj');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'TranslationObj': MockTranslationObj
            };
            createController = function() {
                $injector.get('$controller')("TranslationObjDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'icontentApp:translationObjUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
