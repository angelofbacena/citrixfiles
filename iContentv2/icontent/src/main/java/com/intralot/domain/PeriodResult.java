package com.intralot.domain;

import java.util.HashSet;
import java.util.Set;

import com.intralot.domain.enumeration.Period;

public class PeriodResult {
	
	Period period;
	ResultEntry resultEntry;
	Set<FeedResultEntry> feedResultEntries;
	
	
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public ResultEntry getResultEntry() {
		return resultEntry;
	}
	public void setResultEntry(ResultEntry resultEntry) {
		this.resultEntry = resultEntry;
	}
	public Set<FeedResultEntry> getFeedResultEntries() {
		if (feedResultEntries == null ) {
			feedResultEntries = new HashSet<FeedResultEntry>();
		}
		return feedResultEntries;
	}
	public void setFeedResultEntries(Set<FeedResultEntry> feedResultEntries) {
		this.feedResultEntries = feedResultEntries;
	}
	@Override
	public String toString() {
		return "PeriodResult [period=" + period + ", resultEntry=" + resultEntry + ", feedResultEntries="
				+ feedResultEntries + "]";
	}
	
	
	
}
