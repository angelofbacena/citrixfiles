package com.intralot.domain;

import com.intralot.lexicon.model.utils.Properties.Side;

public class ResultEntry {
	protected int homeScore;
	protected int awayScore;
	protected Side firstScoring;
	
	
	public int getHomeScore() {
		return homeScore;
	}
	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}
	public int getAwayScore() {
		return awayScore;
	}
	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}
	public Side getFirstScoring() {
		return firstScoring;
	}
	public void setFirstScoring(Side firstScoring) {
		this.firstScoring = firstScoring;
	}
	@Override
	public String toString() {
		return "ResultEntry [homeScore=" + homeScore + ", awayScore=" + awayScore + ", firstScoring=" + firstScoring
				+ "]";
	}
	
	

}
