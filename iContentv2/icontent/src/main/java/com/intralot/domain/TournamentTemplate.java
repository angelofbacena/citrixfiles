package com.intralot.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * A TournamentTemplate.
 */

@Document(collection = "tournament_template")
public class TournamentTemplate implements Serializable {

    @Id
    private String id;

    private String description;
    
    private Boolean autoPaymentSuspension;
    
    private OddsFeedSettings oddsFeedSettings;
    
    private String dataFeed;
    
    private ResultingAlertThreshold resultingAlertThreshold;
    
    @DBRef
    private Set<Subscriber> subscribers;
    
    
    public OddsFeedSettings getOddsFeedSettings() {
		return oddsFeedSettings;
	}

	public void setOddsFeedSettings(OddsFeedSettings oddsFeedSettings) {
		this.oddsFeedSettings = oddsFeedSettings;
	}

	public String getDataFeed() {
		return dataFeed;
	}

	public void setDataFeed(String dataFeed) {
		this.dataFeed = dataFeed;
	}

	public ResultingAlertThreshold getResultingAlertThreshold() {
		return resultingAlertThreshold;
	}

	public void setResultingAlertThreshold(ResultingAlertThreshold resultingAlertThreshold) {
		this.resultingAlertThreshold = resultingAlertThreshold;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAutoPaymentSuspension() {
        return autoPaymentSuspension;
    }
    
    public void setAutoPaymentSuspension(Boolean autoPaymentSuspension) {
        this.autoPaymentSuspension = autoPaymentSuspension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TournamentTemplate tournamentTemplate = (TournamentTemplate) o;
        if(tournamentTemplate.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tournamentTemplate.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "TournamentTemplate [id=" + id + ", description=" + description + ", autoPaymentSuspension="
				+ autoPaymentSuspension + ", oddsFeedSettings=" + oddsFeedSettings + ", dataFeed=" + dataFeed
				+ ", resultingAlertThreshold=" + resultingAlertThreshold + "]";
	}
}
