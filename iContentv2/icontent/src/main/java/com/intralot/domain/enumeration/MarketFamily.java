package com.intralot.domain.enumeration;

/**
 * The MarketFamily enumeration.
 */
public enum MarketFamily {
    winner, totals, spread, halffinal, mostgoals, nexttoscore,correctscore, racetoXgoals, timeofgoal, runofplay, handicapingames, handicapinpoints, handicapinsets
}
