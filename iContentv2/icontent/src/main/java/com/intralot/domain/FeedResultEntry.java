package com.intralot.domain;

public class FeedResultEntry extends ResultEntry {
	private String feed;

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	@Override
	public String toString() {
		return "FeedResultEntry [homeScore=" + homeScore + ", awayScore=" + awayScore + ", firstScoring=" + firstScoring
				+ ", feed = " +  feed + "]";
	}
	

}
