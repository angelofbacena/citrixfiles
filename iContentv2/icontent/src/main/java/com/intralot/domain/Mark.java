package com.intralot.domain;

import com.intralot.domain.util.Properties.Period;
import com.intralot.lexicon.model.utils.Properties.Side;

public class Mark {
	
	private String sign;
	private Period period;
	private Side side;
	
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public Side getSide() {
		return side;
	}
	public void setSide(Side side) {
		this.side = side;
	}

}
