package com.intralot.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.intralot.domain.enumeration.DerivativeType;

/**
 * A MarketType.
 */

@Document(collection = "market_type")
public class MarketType implements Serializable {

    @Id
    private String id;

    @Field("description")
    private String description;
    
    @Field("cancelled_by_codes")
    private Boolean cancelledByCodes;
    
    @Field("basic_display")
    private Boolean basicDisplay;
    
    @Field("master_selection")
    private Boolean masterSelection;
    
    @Field("derivative_type")
    private DerivativeType derivativeType;
    
    @DBRef
    @Field("forecast_type")
    private ForecastType forecastType;
    
    @Field("code_type")
    private List<CodeType> codeTypes;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCancelledByCodes() {
        return cancelledByCodes;
    }
    
    public void setCancelledByCodes(Boolean cancelledByCodes) {
        this.cancelledByCodes = cancelledByCodes;
    }

    public Boolean getBasicDisplay() {
        return basicDisplay;
    }
    
    public void setBasicDisplay(Boolean basicDisplay) {
        this.basicDisplay = basicDisplay;
    }

    public Boolean getMasterSelection() {
        return masterSelection;
    }
    
    public void setMasterSelection(Boolean masterSelection) {
        this.masterSelection = masterSelection;
    }

    public DerivativeType getDerivativeType() {
        return derivativeType;
    }
    
    public void setDerivativeType(DerivativeType derivativeType) {
        this.derivativeType = derivativeType;
    }
    
    public ForecastType getForecastType() {
		return forecastType;
	}

	public void setForecastType(ForecastType forecastType) {
		this.forecastType = forecastType;
	}

	public List<CodeType> getCodeTypes() {
		return codeTypes;
	}

	public void setCodeTypes(List<CodeType> codeTypes) {
		this.codeTypes = codeTypes;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MarketType marketType = (MarketType) o;
        if(marketType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, marketType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "MarketType [id=" + id + ", description=" + description
				+ ", cancelledByCodes=" + cancelledByCodes + ", basicDisplay="
				+ basicDisplay + ", masterSelection=" + masterSelection
				+ ", derivativeType=" + derivativeType + ", forecastType="
				+ forecastType + ", codeTypes=" + codeTypes + "]";
	}
}
