package com.intralot.domain.enumeration;

/**
 * The MarketSide enumeration.
 */
public enum MarketSide {
    home,away,both,draw
}
