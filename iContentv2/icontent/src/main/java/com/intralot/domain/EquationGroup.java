package com.intralot.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A EquationGroup.
 */

@Document(collection = "equation_group")
public class EquationGroup implements Serializable {

    @Id
    private String id;

    @Field("description")
    private String description;
    
    @Field("equations")
    private Set<Equation> equations;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Equation> getEquations() {
		return equations;
	}

	public void setEquations(Set<Equation> equations) {
		this.equations = equations;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EquationGroup equationGroup = (EquationGroup) o;
        if(equationGroup.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, equationGroup.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "EquationGroup [id=" + id + ", description=" + description
				+ ", equations=" + equations + "]";
	}
}
