package com.intralot.domain;

import java.time.ZoneId;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TranslationObj {

	@Id
	private String id;

	
	private Integer feedId;
	private boolean translated;
	private TranslatableEntity sport;
	private TranslatableEntity category;
	private TranslatableEntity tournament;
	private TranslatableEntity homeTeam;
	private TranslatableEntity awayTeam;
	private Date kickOff;
	private String zoneId;
	private String feed;
	private Date received;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TranslatableEntity getSport() {
		return sport;
	}

	public void setSport(TranslatableEntity sport) {
		this.sport = sport;
	}

	public TranslatableEntity getCategory() {
		return category;
	}

	public void setCategory(TranslatableEntity category) {
		this.category = category;
	}

	public TranslatableEntity getTournament() {
		return tournament;
	}

	public void setTournament(TranslatableEntity tournament) {
		this.tournament = tournament;
	}

	public TranslatableEntity getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(TranslatableEntity homeTeam) {
		this.homeTeam = homeTeam;
	}

	public TranslatableEntity getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(TranslatableEntity awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Date getKickOff() {
		return kickOff;
	}

	public void setKickOff(Date kickOff) {
		this.kickOff = kickOff;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}
	public boolean isTranslated() {
		return translated;
	}

	public void setTranslated(boolean translated) {
		this.translated = translated;
	}

 

	public Integer getFeedId() {
		return feedId;
	}

	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}
	public Date getReceived() {
		return received;
	}

	public void setReceived(Date received) {
		this.received = received;
	}

	@Override
	public String toString() {
		return "TranslationObj [sport=" + sport + ", category=" + category + ", tournament=" + tournament
				+ ", homeTeam=" + homeTeam + ", awayTeam=" + awayTeam + ", kickOff=" + kickOff + "]";
	}

	

	
}
