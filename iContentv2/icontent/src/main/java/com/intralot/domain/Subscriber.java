package com.intralot.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Subscriber.
 */

@Document(collection = "subscriber")
public class Subscriber implements Serializable {

    @Id
    private String id;

    @NotNull
    @Field("description")
    private String description;
    
    @NotNull
    @Field("endpoint")
    private String endpoint;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndpoint() {
        return endpoint;
    }
    
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subscriber subscriber = (Subscriber) o;
        if(subscriber.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subscriber.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Subscriber{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", endpoint='" + endpoint + "'" +
            '}';
    }
}
