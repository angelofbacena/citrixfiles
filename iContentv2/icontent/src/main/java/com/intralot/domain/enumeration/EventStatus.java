package com.intralot.domain.enumeration;

/**
 * The EventStatus enumeration.
 */
public enum EventStatus {
    inactive, partiallyPriced, priced, active, blocked, cancelled, started, resulted, completed
}
