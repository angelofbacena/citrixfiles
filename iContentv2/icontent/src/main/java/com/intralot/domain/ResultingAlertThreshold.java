package com.intralot.domain;

public class ResultingAlertThreshold {
	
	private int kickOffOffsetInHours;
	private int followupInHours;
	
	
	public int getKickOffOffsetInHours() {
		return kickOffOffsetInHours;
	}
	public void setKickOffOffsetInHours(int kickOffOffsetInHours) {
		this.kickOffOffsetInHours = kickOffOffsetInHours;
	}
	public int getFollowupInHours() {
		return followupInHours;
	}
	public void setFollowupInHours(int followupInHours) {
		this.followupInHours = followupInHours;
	}
	
	

}
