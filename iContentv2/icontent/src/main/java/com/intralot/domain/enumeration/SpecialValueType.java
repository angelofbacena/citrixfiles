package com.intralot.domain.enumeration;

/**
 * The SpecialValueType enumeration.
 */
public enum SpecialValueType {
    limit, handicap, odd, algorithmPrefixed, tableSmost, setBetting, dynamicLimits , tiebreaks, tennisSumGames
}
