package com.intralot.domain;

import com.intralot.domain.util.Properties.ConstantType;

public class Constant {

	private ConstantType type;
	private String sign;
	private Double value;
	
	public ConstantType getType() {
		return type;
	}
	public void setType(ConstantType type) {
		this.type = type;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
	
}
