package com.intralot.domain;

import com.intralot.domain.enumeration.FeedType;

public class OddsFeedSettings {
	
	private String feed;
	private FeedType feedType;
	
	
	public String getFeed() {
		return feed;
	}
	public void setFeed(String feed) {
		this.feed = feed;
	}
	public FeedType getFeedType() {
		return feedType;
	}
	public void setFeedType(FeedType feedType) {
		this.feedType = feedType;
	}
	
	

}
