package com.intralot.domain.enumeration;

/**
 * The MarketPeriod enumeration.
 */
public enum MarketPeriod {
    fullmatch,period1,period2,period3,period4,period5,period6,period7,firsthalf,secondhalf, firstandsecondhalf,firstandfullmatch,secondandfullmatch
}
