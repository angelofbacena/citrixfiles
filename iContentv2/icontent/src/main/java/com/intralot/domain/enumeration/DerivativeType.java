package com.intralot.domain.enumeration;

/**
 * The DerivativeType enumeration.
 */
public enum DerivativeType {
    derivativeTable, derivativeAlgorithm, manual, provider, _reserved_, providerBeforeInput, _undefined_
}
