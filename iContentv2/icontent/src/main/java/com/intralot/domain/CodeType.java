package com.intralot.domain;

import com.intralot.domain.enumeration.MarketSide;

public class CodeType {

	private String description;
	private MarketSide algorithmCodeSide;
	private Double  algorithmCodeLowLimit;
	private Double algorithmCodeHighLimit;
	private /*CodeSpecialValue*/String algorithmCodeSpecialValue;
	private Boolean algorithmCodeSubstractionDependant;
	private String algorithmCodeNormalKey;
	private String algorithmCodeDescription;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MarketSide getAlgorithmCodeSide() {
		return algorithmCodeSide;
	}

	public void setAlgorithmCodeSide(MarketSide algorithmCodeSide) {
		this.algorithmCodeSide = algorithmCodeSide;
	}

	public Double getAlgorithmCodeLowLimit() {
		return algorithmCodeLowLimit;
	}

	public void setAlgorithmCodeLowLimit(Double algorithmCodeLowLimit) {
		this.algorithmCodeLowLimit = algorithmCodeLowLimit;
	}

	public Double getAlgorithmCodeHighLimit() {
		return algorithmCodeHighLimit;
	}

	public void setAlgorithmCodeHighLimit(Double algorithmCodeHighLimit) {
		this.algorithmCodeHighLimit = algorithmCodeHighLimit;
	}

	public String getAlgorithmCodeSpecialValue() {
		return algorithmCodeSpecialValue;
	}

	public void setAlgorithmCodeSpecialValue(String algorithmCodeSpecialValue) {
		this.algorithmCodeSpecialValue = algorithmCodeSpecialValue;
	}

	public Boolean getAlgorithmCodeSubstractionDependant() {
		return algorithmCodeSubstractionDependant;
	}

	public void setAlgorithmCodeSubstractionDependant(
			Boolean algorithmCodeSubstractionDependant) {
		this.algorithmCodeSubstractionDependant = algorithmCodeSubstractionDependant;
	}

	public String getAlgorithmCodeNormalKey() {
		return algorithmCodeNormalKey;
	}

	public void setAlgorithmCodeNormalKey(String algorithmCodeNormalKey) {
		this.algorithmCodeNormalKey = algorithmCodeNormalKey;
	}

	public String getAlgorithmCodeDescription() {
		return algorithmCodeDescription;
	}

	public void setAlgorithmCodeDescription(String algorithmCodeDescription) {
		this.algorithmCodeDescription = algorithmCodeDescription;
	}

}
