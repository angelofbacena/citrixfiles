package com.intralot.domain.util;

public class Properties {
	
	public enum Period {
		first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth
	}

	public enum ConstantType {
		limit, handicapA, handicapB, constant
	}
	
}
