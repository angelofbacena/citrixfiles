package com.intralot.domain;

public class TranslatableEntity {
	
	private String id;
	private String description;
	private boolean translated;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isTranslated() {
		return translated;
	}
	public void setTranslated(boolean translated) {
		this.translated = translated;
	}
	
	@Override
	public String toString() {
		return "TranslatableEntity [id=" + id + ", description=" + description + ", translated=" + translated + "]";
	}
	
	

}
