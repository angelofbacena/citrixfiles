package com.intralot.domain;

import java.util.HashSet;
import java.util.Set;

public class Result {
	
	private boolean manuallyCorrected;
	
	Set<PeriodResult> periodResults;
	
	
	public boolean isManuallyCorrected() {
		return manuallyCorrected;
	}
	public void setManuallyCorrected(boolean manuallyCorrected) {
		this.manuallyCorrected = manuallyCorrected;
	}
	public Set<PeriodResult> getPeriodResults() {
		if (periodResults == null) {
			periodResults = new HashSet<PeriodResult>();
		}
		return periodResults;
	}
	public void setPeriodResults(Set<PeriodResult> periodResults) {
		this.periodResults = periodResults;
	}
	@Override
	public String toString() {
		return "Result [manuallyCorrected=" + manuallyCorrected + ", periodResults=" + periodResults + "]";
	}
	

	
	
	

}
