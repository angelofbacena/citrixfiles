package com.intralot.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.intralot.domain.enumeration.EventStatus;
import com.intralot.lexicon.model.Category;
import com.intralot.lexicon.model.Sport;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;

/**
 * A Fixture.
 */

@Document(collection = "fixture")
public class Fixture implements Serializable {

    @Id
    private String id;

    private ZonedDateTime activationDate;
    
    private ZonedDateTime kickOff;
    
    private ZonedDateTime endDate;
    
    private EventStatus status;
    
    private Sport sport;
    
    private Category category;
    
    private Tournament tournament;
    
    private Team homeTeam;
    
    private Team awayTeam;
    
    private Map<String, Integer> feedIds;
    
    private Result result;
    
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getActivationDate() {
        return activationDate;
    }
    
    public void setActivationDate(ZonedDateTime activationDate) {
        this.activationDate = activationDate;
    }

    public ZonedDateTime getKickOff() {
        return kickOff;
    }
    
    public void setKickOff(ZonedDateTime kickOff) {
        this.kickOff = kickOff;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }
    
    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public EventStatus getStatus() {
        return status;
    }
    
    public void setStatus(EventStatus status) {
        this.status = status;
    }
    public Sport getSport() {
		return sport;
	}

	public void setSport(Sport sport) {
		this.sport = sport;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Map<String, Integer> getFeedIds() {
		if (feedIds == null) {
			feedIds = new HashMap<String, Integer>();
		}
		return feedIds;
	}

	public void setFeedIds(Map<String, Integer> feedIds) {
		this.feedIds = feedIds;
	}
	

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fixture fixture = (Fixture) o;
        if(fixture.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, fixture.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Fixture{" +
            "id=" + id +
            ", activationDate='" + activationDate + "'" +
            ", kickOff='" + kickOff + "'" +
            ", endDate='" + endDate + "'" +
            ", status='" + status + "'" +
            '}';
    }

	 
	
}
