package com.intralot.domain;

import java.util.Set;

public class Equation {

	private Set<EquationPart> leftHandSide;
	private Set<EquationPart> rightHandSide;
	private Boolean remove;
	private String comparison;
	private Integer equationOrder;

	public Set<EquationPart> getLeftHandSide() {
		return leftHandSide;
	}

	public void setLeftHandSide(Set<EquationPart> leftHandSide) {
		this.leftHandSide = leftHandSide;
	}

	public Set<EquationPart> getRightHandSide() {
		return rightHandSide;
	}

	public void setRightHandSide(Set<EquationPart> rightHandSide) {
		this.rightHandSide = rightHandSide;
	}

	public Boolean getRemove() {
		return remove;
	}

	public void setRemove(Boolean remove) {
		this.remove = remove;
	}

	public String getComparison() {
		return comparison;
	}

	public void setComparison(String comparison) {
		this.comparison = comparison;
	}

	public Integer getEquationOrder() {
		return equationOrder;
	}

	public void setEquationOrder(Integer equationOrder) {
		this.equationOrder = equationOrder;
	}
	
	
}


