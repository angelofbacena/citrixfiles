package com.intralot.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ForecastType.
 */

@Document(collection = "forecast_type")
public class ForecastType implements Serializable {

    @Id
    private String id;

    @Field("description")
    private String description;
    
    @Field("sum_of_codes")
    private Integer sumOfCodes;
    
    @Field("with_handicap_a")
    private Boolean withHandicapA;
    
    @Field("with_handicap_b")
    private Boolean withHandicapB;
    
    @Field("has_limit")
    private Boolean hasLimit;
    
    @Field("has_double_chance")
    private Boolean hasDoubleChance;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSumOfCodes() {
        return sumOfCodes;
    }
    
    public void setSumOfCodes(Integer sumOfCodes) {
        this.sumOfCodes = sumOfCodes;
    }

    public Boolean getWithHandicapA() {
        return withHandicapA;
    }
    
    public void setWithHandicapA(Boolean withHandicapA) {
        this.withHandicapA = withHandicapA;
    }

    public Boolean getWithHandicapB() {
        return withHandicapB;
    }
    
    public void setWithHandicapB(Boolean withHandicapB) {
        this.withHandicapB = withHandicapB;
    }

    public Boolean getHasLimit() {
        return hasLimit;
    }
    
    public void setHasLimit(Boolean hasLimit) {
        this.hasLimit = hasLimit;
    }

    public Boolean getHasDoubleChance() {
        return hasDoubleChance;
    }
    
    public void setHasDoubleChance(Boolean hasDoubleChance) {
        this.hasDoubleChance = hasDoubleChance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ForecastType forecastType = (ForecastType) o;
        if(forecastType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, forecastType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ForecastType{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", sumOfCodes='" + sumOfCodes + "'" +
            ", withHandicapA='" + withHandicapA + "'" +
            ", withHandicapB='" + withHandicapB + "'" +
            ", hasLimit='" + hasLimit + "'" +
            ", hasDoubleChance='" + hasDoubleChance + "'" +
            '}';
    }
}
