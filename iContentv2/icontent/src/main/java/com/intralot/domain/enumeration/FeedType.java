package com.intralot.domain.enumeration;

public enum FeedType {
	fullyPassThrough, passThrough, manual

}
