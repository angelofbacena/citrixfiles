package com.intralot.domain;

import java.util.Set;

public class EquationPart {
	
	private Set<Mark> marks;
	private Integer equationPartOrder;
	private Set<Constant> constants;
	private String sign;

	public Set<Mark> getMarks() {
		return marks;
	}

	public void setMarks(Set<Mark> marks) {
		this.marks = marks;
	}

	public Integer getEquationPartOrder() {
		return equationPartOrder;
	}

	public void setEquationPartOrder(Integer equationPartOrder) {
		this.equationPartOrder = equationPartOrder;
	}

	public Set<Constant> getConstants() {
		return constants;
	}

	public void setConstants(Set<Constant> constants) {
		this.constants = constants;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	

}
