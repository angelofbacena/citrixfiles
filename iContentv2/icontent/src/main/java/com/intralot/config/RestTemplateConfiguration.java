package com.intralot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {
	
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@Bean
	public RestTemplate getLexiconRestTemplate() {
		RestTemplate lexiconRestTemplate = new RestTemplate();
		org.springframework.http.client.SimpleClientHttpRequestFactory rf =
			    (org.springframework.http.client.SimpleClientHttpRequestFactory) lexiconRestTemplate.getRequestFactory();
		rf.setReadTimeout(applicationProperties.getLexiconRest().getReadTimeoutInSeconds() * 1000);
		rf.setConnectTimeout(applicationProperties.getLexiconRest().getConnectTimeoutInSeconds() * 1000);
		
		return lexiconRestTemplate;
	}

}
