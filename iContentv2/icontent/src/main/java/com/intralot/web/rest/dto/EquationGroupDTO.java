package com.intralot.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import com.intralot.domain.Equation;


/**
 * A DTO for the EquationGroup entity.
 */
public class EquationGroupDTO implements Serializable {

    private String id;

    private String description;

    private Set<Equation> equations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public Set<Equation> getEquations() {
		return equations;
	}

	public void setEquations(Set<Equation> equations) {
		this.equations = equations;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EquationGroupDTO equationGroupDTO = (EquationGroupDTO) o;

        if ( ! Objects.equals(id, equationGroupDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
	public String toString() {
		return "EquationGroupDTO [id=" + id + ", description=" + description
				+ ", equations=" + equations + "]";
	}
}
