package com.intralot.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.MarketType;
import com.intralot.domain.User;
import com.intralot.domain.enumeration.MarketSide;
import com.intralot.service.MarketTypeService;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;
import com.intralot.web.rest.dto.ManagedUserDTO;
import com.intralot.web.rest.dto.MarketTypeDTO;
import com.intralot.web.rest.mapper.MarketTypeMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing MarketType.
 */
@RestController
@RequestMapping("/api")
public class MarketTypeController {

    private final Logger log = LoggerFactory.getLogger(MarketTypeController.class);
        
    @Inject
    private MarketTypeService marketTypeService;
    
    @Inject
    private MarketTypeMapper marketTypeMapper;
    
    /**
     * POST  /marketTypes -> Create a new marketType.
     */
    @RequestMapping(value = "/marketTypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MarketTypeDTO> createMarketType(@RequestBody MarketTypeDTO marketTypeDTO) throws URISyntaxException {
        log.debug("REST request to save MarketType : {}", marketTypeDTO);
        if (marketTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("marketType", "idexists", "A new marketType cannot already have an ID")).body(null);
        }
        MarketTypeDTO result = marketTypeService.save(marketTypeDTO);
        return ResponseEntity.created(new URI("/api/marketTypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("marketType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /marketTypes -> Updates an existing marketType.
     */
    @RequestMapping(value = "/marketTypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MarketTypeDTO> updateMarketType(@RequestBody MarketTypeDTO marketTypeDTO) throws URISyntaxException {
        log.debug("REST request to update MarketType : {}", marketTypeDTO);
        if (marketTypeDTO.getId() == null) {
            return createMarketType(marketTypeDTO);
        }
        MarketTypeDTO result = marketTypeService.save(marketTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("marketType", marketTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /marketTypes -> get all the marketTypes.
     */
    @RequestMapping(value = "/marketTypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MarketTypeDTO>> getAllMarketTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MarketTypes");
        Page<MarketType> page = marketTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/marketTypes");
        return new ResponseEntity<>(page.getContent().stream()
            .map(marketTypeMapper::marketTypeToMarketTypeDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /marketTypes/:id -> get the "id" marketType.
     */
    @RequestMapping(value = "/marketTypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MarketTypeDTO> getMarketType(@PathVariable String id) {
        log.debug("REST request to get MarketType : {}", id);
        MarketTypeDTO marketTypeDTO = marketTypeService.findOne(id);
        return Optional.ofNullable(marketTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /marketTypes/:id -> delete the "id" marketType.
     */
    @RequestMapping(value = "/marketTypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteMarketType(@PathVariable String id) {
        log.debug("REST request to delete MarketType : {}", id);
        marketTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("marketType", id.toString())).build();
    }
    
    /**
     * GET  /marketSides -> get all marketSides.
     */
    @RequestMapping(value = "/marketSides",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarketSide> getAllMarketSides() {
        return new ArrayList<MarketSide>(Arrays.asList(MarketSide.values()));
    }
    
}
