package com.intralot.web.rest.mapper;

import com.intralot.domain.*;
import com.intralot.web.rest.dto.ForecastTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ForecastType and its DTO ForecastTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ForecastTypeMapper {

    ForecastTypeDTO forecastTypeToForecastTypeDTO(ForecastType forecastType);

    ForecastType forecastTypeDTOToForecastType(ForecastTypeDTO forecastTypeDTO);
}
