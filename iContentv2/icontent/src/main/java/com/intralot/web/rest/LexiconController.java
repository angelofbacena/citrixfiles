package com.intralot.web.rest;

import java.net.URISyntaxException;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.intralot.lexicon.model.Category;
import com.intralot.lexicon.model.Sport;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;
import com.intralot.service.LexiconService;

@RestController
@RequestMapping("/api/lexicon")
public class LexiconController {
	
	
	@Inject
	LexiconService lexiconService;
	
    /**
     * GET  /sports -> get all  sports.
     */
    @RequestMapping(value = "/sports",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Sport[]> getAllSports()
        throws URISyntaxException {
      return new ResponseEntity<>(lexiconService.getSports(), HttpStatus.OK);
      
    }
    
    /**
     * GET  /categories -> get categories by sport id
     */
    @RequestMapping(value = "/categories",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Category[]> getCategories(@RequestParam(value = "sportId") int sportId)
        throws URISyntaxException {
      return new ResponseEntity<>(lexiconService.getCategoriesBySportId(sportId), HttpStatus.OK);
      
    }
    
    /**
     * GET  /categories_and_trounaments -> get categories and tournaments by sport id
     */
    @RequestMapping(value = "/tournaments_bysportid",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tournament[]> getTournamentsBySportId(@RequestParam(value = "sportId") int sportId)
        throws URISyntaxException {
      return new ResponseEntity<>(lexiconService.getTournamentsBySportId(sportId), HttpStatus.OK);
      
    }
    
    
    /**
     * GET  /tournaments -> get tournaments by category id
     */
    @RequestMapping(value = "/tournaments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tournament[]> getTournaments(@RequestParam(value = "categoryId") int categoryId)
        throws URISyntaxException {
      return new ResponseEntity<>(lexiconService.getTournamentsByCategoryId(categoryId), HttpStatus.OK);
      
    }
    
   
    /**
     * GET  /teams -> get teams by description
     */
    @RequestMapping(value = "/teams",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Team[]> getTeams(@RequestParam(value = "description") String description)
        throws URISyntaxException {
      return new ResponseEntity<>(lexiconService.getTeamsByDescription(description), HttpStatus.OK);
      
    }
	

}
