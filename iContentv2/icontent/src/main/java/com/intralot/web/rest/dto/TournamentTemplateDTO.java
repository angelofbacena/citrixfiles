package com.intralot.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the TournamentTemplate entity.
 */
public class TournamentTemplateDTO implements Serializable {

    private String id;

    private String description;


    private Boolean autoPaymentSuspension;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Boolean getAutoPaymentSuspension() {
        return autoPaymentSuspension;
    }

    public void setAutoPaymentSuspension(Boolean autoPaymentSuspension) {
        this.autoPaymentSuspension = autoPaymentSuspension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TournamentTemplateDTO tournamentTemplateDTO = (TournamentTemplateDTO) o;

        if ( ! Objects.equals(id, tournamentTemplateDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TournamentTemplateDTO{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", autoPaymentSuspension='" + autoPaymentSuspension + "'" +
            '}';
    }
}
