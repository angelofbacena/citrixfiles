/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.intralot.web.rest.dto;
