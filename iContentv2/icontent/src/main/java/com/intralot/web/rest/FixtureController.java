package com.intralot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.Fixture;
import com.intralot.exceptions.DuplicateFixtureException;
import com.intralot.service.FixtureService;
import com.intralot.web.rest.dto.FixtureDTO;
import com.intralot.web.rest.mapper.FixtureMapper;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Fixture.
 */
@RestController
@RequestMapping("/api")
public class FixtureController {

	private final Logger log = LoggerFactory.getLogger(FixtureController.class);

	@Inject
	private FixtureService fixtureService;

	@Inject
	private FixtureMapper fixtureMapper;

	/**
	 * POST /fixtures -> Create a new fixture.
	 */
	@RequestMapping(value = "/fixtures", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<FixtureDTO> createFixture(@RequestBody FixtureDTO fixtureDTO) throws URISyntaxException {
		log.debug("REST request to save Fixture : {}", fixtureDTO);
		if (fixtureDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("fixture", "idexists", "A new fixture cannot already have an ID"))
					.body(null);
		}
		FixtureDTO result = null;
		try {
			result = fixtureService.saveOrUpdate(fixtureDTO);
		} catch (DuplicateFixtureException e) {
			log.warn("Duplicate Fixture: {}", fixtureDTO);
			return ResponseEntity.badRequest().body(fixtureDTO);

		}
		return ResponseEntity.created(new URI("/api/fixtures/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("fixture", result.getId().toString())).body(result);
	}

	/**
	 * PUT /fixtures -> Updates an existing fixture.
	 */
	@RequestMapping(value = "/fixtures", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<FixtureDTO> updateFixture(@RequestBody FixtureDTO fixtureDTO) throws URISyntaxException {
		log.debug("REST request to update Fixture : {}", fixtureDTO);
		if (fixtureDTO.getId() == null) {
			return createFixture(fixtureDTO);
		}
		FixtureDTO result = null;
		try {
			result = fixtureService.saveOrUpdate(fixtureDTO);
		} catch (DuplicateFixtureException e) {
			log.warn("Duplicate Fixture: {}", fixtureDTO);
			return ResponseEntity.badRequest().body(fixtureDTO);
		}

		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("fixture", fixtureDTO.getId().toString()))
				.body(result);
	}



	/**
	 * GET /fixtures/:id -> get the "id" fixture.
	 */
	@RequestMapping(value = "/fixtures/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<FixtureDTO> getFixture(@PathVariable String id) {
		log.debug("REST request to get Fixture : {}", id);
		FixtureDTO fixtureDTO = fixtureService.findOne(id);
		return Optional.ofNullable(fixtureDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /fixtures/:id -> delete the "id" fixture.
	 */
	@RequestMapping(value = "/fixtures/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteFixture(@PathVariable String id) {
		log.debug("REST request to delete Fixture : {}", id);
		fixtureService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("fixture", id.toString())).build();
	}

	/**
	 * GET /fixtures -> get fixtures by date range and tournament
	 * @throws URISyntaxException 
	 */
	@RequestMapping(value = "/fixtures", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<FixtureDTO>> getFixturesByDatesAndTournamentId(
			@RequestParam(value = "fromDate") @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime fromDate,
			@RequestParam(value = "toDate") @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime toDate,
			@RequestParam(value = "tournamentId") Integer[] tournamentIds, Pageable pageable
			) throws URISyntaxException {
	 
		log.debug("REST request to get a page of Fixtures by dates and tournamentid:");
		Page<Fixture> page = fixtureService.findByDatesAndTournamentIds(fromDate, toDate, tournamentIds,pageable);
		HttpHeaders headers = null;
		if (page!=null) {
			  headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fixtures");
		}
		return new ResponseEntity<>(page.getContent().stream().map(fixtureMapper::fixtureToFixtureDTO)
				.collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
	}

}
