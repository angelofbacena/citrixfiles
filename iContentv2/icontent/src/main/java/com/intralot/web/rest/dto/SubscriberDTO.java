package com.intralot.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Subscriber entity.
 */
public class SubscriberDTO implements Serializable {

    private String id;

    @NotNull
    private String description;


    @NotNull
    private String endpoint;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubscriberDTO subscriberDTO = (SubscriberDTO) o;

        if ( ! Objects.equals(id, subscriberDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SubscriberDTO{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", endpoint='" + endpoint + "'" +
            '}';
    }
}
