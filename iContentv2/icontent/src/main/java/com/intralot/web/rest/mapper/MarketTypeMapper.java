package com.intralot.web.rest.mapper;

import com.intralot.domain.*;
import com.intralot.web.rest.dto.MarketTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MarketType and its DTO MarketTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MarketTypeMapper {

    MarketTypeDTO marketTypeToMarketTypeDTO(MarketType marketType);

    MarketType marketTypeDTOToMarketType(MarketTypeDTO marketTypeDTO);
}
