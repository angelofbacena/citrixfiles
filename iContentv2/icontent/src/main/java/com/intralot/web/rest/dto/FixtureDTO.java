package com.intralot.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import com.intralot.domain.enumeration.EventStatus;
import com.intralot.lexicon.model.Team;

/**
 * A DTO for the Fixture entity.
 */
public class FixtureDTO implements Serializable {

    private String id;

    private ZonedDateTime activationDate;
    
    private Team homeTeam;
    
    private Team awayTeam;

    private ZonedDateTime kickOff;

    private ZonedDateTime endDate;

    private EventStatus status;


    public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public ZonedDateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(ZonedDateTime activationDate) {
        this.activationDate = activationDate;
    }
    public ZonedDateTime getKickOff() {
        return kickOff;
    }

    public void setKickOff(ZonedDateTime kickOff) {
        this.kickOff = kickOff;
    }
    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }
    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FixtureDTO fixtureDTO = (FixtureDTO) o;

        if ( ! Objects.equals(id, fixtureDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FixtureDTO{" +
            "id=" + id +
            ", activationDate='" + activationDate + "'" +
            ", kickOff='" + kickOff + "'" +
            ", endDate='" + endDate + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
