package com.intralot.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.TournamentTemplate;
import com.intralot.service.TournamentTemplateService;
import com.intralot.web.rest.dto.TournamentTemplateDTO;
import com.intralot.web.rest.dto.TournamentTemplateOverviewDTO;
import com.intralot.web.rest.mapper.TournamentTemplateMapper;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;

/**
 * REST controller for managing TournamentTemplate.
 */
@RestController
@RequestMapping("/api")
public class TournamentTemplateController {

    private final Logger log = LoggerFactory.getLogger(TournamentTemplateController.class);
        
    @Inject
    private TournamentTemplateService tournamentTemplateService;
    
    @Inject
    private TournamentTemplateMapper tournamentTemplateMapper;
    
    /**
     * POST  /tournamentTemplates -> Create a new tournamentTemplate.
     */
    @RequestMapping(value = "/tournamentTemplates",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TournamentTemplateDTO> createTournamentTemplate(@RequestBody TournamentTemplateDTO tournamentTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save TournamentTemplate : {}", tournamentTemplateDTO);
        if (tournamentTemplateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tournamentTemplate", "idexists", "A new tournamentTemplate cannot already have an ID")).body(null);
        }
        TournamentTemplateDTO result = tournamentTemplateService.save(tournamentTemplateDTO);
        return ResponseEntity.created(new URI("/api/tournamentTemplates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tournamentTemplate", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tournamentTemplates -> Updates an existing tournamentTemplate.
     */
    @RequestMapping(value = "/tournamentTemplates",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TournamentTemplateDTO> updateTournamentTemplate(@RequestBody TournamentTemplateDTO tournamentTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update TournamentTemplate : {}", tournamentTemplateDTO);
        if (tournamentTemplateDTO.getId() == null) {
            return createTournamentTemplate(tournamentTemplateDTO);
        }
        TournamentTemplateDTO result = tournamentTemplateService.save(tournamentTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tournamentTemplate", tournamentTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tournamentTemplates -> get all the tournamentTemplates.
     */
    @RequestMapping(value = "/tournamentTemplates",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TournamentTemplateDTO>> getAllTournamentTemplates(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TournamentTemplates");
        Page<TournamentTemplate> page = tournamentTemplateService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tournamentTemplates");
        return new ResponseEntity<>(page.getContent().stream()
            .map(tournamentTemplateMapper::tournamentTemplateToTournamentTemplateDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }
    
    /**
     * GET  /tournamentTemplatesOverview -> get all the tournamentTemplates.
     */
    @RequestMapping(value = "/tournamentTemplatesOverview",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<TournamentTemplateOverviewDTO>> getAllTournamentTemplatesOverview(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TournamentTemplatesOverview");
        Page<TournamentTemplate> page = tournamentTemplateService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tournamentTemplates");
        return new ResponseEntity<>(page.getContent().stream()
            .map(tournamentTemplateMapper::tournamentTemplateToTournamentTemplateOverviewDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /tournamentTemplates/:id -> get the "id" tournamentTemplate.
     */
    @RequestMapping(value = "/tournamentTemplates/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TournamentTemplateDTO> getTournamentTemplate(@PathVariable String id) {
        log.debug("REST request to get TournamentTemplate : {}", id);
        TournamentTemplateDTO tournamentTemplateDTO = tournamentTemplateService.findOne(id);
        return Optional.ofNullable(tournamentTemplateDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tournamentTemplates/:id -> delete the "id" tournamentTemplate.
     */
    @RequestMapping(value = "/tournamentTemplates/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTournamentTemplate(@PathVariable String id) {
        log.debug("REST request to delete TournamentTemplate : {}", id);
        tournamentTemplateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tournamentTemplate", id.toString())).build();
    }
}
