package com.intralot.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.EquationGroup;
import com.intralot.service.EquationGroupService;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;
import com.intralot.web.rest.dto.EquationGroupDTO;
import com.intralot.web.rest.mapper.EquationGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing EquationGroup.
 */
@RestController
@RequestMapping("/api")
public class EquationGroupController {

    private final Logger log = LoggerFactory.getLogger(EquationGroupController.class);
        
    @Inject
    private EquationGroupService equationGroupService;
    
    @Inject
    private EquationGroupMapper equationGroupMapper;
    
    /**
     * POST  /equationGroups -> Create a new equationGroup.
     */
    @RequestMapping(value = "/equationGroups",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EquationGroupDTO> createEquationGroup(@RequestBody EquationGroupDTO equationGroupDTO) throws URISyntaxException {
        log.debug("REST request to save EquationGroup : {}", equationGroupDTO);
        if (equationGroupDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("equationGroup", "idexists", "A new equationGroup cannot already have an ID")).body(null);
        }
        EquationGroupDTO result = equationGroupService.save(equationGroupDTO);
        return ResponseEntity.created(new URI("/api/equationGroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("equationGroup", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /equationGroups -> Updates an existing equationGroup.
     */
    @RequestMapping(value = "/equationGroups",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EquationGroupDTO> updateEquationGroup(@RequestBody EquationGroupDTO equationGroupDTO) throws URISyntaxException {
        log.debug("REST request to update EquationGroup : {}", equationGroupDTO);
        if (equationGroupDTO.getId() == null) {
            return createEquationGroup(equationGroupDTO);
        }
        EquationGroupDTO result = equationGroupService.save(equationGroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("equationGroup", equationGroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /equationGroups -> get all the equationGroups.
     */
    @RequestMapping(value = "/equationGroups",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<EquationGroupDTO>> getAllEquationGroups(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of EquationGroups");
        Page<EquationGroup> page = equationGroupService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/equationGroups");
        return new ResponseEntity<>(page.getContent().stream()
            .map(equationGroupMapper::equationGroupToEquationGroupDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /equationGroups/:id -> get the "id" equationGroup.
     */
    @RequestMapping(value = "/equationGroups/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EquationGroupDTO> getEquationGroup(@PathVariable String id) {
        log.debug("REST request to get EquationGroup : {}", id);
        EquationGroupDTO equationGroupDTO = equationGroupService.findOne(id);
        return Optional.ofNullable(equationGroupDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /equationGroups/:id -> delete the "id" equationGroup.
     */
    @RequestMapping(value = "/equationGroups/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteEquationGroup(@PathVariable String id) {
        log.debug("REST request to delete EquationGroup : {}", id);
        equationGroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("equationGroup", id.toString())).build();
    }
}
