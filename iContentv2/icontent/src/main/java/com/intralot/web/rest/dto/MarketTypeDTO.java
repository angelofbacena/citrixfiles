package com.intralot.web.rest.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.intralot.domain.CodeType;
import com.intralot.domain.ForecastType;
import com.intralot.domain.enumeration.DerivativeType;

/**
 * A DTO for the MarketType entity.
 */
public class MarketTypeDTO implements Serializable {

    private String id;

    private String description;


    private Boolean cancelledByCodes;


    private Boolean basicDisplay;


    private Boolean masterSelection;


    private DerivativeType derivativeType;
    
    
    private ForecastType forecastType;

    
    private List<CodeType> codeTypes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Boolean getCancelledByCodes() {
        return cancelledByCodes;
    }

    public void setCancelledByCodes(Boolean cancelledByCodes) {
        this.cancelledByCodes = cancelledByCodes;
    }
    public Boolean getBasicDisplay() {
        return basicDisplay;
    }

    public void setBasicDisplay(Boolean basicDisplay) {
        this.basicDisplay = basicDisplay;
    }
    public Boolean getMasterSelection() {
        return masterSelection;
    }

    public void setMasterSelection(Boolean masterSelection) {
        this.masterSelection = masterSelection;
    }
    public DerivativeType getDerivativeType() {
        return derivativeType;
    }

    public void setDerivativeType(DerivativeType derivativeType) {
        this.derivativeType = derivativeType;
    }

    public ForecastType getForecastType() {
		return forecastType;
	}

	public void setForecastType(ForecastType forecastType) {
		this.forecastType = forecastType;
	}

	
	public List<CodeType> getCodeTypes() {
		return codeTypes;
	}

	public void setCodeTypes(List<CodeType> codeTypes) {
		this.codeTypes = codeTypes;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MarketTypeDTO marketTypeDTO = (MarketTypeDTO) o;

        if ( ! Objects.equals(id, marketTypeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MarketTypeDTO{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", cancelledByCodes='" + cancelledByCodes + "'" +
            ", basicDisplay='" + basicDisplay + "'" +
            ", masterSelection='" + masterSelection + "'" +
            ", derivativeType='" + derivativeType + "'" +
            '}';
    }
}
