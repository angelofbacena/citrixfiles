package com.intralot.web.websocket;

import java.security.Principal;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class EchoWebSocket    {
	
	private static final Logger log = LoggerFactory.getLogger(ActivityWebSocket.class);
	
	@Inject
    SimpMessageSendingOperations messagingTemplate;

    @SubscribeMapping("/echo/message")
    @SendTo("/topic/room")
    public Object sendActivity(@Payload Map<String,String> message, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        
        log.debug("Sending message {}", message);
        return message;
    }

   

}
