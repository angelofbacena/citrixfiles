package com.intralot.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.Subscriber;
import com.intralot.service.SubscriberService;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;
import com.intralot.web.rest.dto.SubscriberDTO;
import com.intralot.web.rest.mapper.SubscriberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Subscriber.
 */
@RestController
@RequestMapping("/api")
public class SubscriberResource {

    private final Logger log = LoggerFactory.getLogger(SubscriberResource.class);
        
    @Inject
    private SubscriberService subscriberService;
    
    @Inject
    private SubscriberMapper subscriberMapper;
    
    /**
     * POST  /subscribers -> Create a new subscriber.
     */
    @RequestMapping(value = "/subscribers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubscriberDTO> createSubscriber(@Valid @RequestBody SubscriberDTO subscriberDTO) throws URISyntaxException {
        log.debug("REST request to save Subscriber : {}", subscriberDTO);
        if (subscriberDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("subscriber", "idexists", "A new subscriber cannot already have an ID")).body(null);
        }
        SubscriberDTO result = subscriberService.save(subscriberDTO);
        return ResponseEntity.created(new URI("/api/subscribers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("subscriber", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subscribers -> Updates an existing subscriber.
     */
    @RequestMapping(value = "/subscribers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubscriberDTO> updateSubscriber(@Valid @RequestBody SubscriberDTO subscriberDTO) throws URISyntaxException {
        log.debug("REST request to update Subscriber : {}", subscriberDTO);
        if (subscriberDTO.getId() == null) {
            return createSubscriber(subscriberDTO);
        }
        SubscriberDTO result = subscriberService.save(subscriberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("subscriber", subscriberDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subscribers -> get all the subscribers.
     */
    @RequestMapping(value = "/subscribers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SubscriberDTO>> getAllSubscribers(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Subscribers");
        Page<Subscriber> page = subscriberService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subscribers");
        return new ResponseEntity<>(page.getContent().stream()
            .map(subscriberMapper::subscriberToSubscriberDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /subscribers/:id -> get the "id" subscriber.
     */
    @RequestMapping(value = "/subscribers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SubscriberDTO> getSubscriber(@PathVariable String id) {
        log.debug("REST request to get Subscriber : {}", id);
        SubscriberDTO subscriberDTO = subscriberService.findOne(id);
        return Optional.ofNullable(subscriberDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /subscribers/:id -> delete the "id" subscriber.
     */
    @RequestMapping(value = "/subscribers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSubscriber(@PathVariable String id) {
        log.debug("REST request to delete Subscriber : {}", id);
        subscriberService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("subscriber", id.toString())).build();
    }
}
