package com.intralot.web.rest.mapper;

import com.intralot.domain.*;
import com.intralot.web.rest.dto.EquationGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EquationGroup and its DTO EquationGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EquationGroupMapper {

    EquationGroupDTO equationGroupToEquationGroupDTO(EquationGroup equationGroup);

    EquationGroup equationGroupDTOToEquationGroup(EquationGroupDTO equationGroupDTO);
}
