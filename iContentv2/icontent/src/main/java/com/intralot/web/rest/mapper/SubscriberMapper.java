package com.intralot.web.rest.mapper;

import com.intralot.domain.*;
import com.intralot.web.rest.dto.SubscriberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Subscriber and its DTO SubscriberDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubscriberMapper {

    SubscriberDTO subscriberToSubscriberDTO(Subscriber subscriber);

    Subscriber subscriberDTOToSubscriber(SubscriberDTO subscriberDTO);
}
