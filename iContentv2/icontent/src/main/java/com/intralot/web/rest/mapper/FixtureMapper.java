package com.intralot.web.rest.mapper;

import com.intralot.domain.*;
import com.intralot.web.rest.dto.FixtureDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Fixture and its DTO FixtureDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FixtureMapper {

    FixtureDTO fixtureToFixtureDTO(Fixture fixture);

    Fixture fixtureDTOToFixture(FixtureDTO fixtureDTO);
}
