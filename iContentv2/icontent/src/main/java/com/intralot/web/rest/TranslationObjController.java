package com.intralot.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.TranslationObj;
import com.intralot.service.TranslationObjService;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TranslationObj.
 */
@RestController
@RequestMapping("/api")
public class TranslationObjController {

    private final Logger log = LoggerFactory.getLogger(TranslationObjController.class);
        
    @Inject
    private TranslationObjService translationObjService;
    
    /**
     * POST  /translationObjs -> Create a new translationObj.
     */
    @RequestMapping(value = "/translationObjs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TranslationObj> createTranslationObj(@RequestBody TranslationObj translationObj) throws URISyntaxException {
        log.debug("REST request to save TranslationObj : {}", translationObj);
        if (translationObj.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("translationObj", "idexists", "A new translationObj cannot already have an ID")).body(null);
        }
        TranslationObj result = translationObjService.save(translationObj);
        return ResponseEntity.created(new URI("/api/translationObjs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("translationObj", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /translationObjs -> Updates an existing translationObj.
     */
    @RequestMapping(value = "/translationObjs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TranslationObj> updateTranslationObj(@RequestBody TranslationObj translationObj) throws URISyntaxException {
        log.debug("REST request to update TranslationObj : {}", translationObj);
        if (translationObj.getId() == null) {
            return createTranslationObj(translationObj);
        }
        TranslationObj result = translationObjService.save(translationObj);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("translationObj", translationObj.getId().toString()))
            .body(result);
    }

    /**
     * GET  /translationObjs -> get all the translationObjs.
     */
    @RequestMapping(value = "/translationObjs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TranslationObj>> getAllTranslationObjs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TranslationObjs");
        Page<TranslationObj> page = translationObjService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/translationObjs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /translationObjs/:id -> get the "id" translationObj.
     */
    @RequestMapping(value = "/translationObjs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TranslationObj> getTranslationObj(@PathVariable String id) {
        log.debug("REST request to get TranslationObj : {}", id);
        TranslationObj translationObj = translationObjService.findOne(id);
        return Optional.ofNullable(translationObj)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /translationObjs/:id -> delete the "id" translationObj.
     */
    @RequestMapping(value = "/translationObjs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTranslationObj(@PathVariable String id) {
        log.debug("REST request to delete TranslationObj : {}", id);
        translationObjService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("translationObj", id.toString())).build();
    }
}
