package com.intralot.web.rest.mapper;

import org.mapstruct.Mapper;

import com.intralot.domain.TournamentTemplate;
import com.intralot.web.rest.dto.TournamentTemplateDTO;
import com.intralot.web.rest.dto.TournamentTemplateOverviewDTO;

/**
 * Mapper for the entity TournamentTemplate and its DTO TournamentTemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TournamentTemplateMapper {

    TournamentTemplateDTO tournamentTemplateToTournamentTemplateDTO(TournamentTemplate tournamentTemplate);
    
    TournamentTemplateOverviewDTO tournamentTemplateToTournamentTemplateOverviewDTO(TournamentTemplate tournamentTemplate);

    TournamentTemplate tournamentTemplateDTOToTournamentTemplate(TournamentTemplateDTO tournamentTemplateDTO);
}
