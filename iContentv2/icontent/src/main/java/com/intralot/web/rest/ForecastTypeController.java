package com.intralot.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.intralot.domain.ForecastType;
import com.intralot.service.ForecastTypeService;
import com.intralot.web.rest.util.HeaderUtil;
import com.intralot.web.rest.util.PaginationUtil;
import com.intralot.web.rest.dto.ForecastTypeDTO;
import com.intralot.web.rest.mapper.ForecastTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ForecastType.
 */
@RestController
@RequestMapping("/api")
public class ForecastTypeController {

    private final Logger log = LoggerFactory.getLogger(ForecastTypeController.class);
        
    @Inject
    private ForecastTypeService forecastTypeService;
    
    @Inject
    private ForecastTypeMapper forecastTypeMapper;
    
    /**
     * POST  /forecastTypes -> Create a new forecastType.
     */
    @RequestMapping(value = "/forecastTypes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ForecastTypeDTO> createForecastType(@RequestBody ForecastTypeDTO forecastTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ForecastType : {}", forecastTypeDTO);
        if (forecastTypeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("forecastType", "idexists", "A new forecastType cannot already have an ID")).body(null);
        }
        ForecastTypeDTO result = forecastTypeService.save(forecastTypeDTO);
        return ResponseEntity.created(new URI("/api/forecastTypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("forecastType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /forecastTypes -> Updates an existing forecastType.
     */
    @RequestMapping(value = "/forecastTypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ForecastTypeDTO> updateForecastType(@RequestBody ForecastTypeDTO forecastTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ForecastType : {}", forecastTypeDTO);
        if (forecastTypeDTO.getId() == null) {
            return createForecastType(forecastTypeDTO);
        }
        ForecastTypeDTO result = forecastTypeService.save(forecastTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("forecastType", forecastTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /forecastTypes -> get all the forecastTypes.
     */
    @RequestMapping(value = "/forecastTypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ForecastTypeDTO>> getAllForecastTypes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ForecastTypes");
        Page<ForecastType> page = forecastTypeService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/forecastTypes");
        return new ResponseEntity<>(page.getContent().stream()
            .map(forecastTypeMapper::forecastTypeToForecastTypeDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /forecastTypes/:id -> get the "id" forecastType.
     */
    @RequestMapping(value = "/forecastTypes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ForecastTypeDTO> getForecastType(@PathVariable String id) {
        log.debug("REST request to get ForecastType : {}", id);
        ForecastTypeDTO forecastTypeDTO = forecastTypeService.findOne(id);
        return Optional.ofNullable(forecastTypeDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /forecastTypes/:id -> delete the "id" forecastType.
     */
    @RequestMapping(value = "/forecastTypes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteForecastType(@PathVariable String id) {
        log.debug("REST request to delete ForecastType : {}", id);
        forecastTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("forecastType", id.toString())).build();
    }
}
