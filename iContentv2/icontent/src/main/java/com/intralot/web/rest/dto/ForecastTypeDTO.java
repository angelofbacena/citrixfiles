package com.intralot.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the ForecastType entity.
 */
public class ForecastTypeDTO implements Serializable {

    private String id;

    private String description;


    private Integer sumOfCodes;


    private Boolean withHandicapA;


    private Boolean withHandicapB;


    private Boolean hasLimit;


    private Boolean hasDoubleChance;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getSumOfCodes() {
        return sumOfCodes;
    }

    public void setSumOfCodes(Integer sumOfCodes) {
        this.sumOfCodes = sumOfCodes;
    }
    public Boolean getWithHandicapA() {
        return withHandicapA;
    }

    public void setWithHandicapA(Boolean withHandicapA) {
        this.withHandicapA = withHandicapA;
    }
    public Boolean getWithHandicapB() {
        return withHandicapB;
    }

    public void setWithHandicapB(Boolean withHandicapB) {
        this.withHandicapB = withHandicapB;
    }
    public Boolean getHasLimit() {
        return hasLimit;
    }

    public void setHasLimit(Boolean hasLimit) {
        this.hasLimit = hasLimit;
    }
    public Boolean getHasDoubleChance() {
        return hasDoubleChance;
    }

    public void setHasDoubleChance(Boolean hasDoubleChance) {
        this.hasDoubleChance = hasDoubleChance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ForecastTypeDTO forecastTypeDTO = (ForecastTypeDTO) o;

        if ( ! Objects.equals(id, forecastTypeDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ForecastTypeDTO{" +
            "id=" + id +
            ", description='" + description + "'" +
            ", sumOfCodes='" + sumOfCodes + "'" +
            ", withHandicapA='" + withHandicapA + "'" +
            ", withHandicapB='" + withHandicapB + "'" +
            ", hasLimit='" + hasLimit + "'" +
            ", hasDoubleChance='" + hasDoubleChance + "'" +
            '}';
    }
}
