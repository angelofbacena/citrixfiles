package com.intralot.web.rest.dto;

import java.util.Objects;

public class TournamentTemplateOverviewDTO {

	private String id;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TournamentTemplateOverviewDTO tournamentTemplateDTO = (TournamentTemplateOverviewDTO) o;

		if (!Objects.equals(id, tournamentTemplateDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "TournamentTemplateDTO{" + "id=" + id + ", description='" + description + "'" + '}';
	}
}
