// package com.intralot.jms;

// import javax.jms.ConnectionFactory;
// import javax.jms.JMSException;

// import org.apache.activemq.command.ActiveMQObjectMessage;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Profile;
// import org.springframework.jms.annotation.JmsListener;
// import org.springframework.jms.config.JmsListenerContainerFactory;
// import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
// import org.springframework.stereotype.Component;

// import com.intralot.config.ApplicationProperties;
// import com.intralot.config.Constants;
// import com.intralot.iflex.commons.provider.betradar.BetradarBetData;
// import com.intralot.service.FeedService;


// @Profile("!" + Constants.SPRING_PROFILE_TEST)
// @Component
// public class BetradarFeedJMSListener {
// 	private final Logger log = LoggerFactory.getLogger(BetradarFeedJMSListener.class);

//     @Autowired
//     ApplicationProperties applicationProperties;
    
//     @Autowired
//     FeedService betradarFeedService;
//     @Profile("!" + Constants.SPRING_PROFILE_FAST)
// 	@Bean
// 	JmsListenerContainerFactory<?> jmsContainerFactory(ConnectionFactory connectionFactory) {

// 		SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
// 		factory.setConnectionFactory(connectionFactory);
// 		factory.setClientId(applicationProperties.getJms().getClientId());
// 		factory.setPubSubDomain(applicationProperties.getJms().isPubSubDomain());
// 		factory.setSubscriptionDurable(applicationProperties.getJms().isSubscriptionDurable());
		
// 		return factory;
// 	}
//     @Profile("!" + Constants.SPRING_PROFILE_FAST)
// 	@JmsListener(id = "iContent",  destination = "betradarFixtures", containerFactory = "jmsContainerFactory", subscription = "iContent")
// 	public void receiveOrder(Object obj) {
// 		ActiveMQObjectMessage amqObj = (ActiveMQObjectMessage)obj;
		
// 		BetradarBetData bbd;
// 		try {
// 			bbd = (BetradarBetData)amqObj.getObject();
// 			log.info(bbd.toString());
// 			log.info("Sending ...");
// 			betradarFeedService.tranlateFeedAndCreateFixtures(bbd);
// 			log.info("Sent ...");
// 		} catch (JMSException e) {
// 			e.printStackTrace();
// 		}
		
		
		
// 	}

// }
