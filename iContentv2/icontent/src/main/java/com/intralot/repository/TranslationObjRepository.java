package com.intralot.repository;

import com.intralot.domain.TranslationObj;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TranslationObj entity.
 */
public interface TranslationObjRepository extends MongoRepository<TranslationObj,String> {

}
