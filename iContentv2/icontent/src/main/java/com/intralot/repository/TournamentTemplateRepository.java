package com.intralot.repository;

import com.intralot.domain.TournamentTemplate;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TournamentTemplate entity.
 */
public interface TournamentTemplateRepository extends MongoRepository<TournamentTemplate,String> {

}
