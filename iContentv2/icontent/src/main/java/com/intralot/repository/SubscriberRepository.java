package com.intralot.repository;

import com.intralot.domain.Subscriber;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Subscriber entity.
 */
public interface SubscriberRepository extends MongoRepository<Subscriber,String> {

}
