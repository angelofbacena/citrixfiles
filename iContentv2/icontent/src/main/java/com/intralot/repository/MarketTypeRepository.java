package com.intralot.repository;

import com.intralot.domain.MarketType;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the MarketType entity.
 */
public interface MarketTypeRepository extends MongoRepository<MarketType,String> {

}
