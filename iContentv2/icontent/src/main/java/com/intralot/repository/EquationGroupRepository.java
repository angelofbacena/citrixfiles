package com.intralot.repository;

import com.intralot.domain.EquationGroup;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the EquationGroup entity.
 */
public interface EquationGroupRepository extends MongoRepository<EquationGroup,String> {

}
