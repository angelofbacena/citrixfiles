package com.intralot.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.intralot.domain.Fixture;

/**
 * Spring Data MongoDB repository for the Fixture entity.
 */
public interface FixtureRepository extends MongoRepository<Fixture, String> {

	@Query(value = "{ 'kickOff' : ?0, 'homeTeam._id' : ?1, 'awayTeam._id' : ?2 }")
	List<Fixture> findByKickOffAndHomeTeamIdAndAwayTeamId(ZonedDateTime date, int homeTeamId, int awayTeamId);

	@Query("{ 'kickOff' : { $gte:  ?0 , $lte:  ?1  } }")
	Page<Fixture> findByKickOffRangeAndTournamentId(ZonedDateTime fromDate, ZonedDateTime toDate,
			Integer[] tournamentIds, Pageable pageable);

}
