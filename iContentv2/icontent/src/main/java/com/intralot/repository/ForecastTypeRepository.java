package com.intralot.repository;

import com.intralot.domain.ForecastType;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ForecastType entity.
 */
public interface ForecastTypeRepository extends MongoRepository<ForecastType,String> {

}
