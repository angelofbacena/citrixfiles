package com.intralot.exceptions;

public class DuplicateFixtureException extends Exception {

	public DuplicateFixtureException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
