package com.intralot.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.actuate.audit.AuditEvent;

public interface AuditEventService {

	List<AuditEvent> findAll();

	List<AuditEvent> findByDates(LocalDateTime fromDate, LocalDateTime toDate);

	Optional<AuditEvent> find(String id);

}