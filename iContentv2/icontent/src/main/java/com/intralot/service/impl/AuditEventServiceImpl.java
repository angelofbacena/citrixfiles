package com.intralot.service.impl;

import com.intralot.config.audit.AuditEventConverter;
import com.intralot.domain.PersistentAuditEvent;
import com.intralot.repository.PersistenceAuditEventRepository;
import com.intralot.service.AuditEventService;

import java.time.LocalDateTime;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Service for managing audit events.
 * <p/>
 * <p>
 * This is the default implementation to support SpringBoot Actuator AuditEventRepository
 * </p>
 */
@Service
public class AuditEventServiceImpl implements AuditEventService {

	
	@Inject
    PersistenceAuditEventRepository persistenceAuditEventRepository;

    @Inject
    AuditEventConverter auditEventConverter;

    /* (non-Javadoc)
	 * @see com.intralot.service.impl.AuditEventService#findAll()
	 */
    @Override
	public List<AuditEvent> findAll() {
        return auditEventConverter.convertToAuditEvent(persistenceAuditEventRepository.findAll());
    }

    /* (non-Javadoc)
	 * @see com.intralot.service.impl.AuditEventService#findByDates(java.time.LocalDateTime, java.time.LocalDateTime)
	 */
    @Override
	public List<AuditEvent> findByDates(LocalDateTime fromDate, LocalDateTime toDate) {
        List<PersistentAuditEvent> persistentAuditEvents =
            persistenceAuditEventRepository.findAllByAuditEventDateBetween(fromDate, toDate);

        return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
    }

    /* (non-Javadoc)
	 * @see com.intralot.service.impl.AuditEventService#find(java.lang.String)
	 */
    @Override
	public Optional<AuditEvent> find(String id) {
        return Optional.ofNullable(persistenceAuditEventRepository.findOne(id)).map
            (auditEventConverter::convertToAuditEvent);
    }
}
