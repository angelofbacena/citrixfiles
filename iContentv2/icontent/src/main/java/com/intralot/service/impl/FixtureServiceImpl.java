package com.intralot.service.impl;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.intralot.domain.Fixture;
import com.intralot.exceptions.DuplicateFixtureException;
import com.intralot.repository.FixtureRepository;
import com.intralot.service.FixtureService;
import com.intralot.web.rest.dto.FixtureDTO;
import com.intralot.web.rest.mapper.FixtureMapper;

/**
 * Service Implementation for managing Fixture.
 */
@Service
public class FixtureServiceImpl implements FixtureService {

	private final Logger log = LoggerFactory.getLogger(FixtureServiceImpl.class);

	@Inject
	private FixtureRepository fixtureRepository;

	@Inject
	private FixtureMapper fixtureMapper;

	/**
	 * Save a fixture.
	 * 
	 * @return the persisted entity
	 */
 

	@Override
	public FixtureDTO saveOrUpdate(FixtureDTO fixtureDTO) throws DuplicateFixtureException {
		log.debug("Request to create Fixture : {}", fixtureDTO);
		Fixture fixture = fixtureMapper.fixtureDTOToFixture(fixtureDTO);
		if (fixture.getHomeTeam()!=null && fixture.getAwayTeam()!=null ) {
			List<Fixture> existing = fixtureRepository.findByKickOffAndHomeTeamIdAndAwayTeamId(fixture.getKickOff(),
					fixture.getHomeTeam().getTeamId(), fixture.getAwayTeam().getTeamId());
			if (existing.size() == 1) {
				fixture.setId(existing.get(0).getId());
			} else if (existing.size() >1 )  {
				 throw new DuplicateFixtureException("More than one fixtures exist with same kickoff, home, away team");
			}
		}

		fixture = fixtureRepository.save(fixture);
		FixtureDTO result = fixtureMapper.fixtureToFixtureDTO(fixture);
		return result;
	}

	@Override
	public Fixture saveOrUpdate(Fixture fixture) throws DuplicateFixtureException {
		List<Fixture> existing = fixtureRepository.findByKickOffAndHomeTeamIdAndAwayTeamId(fixture.getKickOff(),
				fixture.getHomeTeam().getTeamId(), fixture.getAwayTeam().getTeamId());
		if (existing.size() == 1) {
			fixture.setId(existing.get(0).getId());
		} else if (existing.size() >1 )  {
			 throw new DuplicateFixtureException("More than one fixtures exist with same kickoff, home, away team");
		}
		return fixture = fixtureRepository.save(fixture);
	}

	/**
	 * get all the fixtures.
	 * 
	 * @return the list of entities
	 */
	public Page<Fixture> findAll(Pageable pageable) {
		log.debug("Request to get all Fixtures");
		Page<Fixture> result = fixtureRepository.findAll(pageable);
		return result;
	}

	/**
	 * get one fixture by id.
	 * 
	 * @return the entity
	 */
	public FixtureDTO findOne(String id) {
		log.debug("Request to get Fixture : {}", id);
		Fixture fixture = fixtureRepository.findOne(id);
		FixtureDTO fixtureDTO = fixtureMapper.fixtureToFixtureDTO(fixture);
		return fixtureDTO;
	}

	/**
	 * delete the fixture by id.
	 */
	public void delete(String id) {
		log.debug("Request to delete Fixture : {}", id);
		fixtureRepository.delete(id);
	}

	@Override
	public Page<Fixture> findByDatesAndTournamentIds(ZonedDateTime fromDate, ZonedDateTime toDate,
			Integer[] tournamentIds, Pageable pageable) {
		 
		return fixtureRepository.findByKickOffRangeAndTournamentId(fromDate, toDate, tournamentIds,pageable);
	}

}
