package com.intralot.service;

import com.intralot.domain.TournamentTemplate;
import com.intralot.web.rest.dto.TournamentTemplateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing TournamentTemplate.
 */
public interface TournamentTemplateService {

    /**
     * Save a tournamentTemplate.
     * @return the persisted entity
     */
    public TournamentTemplateDTO save(TournamentTemplateDTO tournamentTemplateDTO);

    /**
     *  get all the tournamentTemplates.
     *  @return the list of entities
     */
    public Page<TournamentTemplate> findAll(Pageable pageable);

    /**
     *  get the "id" tournamentTemplate.
     *  @return the entity
     */
    public TournamentTemplateDTO findOne(String id);

    /**
     *  delete the "id" tournamentTemplate.
     */
    public void delete(String id);
}
