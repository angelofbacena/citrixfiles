package com.intralot.service;

import org.springframework.scheduling.annotation.Async;

import com.intralot.domain.User;

public interface MailService {

	void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml);

	void sendActivationEmail(User user, String baseUrl);

	void sendCreationEmail(User user, String baseUrl);

	void sendPasswordResetMail(User user, String baseUrl);

}