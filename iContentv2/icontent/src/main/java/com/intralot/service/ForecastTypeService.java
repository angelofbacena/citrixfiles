package com.intralot.service;

import com.intralot.domain.ForecastType;
import com.intralot.web.rest.dto.ForecastTypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing ForecastType.
 */
public interface ForecastTypeService {

    /**
     * Save a forecastType.
     * @return the persisted entity
     */
    public ForecastTypeDTO save(ForecastTypeDTO forecastTypeDTO);

    /**
     *  get all the forecastTypes.
     *  @return the list of entities
     */
    public Page<ForecastType> findAll(Pageable pageable);

    /**
     *  get the "id" forecastType.
     *  @return the entity
     */
    public ForecastTypeDTO findOne(String id);

    /**
     *  delete the "id" forecastType.
     */
    public void delete(String id);
}
