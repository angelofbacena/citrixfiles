package com.intralot.service.impl;

import com.intralot.service.SubscriberService;
import com.intralot.domain.Subscriber;
import com.intralot.repository.SubscriberRepository;
import com.intralot.web.rest.dto.SubscriberDTO;
import com.intralot.web.rest.mapper.SubscriberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Subscriber.
 */
@Service
public class SubscriberServiceImpl implements SubscriberService{

    private final Logger log = LoggerFactory.getLogger(SubscriberServiceImpl.class);
    
    @Inject
    private SubscriberRepository subscriberRepository;
    
    @Inject
    private SubscriberMapper subscriberMapper;
    
    /**
     * Save a subscriber.
     * @return the persisted entity
     */
    public SubscriberDTO save(SubscriberDTO subscriberDTO) {
        log.debug("Request to save Subscriber : {}", subscriberDTO);
        Subscriber subscriber = subscriberMapper.subscriberDTOToSubscriber(subscriberDTO);
        subscriber = subscriberRepository.save(subscriber);
        SubscriberDTO result = subscriberMapper.subscriberToSubscriberDTO(subscriber);
        return result;
    }

    /**
     *  get all the subscribers.
     *  @return the list of entities
     */
    public Page<Subscriber> findAll(Pageable pageable) {
        log.debug("Request to get all Subscribers");
        Page<Subscriber> result = subscriberRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one subscriber by id.
     *  @return the entity
     */
    public SubscriberDTO findOne(String id) {
        log.debug("Request to get Subscriber : {}", id);
        Subscriber subscriber = subscriberRepository.findOne(id);
        SubscriberDTO subscriberDTO = subscriberMapper.subscriberToSubscriberDTO(subscriber);
        return subscriberDTO;
    }

    /**
     *  delete the  subscriber by id.
     */
    public void delete(String id) {
        log.debug("Request to delete Subscriber : {}", id);
        subscriberRepository.delete(id);
    }
}
