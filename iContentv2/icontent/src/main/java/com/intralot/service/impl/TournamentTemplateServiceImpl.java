package com.intralot.service.impl;

import com.intralot.service.TournamentTemplateService;
import com.intralot.domain.TournamentTemplate;
import com.intralot.repository.TournamentTemplateRepository;
import com.intralot.web.rest.dto.TournamentTemplateDTO;
import com.intralot.web.rest.mapper.TournamentTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TournamentTemplate.
 */
@Service
public class TournamentTemplateServiceImpl implements TournamentTemplateService{

    private final Logger log = LoggerFactory.getLogger(TournamentTemplateServiceImpl.class);
    
    @Inject
    private TournamentTemplateRepository tournamentTemplateRepository;
    
    @Inject
    private TournamentTemplateMapper tournamentTemplateMapper;
    
    /**
     * Save a tournamentTemplate.
     * @return the persisted entity
     */
    public TournamentTemplateDTO save(TournamentTemplateDTO tournamentTemplateDTO) {
        log.debug("Request to save TournamentTemplate : {}", tournamentTemplateDTO);
        TournamentTemplate tournamentTemplate = tournamentTemplateMapper.tournamentTemplateDTOToTournamentTemplate(tournamentTemplateDTO);
        tournamentTemplate = tournamentTemplateRepository.save(tournamentTemplate);
        TournamentTemplateDTO result = tournamentTemplateMapper.tournamentTemplateToTournamentTemplateDTO(tournamentTemplate);
        return result;
    }

    /**
     *  get all the tournamentTemplates.
     *  @return the list of entities
     */
    public Page<TournamentTemplate> findAll(Pageable pageable) {
        log.debug("Request to get all TournamentTemplates");
        Page<TournamentTemplate> result = tournamentTemplateRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one tournamentTemplate by id.
     *  @return the entity
     */
    public TournamentTemplateDTO findOne(String id) {
        log.debug("Request to get TournamentTemplate : {}", id);
        TournamentTemplate tournamentTemplate = tournamentTemplateRepository.findOne(id);
        TournamentTemplateDTO tournamentTemplateDTO = tournamentTemplateMapper.tournamentTemplateToTournamentTemplateDTO(tournamentTemplate);
        return tournamentTemplateDTO;
    }

    /**
     *  delete the  tournamentTemplate by id.
     */
    public void delete(String id) {
        log.debug("Request to delete TournamentTemplate : {}", id);
        tournamentTemplateRepository.delete(id);
    }
}
