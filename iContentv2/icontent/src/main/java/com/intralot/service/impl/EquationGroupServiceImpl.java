package com.intralot.service.impl;

import com.intralot.service.EquationGroupService;
import com.intralot.domain.EquationGroup;
import com.intralot.repository.EquationGroupRepository;
import com.intralot.web.rest.dto.EquationGroupDTO;
import com.intralot.web.rest.mapper.EquationGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing EquationGroup.
 */
@Service
public class EquationGroupServiceImpl implements EquationGroupService{

    private final Logger log = LoggerFactory.getLogger(EquationGroupServiceImpl.class);
    
    @Inject
    private EquationGroupRepository equationGroupRepository;
    
    @Inject
    private EquationGroupMapper equationGroupMapper;
    
    /**
     * Save a equationGroup.
     * @return the persisted entity
     */
    public EquationGroupDTO save(EquationGroupDTO equationGroupDTO) {
        log.debug("Request to save EquationGroup : {}", equationGroupDTO);
        EquationGroup equationGroup = equationGroupMapper.equationGroupDTOToEquationGroup(equationGroupDTO);
        equationGroup = equationGroupRepository.save(equationGroup);
        EquationGroupDTO result = equationGroupMapper.equationGroupToEquationGroupDTO(equationGroup);
        return result;
    }

    /**
     *  get all the equationGroups.
     *  @return the list of entities
     */
    public Page<EquationGroup> findAll(Pageable pageable) {
        log.debug("Request to get all EquationGroups");
        Page<EquationGroup> result = equationGroupRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one equationGroup by id.
     *  @return the entity
     */
    public EquationGroupDTO findOne(String id) {
        log.debug("Request to get EquationGroup : {}", id);
        EquationGroup equationGroup = equationGroupRepository.findOne(id);
        EquationGroupDTO equationGroupDTO = equationGroupMapper.equationGroupToEquationGroupDTO(equationGroup);
        return equationGroupDTO;
    }

    /**
     *  delete the  equationGroup by id.
     */
    public void delete(String id) {
        log.debug("Request to delete EquationGroup : {}", id);
        equationGroupRepository.delete(id);
    }
}
