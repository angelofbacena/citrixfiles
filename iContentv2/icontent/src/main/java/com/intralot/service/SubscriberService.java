package com.intralot.service;

import com.intralot.domain.Subscriber;
import com.intralot.web.rest.dto.SubscriberDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Subscriber.
 */
public interface SubscriberService {

    /**
     * Save a subscriber.
     * @return the persisted entity
     */
    public SubscriberDTO save(SubscriberDTO subscriberDTO);

    /**
     *  get all the subscribers.
     *  @return the list of entities
     */
    public Page<Subscriber> findAll(Pageable pageable);

    /**
     *  get the "id" subscriber.
     *  @return the entity
     */
    public SubscriberDTO findOne(String id);

    /**
     *  delete the "id" subscriber.
     */
    public void delete(String id);
}
