package com.intralot.service;

import com.intralot.domain.TranslationObj;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing TranslationObj.
 */
public interface TranslationObjService {

    /**
     * Save a translationObj.
     * @return the persisted entity
     */
    public TranslationObj save(TranslationObj translationObj);

    /**
     *  get all the translationObjs.
     *  @return the list of entities
     */
    public Page<TranslationObj> findAll(Pageable pageable);

    /**
     *  get the "id" translationObj.
     *  @return the entity
     */
    public TranslationObj findOne(String id);

    /**
     *  delete the "id" translationObj.
     */
    public void delete(String id);
}
