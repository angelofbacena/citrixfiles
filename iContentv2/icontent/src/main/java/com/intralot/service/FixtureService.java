package com.intralot.service;

import java.time.ZonedDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.intralot.domain.Fixture;
import com.intralot.exceptions.DuplicateFixtureException;
import com.intralot.web.rest.dto.FixtureDTO;

/**
 * Service Interface for managing Fixture.
 */
public interface FixtureService {

	public FixtureDTO saveOrUpdate(FixtureDTO fixtureDTO) throws DuplicateFixtureException;

	public Fixture saveOrUpdate(Fixture fixture) throws DuplicateFixtureException;

	public Page<Fixture> findAll(Pageable pageable);

	public FixtureDTO findOne(String id);

	public Page<Fixture> findByDatesAndTournamentIds(ZonedDateTime fromDate, ZonedDateTime toDate, Integer[] tournamentIds, Pageable pageable);

	public void delete(String id);

}
