package com.intralot.service.impl;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.intralot.config.ApplicationProperties;
import com.intralot.lexicon.model.Category;
import com.intralot.lexicon.model.Sport;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;
import com.intralot.service.LexiconService;

@Service
public class LexiconServiceImpl implements LexiconService {

	private final Logger log = LoggerFactory.getLogger(BetradarFeedService.class);

	@Autowired
	RestTemplate lexiconRestTemplate;

	@Autowired
	ApplicationProperties applicationProperties;

	@Override
	public Sport getSportByProviderId(int providerId, String lang) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getSportByExtIdEndpoint() + "?extId=" + providerId + "&level=1"
				+ "&lang=" + lang;

		log.info("Calling: {}", url);
		ResponseEntity<Sport> sportRE = lexiconRestTemplate.getForEntity(url, Sport.class);

		return sportRE.getBody();
	}

	@Override
	public Category getCategoryByProviderId(int providerId, String lang) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getCategoryByExtIdEndpoint() + "?extId=" + providerId
				+ "&level=1" + "&lang=" + lang;
		log.info("Calling: {}", url);
		Category c = null;
		ResponseEntity<Category[]> categoryRE = lexiconRestTemplate.getForEntity(url, Category[].class);
		if (categoryRE.getBody().length != 1) {
			log.warn("Not unique Category : {}", categoryRE);
		} else {
			c = categoryRE.getBody()[0];
		}

		return c;
	}

	@Override
	public Tournament getTournamentByProviderId(int providerId, String lang) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getTournamentByExtIdEndpoint() + "?extId=" + providerId
				+ "&level=1" + "&lang=" + lang;
		log.info("Calling: {}", url);
		Tournament t = null;
		ResponseEntity<Tournament[]> tournamentRE = lexiconRestTemplate.getForEntity(url, Tournament[].class);
		if (tournamentRE.getBody().length != 1) {
			log.warn("Not unique Tournament : {}", tournamentRE);
		} else {
			t = tournamentRE.getBody()[0];
		}

		return t;
	}

	@Override
	public Team getTeamByProviderId(int providerId, String lang) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getTeamByExtIdEndpoint() + "?extId=" + providerId + "&level=1"
				+ "&lang=" + lang;
		log.info("Calling: {}", url);
		ResponseEntity<Team> teamRE = lexiconRestTemplate.getForEntity(url, Team.class);

		return teamRE.getBody();
	}

	@Override
	public Sport[] getSports() {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getSportsByDescription() + "?description=&level=0";
		log.info("Calling: {}", url);
		ResponseEntity<Sport[]> sports = lexiconRestTemplate.getForEntity(url, Sport[].class);
		
		return sports.getBody();
	}

	@Override
	public Category[] getCategoriesBySportId(int sportId) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getCategoriesBySportId() + "?sportId="+ sportId+"&literalDescr=&level=0";
		log.info("Calling: {}", url);
		ResponseEntity<Category[]> categories = lexiconRestTemplate.getForEntity(url, Category[].class);
		return categories.getBody();
	}

	@Override
	public Tournament[] getTournamentsByCategoryId(int categoryId) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getTournamentsByCategoryId() + "?categoryId="+ categoryId+"&level=0";
		log.info("Calling: {}", url);
		ResponseEntity<Tournament[]> tournaments = lexiconRestTemplate.getForEntity(url, Tournament[].class);
		return tournaments.getBody();
	}

	@Override
	public Tournament[] getTournamentsBySportId(int sportId) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getTournamentsBySportId() + "?sportId="+ sportId +"&level=0";
		log.info("Calling: {}", url);
		ResponseEntity<Tournament[]> tournaments = lexiconRestTemplate.getForEntity(url, Tournament[].class);
		return tournaments.getBody();
	}

	@Override
	public Team[] getTeamsByDescription(String description) {
		String url = applicationProperties.getLexiconRest().getBaseUrl()
				+ applicationProperties.getLexiconRest().getTeamsByDescription() + "?description="+ description+"&level=0";
		log.info("Calling: {}", url);
		ResponseEntity<Team[]> teams = lexiconRestTemplate.getForEntity(url, Team[].class);
		return teams.getBody();
	}

}
