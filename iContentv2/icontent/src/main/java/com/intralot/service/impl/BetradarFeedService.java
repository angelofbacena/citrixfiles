package com.intralot.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.intralot.config.ApplicationProperties;
import com.intralot.domain.Fixture;
import com.intralot.domain.TranslatableEntity;
import com.intralot.domain.TranslationObj;
import com.intralot.exceptions.DuplicateFixtureException;
import com.intralot.iflex.commons.provider.betradar.BetradarBetData;
import com.intralot.iflex.commons.provider.betradar.Match;
import com.intralot.iflex.commons.provider.betradar.Text;
import com.intralot.iflex.commons.provider.betradar.Texts;
import com.intralot.lexicon.model.Category;
import com.intralot.lexicon.model.Sport;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;
import com.intralot.repository.TranslationObjRepository;
import com.intralot.service.FeedService;
import com.intralot.service.FixtureService;
import com.intralot.service.LexiconService;

@Service
public class BetradarFeedService implements FeedService {
	
	private final Logger log = LoggerFactory.getLogger(BetradarFeedService.class);

	@Autowired
	LexiconService lexiconService;
	
	@Autowired
	TranslationObjRepository translationObjRepository;
	
	@Autowired
	FixtureService fixtureService;
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@Override
	@Async
	public void tranlateFeedAndCreateFixtures(Object obj) {
		log.info("Starting...");
		BetradarBetData bbd = (BetradarBetData)obj;
		Set<TranslationObj> tos = getTranslationObjects(bbd);
		Set<Fixture> fixtures = translateFixtures(tos);
		for (Fixture f : fixtures) {
			try {
				fixtureService.saveOrUpdate(f);
			} catch (DuplicateFixtureException e) {
				log.warn("Duplicate Fixture : {}",f  );
			}
		}
		translationObjRepository.save(tos);
	}
	
	private Set<Fixture> translateFixtures(Set<TranslationObj> tos) {
		Set<Fixture> fixtures = new HashSet<>();
		for (TranslationObj to : tos) {
			Fixture f = translateFixture(to);
			if (f!=null) {
				fixtures.add(f);
			}
		}
		
		return fixtures;
	}
	 
	private Fixture translateFixture(TranslationObj to) {
		Sport s = lexiconService.getSportByProviderId(Integer.parseInt(to.getSport().getId()), to.getFeed());
		
		Category c = lexiconService.getCategoryByProviderId(Integer.parseInt(to.getCategory().getId()), to.getFeed());
		Tournament t = lexiconService.getTournamentByProviderId(Integer.parseInt(to.getTournament().getId()),to.getFeed());
		Team homeTeam = lexiconService.getTeamByProviderId(Integer.parseInt(to.getHomeTeam().getId()),to.getFeed());
		Team awayTeam = lexiconService.getTeamByProviderId(Integer.parseInt(to.getAwayTeam().getId()), to.getFeed());
		
		
		to.getSport().setTranslated(! (s == null));
		to.getCategory().setTranslated(!(c == null));
		to.getTournament().setTranslated(!(t == null));
		to.getHomeTeam().setTranslated(!(homeTeam == null));
		to.getAwayTeam().setTranslated(!(awayTeam == null));
		
		Fixture f = null;
		if (s!=null && c!=null && t!=null && homeTeam!=null && awayTeam!=null) {
			f = new Fixture();
			f.setSport(s);
			f.setCategory(c);
			f.setTournament(t);
			f.setHomeTeam(homeTeam);
			f.setAwayTeam(awayTeam);
			f.getFeedIds().put(to.getFeed(), to.getFeedId());
			
			ZonedDateTime kickOff = ZonedDateTime.ofInstant(to.getKickOff().toInstant(), ZoneId.of(to.getZoneId()));
			f.setKickOff(kickOff);
			to.setTranslated(true);
		} else {
			log.warn("Not Translated entity : {}", to);
			to.setTranslated(false);
		}
		return f;
	}
	 
	
	private String getDescriptionFromTexts(Collection<Text> texts) {
		String descr = "";
		for (Text t : texts) {
			if (t.getLanguage().equals(applicationProperties.getFeedSettings().getLangToRead())) {
				descr = t.getValue();
				break;
			}
		}
		return descr;
	}

	private Set<TranslationObj> getTranslationObjects(BetradarBetData bbd) {
		
		
		Set<TranslationObj> tos = new HashSet<TranslationObj>();
		for (com.intralot.iflex.commons.provider.betradar.Sport brsport : bbd.getSports().getSport()) {
			TranslatableEntity sportTe = new TranslatableEntity();
			sportTe.setId(brsport.getBetradarSportID() + "");
			sportTe.setDescription(getDescriptionFromTexts (brsport.getTexts().getText()));
			for (com.intralot.iflex.commons.provider.betradar.Category brcategory : brsport.getCategory()) {
				TranslatableEntity categoryTe = new TranslatableEntity();
				categoryTe.setId(brcategory.getBetradarCategoryID() + "");
				categoryTe.setDescription(getDescriptionFromTexts (brcategory.getTexts().getText()));
				for (com.intralot.iflex.commons.provider.betradar.Tournament brtournament : brcategory
						.getTournament()) {
					TranslatableEntity tournamentTe = new TranslatableEntity();
					tournamentTe.setId(brtournament.getBetradarTournamentID() + "");
					tournamentTe.setDescription(getDescriptionFromTexts (brtournament.getTexts().getText()));
					for (Match match : brtournament.getMatch()) {
						TranslatableEntity homeTe = null;
						TranslatableEntity awayTe = null;
						for (Texts compTexts: match.getFixture().getCompetitors().getTexts()) {
							for (Text t : compTexts.getText()) {
								if (t.getType().equals("1")) {
									homeTe = new TranslatableEntity();
									homeTe.setId(t.getSUPERID()+"");
									homeTe.setDescription(getDescriptionFromTexts (t.getText()));
								} else if (t.getType().equals("2")) {
									awayTe = new TranslatableEntity();
									awayTe.setId(t.getSUPERID()+"");
									awayTe.setDescription(getDescriptionFromTexts (t.getText()));
								}
							}
						}
						String matchDate = match.getFixture().getDateInfo().getMatchDate();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
						

						 
						 
						if (homeTe!=null && awayTe!=null) {
							TranslationObj to = new TranslationObj();
							to.setReceived(new Date());
							to.setFeed(applicationProperties.getFeedSettings().getBetradarLang());
							to.setSport(sportTe);
							to.setCategory(categoryTe);
							to.setTournament(tournamentTe);
							to.setHomeTeam(homeTe);
							to.setAwayTeam(awayTe);
							to.setFeedId(match.getBetradarMatchID());
							try {
								Date  date = sdf.parse(matchDate);
								to.setKickOff(date);
								to.setZoneId(bbd.getTimestamp().getTimeZone());
								tos.add(to);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
					}
				}
			}
		}
		return tos;
	}

	
	

}
