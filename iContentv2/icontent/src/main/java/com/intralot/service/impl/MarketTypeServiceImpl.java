package com.intralot.service.impl;

import com.intralot.service.MarketTypeService;
import com.intralot.domain.MarketType;
import com.intralot.repository.MarketTypeRepository;
import com.intralot.web.rest.dto.MarketTypeDTO;
import com.intralot.web.rest.mapper.MarketTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing MarketType.
 */
@Service
public class MarketTypeServiceImpl implements MarketTypeService{

    private final Logger log = LoggerFactory.getLogger(MarketTypeServiceImpl.class);
    
    @Inject
    private MarketTypeRepository marketTypeRepository;
    
    @Inject
    private MarketTypeMapper marketTypeMapper;
    
    /**
     * Save a marketType.
     * @return the persisted entity
     */
    public MarketTypeDTO save(MarketTypeDTO marketTypeDTO) {
        log.debug("Request to save MarketType : {}", marketTypeDTO);
        MarketType marketType = marketTypeMapper.marketTypeDTOToMarketType(marketTypeDTO);
        marketType = marketTypeRepository.save(marketType);
        MarketTypeDTO result = marketTypeMapper.marketTypeToMarketTypeDTO(marketType);
        return result;
    }

    /**
     *  get all the marketTypes.
     *  @return the list of entities
     */
    public Page<MarketType> findAll(Pageable pageable) {
        log.debug("Request to get all MarketTypes");
        Page<MarketType> result = marketTypeRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one marketType by id.
     *  @return the entity
     */
    public MarketTypeDTO findOne(String id) {
        log.debug("Request to get MarketType : {}", id);
        MarketType marketType = marketTypeRepository.findOne(id);
        MarketTypeDTO marketTypeDTO = marketTypeMapper.marketTypeToMarketTypeDTO(marketType);
        return marketTypeDTO;
    }

    /**
     *  delete the  marketType by id.
     */
    public void delete(String id) {
        log.debug("Request to delete MarketType : {}", id);
        marketTypeRepository.delete(id);
    }
}
