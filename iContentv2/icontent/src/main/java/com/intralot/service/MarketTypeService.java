package com.intralot.service;

import com.intralot.domain.MarketType;
import com.intralot.web.rest.dto.MarketTypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing MarketType.
 */
public interface MarketTypeService {

    /**
     * Save a marketType.
     * @return the persisted entity
     */
    public MarketTypeDTO save(MarketTypeDTO marketTypeDTO);

    /**
     *  get all the marketTypes.
     *  @return the list of entities
     */
    public Page<MarketType> findAll(Pageable pageable);

    /**
     *  get the "id" marketType.
     *  @return the entity
     */
    public MarketTypeDTO findOne(String id);

    /**
     *  delete the "id" marketType.
     */
    public void delete(String id);
}
