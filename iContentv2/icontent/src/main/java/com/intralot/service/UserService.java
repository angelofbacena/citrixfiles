package com.intralot.service;

import java.util.Optional;

import org.springframework.scheduling.annotation.Scheduled;

import com.intralot.domain.User;
import com.intralot.web.rest.dto.ManagedUserDTO;

public interface UserService {

	Optional<User> activateRegistration(String key);

	Optional<User> completePasswordReset(String newPassword, String key);

	Optional<User> requestPasswordReset(String mail);

	User createUserInformation(String login, String password, String firstName, String lastName, String email,
			String langKey);

	User createUser(ManagedUserDTO managedUserDTO);

	void updateUserInformation(String firstName, String lastName, String email, String langKey);

	void deleteUserInformation(String login);

	void changePassword(String password);

	Optional<User> getUserWithAuthoritiesByLogin(String login);

	User getUserWithAuthorities(String id);

	User getUserWithAuthorities();

	/**
	 * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
	 * 30 days.
	 * <p/>
	 * <p>
	 * This is scheduled to get fired everyday, at midnight.
	 * </p>
	 */
	void removeOldPersistentTokens();

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p/>
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 * </p>
	 */
	void removeNotActivatedUsers();

}