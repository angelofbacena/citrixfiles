package com.intralot.service;

import com.intralot.domain.EquationGroup;
import com.intralot.web.rest.dto.EquationGroupDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing EquationGroup.
 */
public interface EquationGroupService {

    /**
     * Save a equationGroup.
     * @return the persisted entity
     */
    public EquationGroupDTO save(EquationGroupDTO equationGroupDTO);

    /**
     *  get all the equationGroups.
     *  @return the list of entities
     */
    public Page<EquationGroup> findAll(Pageable pageable);

    /**
     *  get the "id" equationGroup.
     *  @return the entity
     */
    public EquationGroupDTO findOne(String id);

    /**
     *  delete the "id" equationGroup.
     */
    public void delete(String id);
}
