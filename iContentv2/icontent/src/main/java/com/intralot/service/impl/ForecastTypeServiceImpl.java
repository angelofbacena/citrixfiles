package com.intralot.service.impl;

import com.intralot.service.ForecastTypeService;
import com.intralot.domain.ForecastType;
import com.intralot.repository.ForecastTypeRepository;
import com.intralot.web.rest.dto.ForecastTypeDTO;
import com.intralot.web.rest.mapper.ForecastTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ForecastType.
 */
@Service
public class ForecastTypeServiceImpl implements ForecastTypeService{

    private final Logger log = LoggerFactory.getLogger(ForecastTypeServiceImpl.class);
    
    @Inject
    private ForecastTypeRepository forecastTypeRepository;
    
    @Inject
    private ForecastTypeMapper forecastTypeMapper;
    
    /**
     * Save a forecastType.
     * @return the persisted entity
     */
    public ForecastTypeDTO save(ForecastTypeDTO forecastTypeDTO) {
        log.debug("Request to save ForecastType : {}", forecastTypeDTO);
        ForecastType forecastType = forecastTypeMapper.forecastTypeDTOToForecastType(forecastTypeDTO);
        forecastType = forecastTypeRepository.save(forecastType);
        ForecastTypeDTO result = forecastTypeMapper.forecastTypeToForecastTypeDTO(forecastType);
        return result;
    }

    /**
     *  get all the forecastTypes.
     *  @return the list of entities
     */
    public Page<ForecastType> findAll(Pageable pageable) {
        log.debug("Request to get all ForecastTypes");
        Page<ForecastType> result = forecastTypeRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one forecastType by id.
     *  @return the entity
     */
    public ForecastTypeDTO findOne(String id) {
        log.debug("Request to get ForecastType : {}", id);
        ForecastType forecastType = forecastTypeRepository.findOne(id);
        ForecastTypeDTO forecastTypeDTO = forecastTypeMapper.forecastTypeToForecastTypeDTO(forecastType);
        return forecastTypeDTO;
    }

    /**
     *  delete the  forecastType by id.
     */
    public void delete(String id) {
        log.debug("Request to delete ForecastType : {}", id);
        forecastTypeRepository.delete(id);
    }
}
