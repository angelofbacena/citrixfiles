package com.intralot.service.impl;

import com.intralot.service.TranslationObjService;
import com.intralot.domain.TranslationObj;
import com.intralot.repository.TranslationObjRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing TranslationObj.
 */
@Service
public class TranslationObjServiceImpl implements TranslationObjService{

    private final Logger log = LoggerFactory.getLogger(TranslationObjServiceImpl.class);
    
    @Inject
    private TranslationObjRepository translationObjRepository;
    
    /**
     * Save a translationObj.
     * @return the persisted entity
     */
    public TranslationObj save(TranslationObj translationObj) {
        log.debug("Request to save TranslationObj : {}", translationObj);
        TranslationObj result = translationObjRepository.save(translationObj);
        return result;
    }

    /**
     *  get all the translationObjs.
     *  @return the list of entities
     */
    public Page<TranslationObj> findAll(Pageable pageable) {
        log.debug("Request to get all TranslationObjs");
        Page<TranslationObj> result = translationObjRepository.findAll(pageable); 
        return result;
    }

    /**
     *  get one translationObj by id.
     *  @return the entity
     */
    public TranslationObj findOne(String id) {
        log.debug("Request to get TranslationObj : {}", id);
        TranslationObj translationObj = translationObjRepository.findOne(id);
        return translationObj;
    }

    /**
     *  delete the  translationObj by id.
     */
    public void delete(String id) {
        log.debug("Request to delete TranslationObj : {}", id);
        translationObjRepository.delete(id);
    }
}
