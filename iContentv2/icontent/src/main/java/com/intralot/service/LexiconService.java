package com.intralot.service;

import java.util.Collection;

import com.intralot.lexicon.model.Category;
import com.intralot.lexicon.model.Sport;
import com.intralot.lexicon.model.Team;
import com.intralot.lexicon.model.Tournament;

public interface LexiconService {
	
	Sport getSportByProviderId(int providerId, String lang);
	Category getCategoryByProviderId(int providerId, String lang);
	Tournament getTournamentByProviderId(int providerId, String lang);
	Tournament[] getTournamentsBySportId(int sportId);
	Team getTeamByProviderId(int providerId, String lang);
	Sport[] getSports();
	Category[] getCategoriesBySportId(int sportId);
	Tournament[] getTournamentsByCategoryId(int categoryId);
	Team[] getTeamsByDescription(String description);
	

}
