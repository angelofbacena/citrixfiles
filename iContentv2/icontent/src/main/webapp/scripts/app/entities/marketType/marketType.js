'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('marketType', {
                parent: 'entity',
                url: '/marketTypes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.marketType.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/marketType/marketTypes.html',
                        controller: 'MarketTypeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('marketType');
                        $translatePartialLoader.addPart('derivativeType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('marketType.detail', {
                parent: 'entity',
                url: '/marketType/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.marketType.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/marketType/marketType-detail.html',
                        controller: 'MarketTypeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('marketType');
                        $translatePartialLoader.addPart('derivativeType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'MarketType', function($stateParams, MarketType) {
                        return MarketType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('marketType.new', {
                parent: 'marketType',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-dialog.html',
                        controller: 'MarketTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    cancelledByCodes: null,
                                    basicDisplay: null,
                                    masterSelection: null,
                                    derivativeType: null,
                                    forecastType: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('marketType', null, { reload: true });
                    }, function() {
                        $state.go('marketType');
                    })
                }]
            })
            .state('marketType.edit', {
                parent: 'marketType',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-dialog.html',
                        controller: 'MarketTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('marketType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('marketType.delete', {
                parent: 'marketType',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-delete-dialog.html',
                        controller: 'MarketTypeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('marketType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('marketType.new.codeTypeNew', {
                parent: 'marketType.new',
                url: '/marketType/{id}/codeType/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-codeType-dialog.html',
                        controller: 'MarketTypeCodeTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('marketType.new', null, { reload: true });
                    }, function() {
                        $state.go('marketType.new');
                    })
                }]
            })
            .state('marketType.new.codeTypeEdit', {
                parent: 'marketType.new',
                url: '/marketType/{id}/codeType/{cd}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-codeType-dialog.html',
                        controller: 'MarketTypeCodeTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('marketType.new', null, { reload: true });
                    }, function() {
                        $state.go('marketType.new');
                    })
                }]
            })
            .state('marketType.edit.codeTypeNew', {
                parent: 'marketType.edit',
                url: '/marketType/{id}/codeType/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-codeType-dialog.html',
                        controller: 'MarketTypeCodeTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }],
                            codeType: undefined
                    		
                        }
                    }).result.then(function(result) {
                        $state.go('marketType.edit', null, { reload: true });
                    }, function() {
                        $state.go('marketType.edit');
                    })
                }]
            }) 
            /*.state('marketType.edit.codeTypeEdit', {
                parent: 'marketType.edit',
                url: '/marketType/{id}/codeType/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/marketType/marketType-codeType-dialog.html',
                        controller: 'MarketTypeCodeTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['MarketType', function(MarketType) {
                                return MarketType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('marketType.edit', null, { reload: true });
                    }, function() {
                        $state.go('marketType.edit');
                    })
                }]
            })*/;
    });
