'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tournamentTemplate', {
                parent: 'entity',
                url: '/tournamentTemplates',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.tournamentTemplate.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tournamentTemplate/tournamentTemplates.html',
                        controller: 'TournamentTemplateController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tournamentTemplate');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tournamentTemplate.detail', {
                parent: 'entity',
                url: '/tournamentTemplate/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.tournamentTemplate.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tournamentTemplate/tournamentTemplate-detail.html',
                        controller: 'TournamentTemplateDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tournamentTemplate');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'TournamentTemplate', function($stateParams, TournamentTemplate) {
                        return TournamentTemplate.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tournamentTemplate.new', {
                parent: 'tournamentTemplate',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tournamentTemplate/tournamentTemplate-dialog.html',
                        controller: 'TournamentTemplateDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    autoPaymentSuspension: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tournamentTemplate', null, { reload: true });
                    }, function() {
                        $state.go('tournamentTemplate');
                    })
                }]
            })
            .state('tournamentTemplate.edit', {
                parent: 'tournamentTemplate',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tournamentTemplate/tournamentTemplate-dialog.html',
                        controller: 'TournamentTemplateDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['TournamentTemplate', function(TournamentTemplate) {
                                return TournamentTemplate.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tournamentTemplate', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tournamentTemplate.delete', {
                parent: 'tournamentTemplate',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tournamentTemplate/tournamentTemplate-delete-dialog.html',
                        controller: 'TournamentTemplateDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['TournamentTemplate', function(TournamentTemplate) {
                                return TournamentTemplate.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tournamentTemplate', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
