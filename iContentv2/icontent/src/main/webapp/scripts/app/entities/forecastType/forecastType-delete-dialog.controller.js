'use strict';

angular.module('icontentApp')
	.controller('ForecastTypeDeleteController', function($scope, $uibModalInstance, entity, ForecastType) {

        $scope.forecastType = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ForecastType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
