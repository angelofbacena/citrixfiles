'use strict';

angular.module('icontentApp').controller('EquationGroupDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'EquationGroup',
        function($scope, $stateParams, $uibModalInstance, entity, EquationGroup) {

        $scope.equationGroup = entity;
        $scope.load = function(id) {
            EquationGroup.get({id : id}, function(result) {
                $scope.equationGroup = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:equationGroupUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.equationGroup.id != null) {
                EquationGroup.update($scope.equationGroup, onSaveSuccess, onSaveError);
            } else {
                EquationGroup.save($scope.equationGroup, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
