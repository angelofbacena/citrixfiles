'use strict';

angular.module('icontentApp').controller('MarketTypeCodeTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'MarketType', 'General','ParseLinks','codeType', 
        function($scope, $stateParams, $uibModalInstance, entity, MarketType, General ,ParseLinks, codeType) {
    	 
        $scope.marketType = entity;
        $scope.codeType = codeType;
        var toSave = $scope.codeType==undefined;

        /*$scope.load = function(id) {
            MarketType.get({id : id}, function(result) {
                $scope.marketType = result;
            });
        };*/
        
        General.query(function(result) {
            $scope.marketSides = result;
            console.log($scope.marketSides);
        });
        
        
        
        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:marketTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = false;
            
            console.log("save button clicked for marketType:" + $scope.marketType.id);
            console.log($scope.codeType);
            if($scope.marketType.codeTypes== undefined){
            	$scope.marketType.codeTypes = [];
            }
            if (toSave && $scope.codeType!=undefined){
            	$scope.marketType.codeTypes.push($scope.codeType);
                console.log("Codetypes: " + $scope.marketType.codeTypes);
            }
            
            $uibModalInstance.close();
           /* if ($scope.marketType.id != null) {
                MarketType.update($scope.marketType, onSaveSuccess, onSaveError);
            } else {
                MarketType.save($scope.marketType, onSaveSuccess, onSaveError);
            }*/
            
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
