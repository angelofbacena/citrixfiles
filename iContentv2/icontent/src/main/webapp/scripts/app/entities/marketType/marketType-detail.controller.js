'use strict';

angular.module('icontentApp')
    .controller('MarketTypeDetailController', function ($scope, $rootScope, $stateParams, entity, MarketType) {
        $scope.marketType = entity;
        $scope.load = function (id) {
            MarketType.get({id: id}, function(result) {
                $scope.marketType = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:marketTypeUpdate', function(event, result) {
            $scope.marketType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
