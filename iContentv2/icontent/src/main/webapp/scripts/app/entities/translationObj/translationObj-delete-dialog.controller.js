'use strict';

angular.module('icontentApp')
	.controller('TranslationObjDeleteController', function($scope, $uibModalInstance, entity, TranslationObj) {

        $scope.translationObj = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            TranslationObj.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
