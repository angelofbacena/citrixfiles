'use strict';

angular.module('icontentApp').controller('TranslationObjDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'TranslationObj',
        function($scope, $stateParams, $uibModalInstance, entity, TranslationObj) {

        $scope.translationObj = entity;
        $scope.load = function(id) {
            TranslationObj.get({id : id}, function(result) {
                $scope.translationObj = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:translationObjUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.translationObj.id != null) {
                TranslationObj.update($scope.translationObj, onSaveSuccess, onSaveError);
            } else {
                TranslationObj.save($scope.translationObj, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForKickOff = {};

        $scope.datePickerForKickOff.status = {
            opened: false
        };

        $scope.datePickerForKickOffOpen = function($event) {
            $scope.datePickerForKickOff.status.opened = true;
        };
}]);
