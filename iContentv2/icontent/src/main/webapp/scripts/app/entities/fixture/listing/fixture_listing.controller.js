(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('FixtureListingController', FixtureListingController);

    FixtureListingController.$inject = ['$filter', '$scope','$rootScope', '$state', 'Fixture', 'ParseLinks', 'AlertService', 'lexiconSports'];

    function FixtureListingController ($filter, $scope, $rootScope, $state, Fixture, ParseLinks, AlertService, lexiconSports) {

        var vm              = this;
        // vm.loadFixtures     = loadFixtures;
        vm.loadPage         = loadPage;
        vm.predicate        = 'id';
        vm.reverse          = true;
        vm.page = 1;
        vm.transition       = transition;
        vm.checkAll         = checkAll;
        vm.showStatus       = showStatus;
        vm.updateFixture    = updateFixture;
        vm.importExcel      = importExcel;
        vm.exportExcel      = exportExcel;
        vm.clearAll         = clearAll;
        vm.bulkFixtureUpdate = bulkFixtureUpdate;
        vm.open              = open;
        vm.fixtures = [];
        // vm.loadFixtures();
        vm.changedData      = [];
        $rootScope.changedData = vm.changedData;
        vm.saveData = saveData;
        vm.lexiconSports = lexiconSports;
        vm.lexiconTournament;
        vm.fetchCategoriesAndTournaments = fetchCategoriesAndTournaments;
        vm.datePicker = {
            startDate: null,
            endDate: null
        };
        // vm.filterStatus=paginationConstants.filterStatus;
        // vm.filterSports=paginationConstants.filterSports;
        // vm.filterTournament=paginationConstants.filterTournament;
        vm.dateRangeOptions = {
            format: "MM-DD-YYYY",
            min: null
        };

        console.log("Hello");


        vm.statuses = [
            {value: 'Active', text: 'Active'},
            {value: 'In-Active', text: 'In-Active'}
        ];

        vm.loadAll = function() {
            Fixture.query({page: vm.page - 1, size: 20, sort: [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.fixtures = result;
            });
        };
        vm.loadAll();

        function fetchCategoriesAndTournaments(sportID){
            Fixture.fetchTournamentsBySportId({ sportId: sportID }, fetchCategoriesAndTournamentsSuccess, fetchCategoriesAndTournamentsError);
            function fetchCategoriesAndTournamentsSuccess(data, headers) {
                // vm.fixture.category = null;
                // vm.fixture.tournament = null;
                // vm.fixture.template = null;
                vm.lexiconSportTournaments = data;
            }
            function fetchCategoriesAndTournamentsError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function loadFixtures (option) {

            // if(option) {
            //     paginationConstants.itemsPerPage = $scope.pagination.itemsPerPage;
            //     uibPaginationConfig.itemsPerPage = $scope.pagination.itemsPerPage;
            // }

            // Fixture.query({
            //     page: pagingParams.page - 1,
            //     size: paginationConstants.itemsPerPage,
            //     sort: sort()
            // }, onSuccess, onError);

            Fixture.get(onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                var fixture_listing = [];
                console.log("Hello" + data);
                // if((vm.filterStatus!==null&&vm.filterStatus!="")||vm.datePicker.startDate!==null||vm.filterSports!==null||vm.filterTournament!==null){
                	
                //     for (var i = 0; i < data.length; i++) {
                //         var newCompare=new Date(compare).getTime();
                //         var dateFrom=$filter('date')(vm.datePicker.startDate, "yyyy-MM-dd");
                //         var dateTo=$filter('date')(vm.datePicker.endDate, "yyyy-MM-dd");
                //         var compare=$filter('date')(data[i].kickOff, "yyyy-MM-dd");
                //         var newDateFrom=new Date(dateFrom).getTime();
                //         var newDateTo=new Date(dateTo).getTime();
                //         data[i].inputParameters1 = parseInt(data[i].inputParameters1);
                //         data[i].inputParameters2 = parseInt(data[i].inputParameters2);
                //         data[i].status = 'Active';
                //         data[i]._1tt = 2.18;
                //         data[i]._xtt2 = 3.15;
                //         data[i]._2     = 3.25;
                //         data[i].Overround = 112.52;
                //         data[i].O25 = "----";
                //         data[i].U25 = 1.70;
                //         data[i].inputParameters1 = 1;
                //         data[i].inputParameters2 = 1;
                //         data[i].oddFd = 'betradar';
                //         data[i].dataFd = 'betradar';
                //         data[i].Selected = false;

                //         if(vm.filterStatus!==null&&vm.datePicker.startDate!==null&&vm.filterSports!==null&&vm.filterTournament!==null) {
                //             if(data[i].sports==vm.filterSports&&data[i].tournament==vm.filterTournament&&newDateFrom<=newCompare&&newDateTo>=newCompare) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.filterSports!==null&&vm.filterTournament!==null) {
                //             if(data[i].sports==vm.filterSports&&data[i].tournament==vm.filterTournament) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.datePicker.startDate!==null&&vm.filterTournament!==null) {
                //             if(data[i].tournament==vm.filterTournament&&newDateFrom<=newCompare&&newDateTo>=newCompare) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.datePicker.startDate!==null&&vm.filterSports!==null) {
                //             if(data[i].sports==vm.filterSports&&newDateFrom<=newCompare&&newDateTo>=newCompare) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.datePicker.startDate!==null&&vm.filterStatus!==null){

                //             if(newDateFrom<=newCompare&&newDateTo>=newCompare) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.filterSports!==null) {
                //             if(data[i].sports==vm.filterSports) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.filterTournament!==null) {
                //             if(data[i].tournament==vm.filterTournament) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }else if(vm.filterStatus!==null) {
                //             fixture_listing.push(data[i]);
                //         }else if(vm.datePicker.startDate!==null){ 
                //             if(newDateFrom<=newCompare&&newDateTo>=newCompare) {
                //                 fixture_listing.push(data[i]);
                //             }
                //         }
                //     }

                //     if (vm.filterSports!==null||vm.filterTournament!==null||vm.datePicker.startDate!==null) vm.totalItems=fixture_listing.length;
                //     else if(vm.filterStatus!==null) vm.totalItems=data.length;

                //     vm.queryCount = vm.totalItems;
                //     vm.fixtures = fixture_listing; 
                //     $rootScope.listScope = vm.fixtures;
                //     vm.page = pagingParams.page;
                //     vm.opened = {};
                //     vm.opened.$data = false;

                //     if(option) {
                //         vm.page = 1;
                //         $state.go($state.current, {
                //             page: vm.page,
                //             sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                //             search: vm.currentSearch
                //         }, {reload: true});
                //     }else {
                //         vm.page = pagingParams.page;
                //     }

                //     vm.itemsPerPage = paginationConstants.itemsPerPage;
                //     paginationConstants.filterStatus=vm.filterStatus;
                //     paginationConstants.dateRange.startDate=vm.datePicker.startDate;
                //     paginationConstants.dateRange.endDate=vm.datePicker.endDate;
                //     paginationConstants.filterSports=vm.filterSports;
                //     paginationConstants.filterTournament=vm.filterTournament;

                // }else { 
                //     vm.fixtures = [];
                //     paginationConstants.filterStatus=null;
                //     paginationConstants.dateRange.startDate=null;
                //     paginationConstants.dateRange.endDate=null;
                //     paginationConstants.filterSports=null;
                //     paginationConstants.filterTournament=null;
                // }

            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function showStatus (status) {
            var selected = $filter('filter')(vm.statuses, {value: status});
            return (status && selected.length) ? selected[0].text : 'Not set';
        }

        function open ($event, elementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            vm.opened[elementOpened] = !vm.opened[elementOpened];
        }

        function checkAll() {
            if ($scope.selectedAll) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }
            angular.forEach(vm.fixtures, function (item) {
                item.Selected = $scope.selectedAll;
            });
        }

        function clearAll() {

        }

        function updateFixture() {
            if (array.length != 0) {
                angular.forEach(vm.changedData, function (item,key) {
                    if (item.Selected) {
                        item.template = vm.fixtures.text;
                        item.oddFd    = vm.fixtures.oddFd;
                        item.dataFd   = vm.fixtures.dataFd;
                        item.status   = vm.fixtures.status
                        Fixture.update(array[key], onSaveSuccess, onSaveError);
                    }
                });
            } else {
                alert("Please select items to update first.");
            }

            var onSaveSuccess = function (result) {
                $scope.$emit('fixtureApp:fixture  Update', result);
                $uibModalInstance.close(result);
                vm.isSaving = false;
            };

            var onSaveError = function () {
                vm.isSaving = false;
            };
        }

        function importExcel() {

        }

        function exportExcel() {

        }

        function bulkFixtureUpdate() {

        }

        function saveData(fixtureID, data, option) {
            var item = [];
            var isNewItem = true;

            if(vm.changedData.length >= 1) {

                for (var i = 0; i < vm.changedData.length; i++) {
                    if(fixtureID==vm.changedData[i].id) {
                        switch(option) {
                            case 1: vm.changedData[i].kickOff = data; break;
                            case 5: vm.changedData[i].template = data; break;
                        } isNewItem=false; break;
                    }
                }

                if(isNewItem) {
                    item.id = fixtureID;
                    switch(option) {
                        case 1: item.kickOff = data; break;
                        case 5: item.template = data; break;
                    }
                    vm.changedData.push(item);
                }
            }else {

                item.id = fixtureID;
                switch(option) {
                    case 1: item.kickOff = data; break;
                    case 5: item.template = data; break;
                }
                vm.changedData.push(item);
            }
        }

    }
})();
