'use strict';

angular.module('icontentApp')
    .controller('MarketTypeController', function ($scope, $state, MarketType, ParseLinks) {

        $scope.marketTypes = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            MarketType.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.marketTypes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.marketType = {
                description: null,
                cancelledByCodes: null,
                basicDisplay: null,
                masterSelection: null,
                derivativeType: null,
                id: null
            };
        };
    });
