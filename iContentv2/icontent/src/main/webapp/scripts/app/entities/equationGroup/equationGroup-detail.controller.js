'use strict';

angular.module('icontentApp')
    .controller('EquationGroupDetailController', function ($scope, $rootScope, $stateParams, entity, EquationGroup) {
        $scope.equationGroup = entity;
        $scope.load = function (id) {
            EquationGroup.get({id: id}, function(result) {
                $scope.equationGroup = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:equationGroupUpdate', function(event, result) {
            $scope.equationGroup = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
