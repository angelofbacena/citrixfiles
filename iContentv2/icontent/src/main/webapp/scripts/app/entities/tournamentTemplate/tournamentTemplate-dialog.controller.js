'use strict';

angular.module('icontentApp').controller('TournamentTemplateDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'TournamentTemplate',
        function($scope, $stateParams, $uibModalInstance, entity, TournamentTemplate) {

        $scope.tournamentTemplate = entity;
        $scope.load = function(id) {
            TournamentTemplate.get({id : id}, function(result) {
                $scope.tournamentTemplate = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:tournamentTemplateUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tournamentTemplate.id != null) {
                TournamentTemplate.update($scope.tournamentTemplate, onSaveSuccess, onSaveError);
            } else {
                TournamentTemplate.save($scope.tournamentTemplate, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
