'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('subscriber', {
                parent: 'entity',
                url: '/subscribers',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.subscriber.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/subscriber/subscribers.html',
                        controller: 'SubscriberController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('subscriber');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('subscriber.detail', {
                parent: 'entity',
                url: '/subscriber/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.subscriber.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/subscriber/subscriber-detail.html',
                        controller: 'SubscriberDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('subscriber');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Subscriber', function($stateParams, Subscriber) {
                        return Subscriber.get({id : $stateParams.id});
                    }]
                }
            })
            .state('subscriber.new', {
                parent: 'subscriber',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/subscriber/subscriber-dialog.html',
                        controller: 'SubscriberDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    endpoint: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('subscriber', null, { reload: true });
                    }, function() {
                        $state.go('subscriber');
                    })
                }]
            })
            .state('subscriber.edit', {
                parent: 'subscriber',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/subscriber/subscriber-dialog.html',
                        controller: 'SubscriberDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Subscriber', function(Subscriber) {
                                return Subscriber.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('subscriber', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('subscriber.delete', {
                parent: 'subscriber',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/subscriber/subscriber-delete-dialog.html',
                        controller: 'SubscriberDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Subscriber', function(Subscriber) {
                                return Subscriber.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('subscriber', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
