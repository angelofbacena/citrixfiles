'use strict';

angular.module('icontentApp')
	.controller('SubscriberDeleteController', function($scope, $uibModalInstance, entity, Subscriber) {

        $scope.subscriber = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Subscriber.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
