'use strict';

angular.module('icontentApp')
    .controller('TranslationObjDetailController', function ($scope, $rootScope, $stateParams, entity, TranslationObj) {
        $scope.translationObj = entity;
        $scope.load = function (id) {
            TranslationObj.get({id: id}, function(result) {
                $scope.translationObj = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:translationObjUpdate', function(event, result) {
            $scope.translationObj = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
