(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fixture-listing', {
            parent: 'fixture',
            url: '/fixture/listing',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Fixture Listing'
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/fixture/listing/fixture_listing.html',
                    controller: 'FixtureListingController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                lexiconSports: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchSports({});
                    }
                ]
            }
        }) 
        .state('update-fixture-modal', {
            parent: 'fixture-listing',
            url: '/update',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/entities/fixture/listing/update_fixture_modal.html',
                    controller: 'FixtureDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                box: false,
                                event: null,
                                homeTeam: null,
                                awayTeam: null,
                                tournament: null,
                                kickOff: null,
                                initialPricingDate: null,
                                status: null,
                                inputParameters1: null,
                                inputParameters2: null,
                                bFinalResult: null,
                                bUnderOver: null,
                                oddFd: null,
                                dataFd: null,
                                template: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('fixture-listing', null, { reload: false });
                }, function() {
                    $state.go('fixture-listing');
                });
            }]
        });
    }

})();
