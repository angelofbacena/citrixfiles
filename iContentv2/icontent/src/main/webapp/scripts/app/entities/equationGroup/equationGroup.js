'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('equationGroup', {
                parent: 'entity',
                url: '/equationGroups',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.equationGroup.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroups.html',
                        controller: 'EquationGroupController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('equationGroup');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('equationGroup.detail', {
                parent: 'entity',
                url: '/equationGroup/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.equationGroup.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-detail.html',
                        controller: 'EquationGroupDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('equationGroup');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'EquationGroup', function($stateParams, EquationGroup) {
                        return EquationGroup.get({id : $stateParams.id});
                    }]
                }
            })
            .state('equationGroup.new', {
                parent: 'equationGroup',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-dialog.html',
                        controller: 'EquationGroupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('equationGroup', null, { reload: true });
                    }, function() {
                        $state.go('equationGroup');
                    })
                }]
            })
            .state('equationGroup.edit', {
                parent: 'equationGroup',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-dialog.html',
                        controller: 'EquationGroupDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['EquationGroup', function(EquationGroup) {
                                return EquationGroup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('equationGroup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('equationGroup.delete', {
                parent: 'equationGroup',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-delete-dialog.html',
                        controller: 'EquationGroupDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['EquationGroup', function(EquationGroup) {
                                return EquationGroup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('equationGroup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('equationGroup.newEquation', {
                parent: 'equationGroup.new',
                url: '/newequation',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-equation-dialog.html',
                        controller: 'EquationGroupEquationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('equationGroup', null, { reload: true });
                    }, function() {
                        $state.go('equationGroup');
                    })
                }]
            })
            .state('equationGroup.editEquation', {
                parent: 'equationGroup.edit',
                url: '/{id}/{eid}/editequation',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/equationGroup/equationGroup-equation-dialog.html',
                        controller: 'EquationGroupEquationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['EquationGroup', function(EquationGroup) {
                                return EquationGroup.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('equationGroup', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
