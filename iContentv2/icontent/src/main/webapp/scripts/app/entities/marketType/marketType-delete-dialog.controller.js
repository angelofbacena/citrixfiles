'use strict';

angular.module('icontentApp')
	.controller('MarketTypeDeleteController', function($scope, $uibModalInstance, entity, MarketType) {

        $scope.marketType = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            MarketType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
