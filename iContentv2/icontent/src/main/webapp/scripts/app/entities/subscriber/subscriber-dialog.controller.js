'use strict';

angular.module('icontentApp').controller('SubscriberDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Subscriber',
        function($scope, $stateParams, $uibModalInstance, entity, Subscriber) {

        $scope.subscriber = entity;
        $scope.load = function(id) {
            Subscriber.get({id : id}, function(result) {
                $scope.subscriber = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:subscriberUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.subscriber.id != null) {
                Subscriber.update($scope.subscriber, onSaveSuccess, onSaveError);
            } else {
                Subscriber.save($scope.subscriber, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
