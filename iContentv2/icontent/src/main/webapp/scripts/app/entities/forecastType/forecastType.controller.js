'use strict';

angular.module('icontentApp')
    .controller('ForecastTypeController', function ($scope, $state, ForecastType, ParseLinks) {

        $scope.forecastTypes = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            ForecastType.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.forecastTypes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.forecastType = {
                description: null,
                sumOfCodes: null,
                withHandicapA: null,
                withHandicapB: null,
                hasLimit: null,
                hasDoubleChance: null,
                id: null
            };
        };
    });
