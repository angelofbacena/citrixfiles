(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('FixtureCreatorController', FixtureCreatorController);

    FixtureCreatorController.$inject = ['$scope', '$state', '$stateParams', 'fixture', 'Fixture', 'AlertService', 'lexiconSports', '$http'];

    function FixtureCreatorController ($scope, $state, $stateParams, fixture, Fixture, AlertService, lexiconSports, $http) {
        var vm = this;
        
        vm.fixture = fixture;
        vm.lexiconSports = lexiconSports;
        vm.fetchCategories = fetchCategories;
        vm.fetchTournaments = fetchTournaments;
        vm.fetchTemplates = fetchTemplates;
        vm.validationTeamChange = validationTeamChange;
        vm.validationTeamSelection = validationTeamSelection;
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.kickOff = false;
        vm.searchTeam = searchTeam;
        vm.tempHomeTeam;
        vm.tempAwayTeam;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };

        function searchTeam(teamDescription) {

            
            return $http.get('/api/lexicon/teams', {
              params: {
                description: teamDescription
              }
            }).then(function(response){
                console.log(response.data);
                return response.data;
            }).catch(function(error) {
                console.log(error);
                return AlertService.error(error.data.error);
            }); 
        }

        function fetchCategories(sportID){
            Fixture.fetchCategories({ sportId: sportID },fetchCategoriesSuccess,fetchCategoriesError);
            function fetchCategoriesSuccess(data, headers) {
                vm.fixture.category = null;
                vm.fixture.tournament = null;
                vm.fixture.template = null;
                vm.lexiconSportsCategories = data;
            }
            function fetchCategoriesError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function fetchTournaments(categoryID){

            Fixture.fetchTournaments({ categoryId: categoryID }, fetchTournamentsSuccess, fetchTournamentsError);
            function fetchTournamentsSuccess(data, headers) {
                vm.fixture.tournament = null;
                vm.fixture.template = null;
                vm.tournamentsList = data;
            }
            function fetchTournamentsError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function fetchTemplates(tournament){
            Fixture.fetchTemplates({tournament:tournament}, fetchTemplatesSuccess, fetchTemplatesError);
            function fetchTemplatesSuccess(data, headers) {
                vm.fixture.template = null;
                vm.templateList = data.response;
            }
            function fetchTemplatesError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function validationTeamChange(type, value) {
            if (type == "home") {
                if (vm.fixture.homeTeam) {
                    if (value) {
                        if (!angular.equals(value, vm.fixture.homeTeam.description)) {
                            $scope.editForm.home.$invalid = true;
                            $scope.editForm.$invalid = true;
                        } else {
                            $scope.editForm.home.$invalid = false;
                            $scope.editForm.$invalid = false;
                        }
                    } else {
                        delete vm.fixture.homeTeam;
                        $scope.editForm.home.$invalid = false;
                        $scope.editForm.$invalid = false;
                    }
                }    
            } else if (type == "away") {
                if (vm.fixture.awayTeam) {
                    if (value) {
                        if (!angular.equals(value, vm.fixture.awayTeam.description)) {
                            $scope.editForm.away.$invalid = true;
                            $scope.editForm.$invalid = true;
                        } else {
                            $scope.editForm.away.$invalid = false;
                            $scope.editForm.$invalid = false;
                        }
                    } else {
                        delete vm.fixture.awayTeam;
                        $scope.editForm.away.$invalid = false;
                        $scope.editForm.$invalid = false;
                    }
                }    
            }
        }
        
        function validationTeamSelection(type, value) {
            if (type == "home") {
                if (vm.fixture.awayTeam) {
                    console.log("hello from home before");
                    if (angular.equals(vm.fixture.homeTeam.description, vm.fixture.awayTeam.description)) {
                        console.log("hello from home after");
                        $scope.editForm.home.$invalid = true;
                        $scope.editForm.away.$invalid = true;
                        $scope.editForm.$invalid = true;
                    } else {
                        $scope.editForm.home.$invalid = false;
                        $scope.editForm.away.$invalid = false;
                        $scope.editForm.$invalid = false;
                    }
                } else {
                    $scope.editForm.home.$invalid = false;
                    $scope.editForm.away.$invalid = false;
                    $scope.editForm.$invalid = false;
                }

                delete vm.fixture.homeTeam.sport;
                delete vm.fixture.homeTeam.literals;
                delete vm.fixture.homeTeam.tournaments;

            } else {
                if (vm.fixture.homeTeam) {
                    if (angular.equals(vm.fixture.homeTeam.description, vm.fixture.awayTeam.description)) {
                        $scope.editForm.home.$invalid = true;
                        $scope.editForm.away.$invalid = true;
                        $scope.editForm.$invalid = true;
                    } else {
                        $scope.editForm.home.$invalid = false;
                        $scope.editForm.away.$invalid = false;
                        $scope.editForm.$invalid = false;
                    }
                } else {
                    $scope.editForm.home.$invalid = false;
                    $scope.editForm.away.$invalid = false;
                    $scope.editForm.$invalid = false;
                }

                delete vm.fixture.awayTeam.sport;
                delete vm.fixture.awayTeam.literals;
                delete vm.fixture.awayTeam.tournaments;
            }
        }

        var onSaveSuccess = function (result) {
            vm.isSaving = false;
            vm.fixture = {
                kickOff: new Date(),
                sports: null,
                category: null,
                tournament: null,
                template: null,
                home: null,
                away: null,
                id: null
            }
            $scope.editForm.home.$pristine = true;
            $scope.editForm.away.$pristine = true;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.saveFixture = function () {
            vm.isSaving = true;
            if (vm.fixture.id !== null) {
                Fixture.update(vm.fixture, onSaveSuccess, onSaveError);
            } else {
                // console.log(vm.fixture);
                Fixture.save(vm.fixture, onSaveSuccess, onSaveError);
            }
        };

    }
})();
