'use strict';

angular.module('icontentApp')
    .controller('ForecastTypeDetailController', function ($scope, $rootScope, $stateParams, entity, ForecastType) {
        $scope.forecastType = entity;
        $scope.load = function (id) {
            ForecastType.get({id: id}, function(result) {
                $scope.forecastType = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:forecastTypeUpdate', function(event, result) {
            $scope.forecastType = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
