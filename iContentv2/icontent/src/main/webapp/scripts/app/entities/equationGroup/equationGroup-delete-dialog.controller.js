'use strict';

angular.module('icontentApp')
	.controller('EquationGroupDeleteController', function($scope, $uibModalInstance, entity, EquationGroup) {

        $scope.equationGroup = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            EquationGroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
