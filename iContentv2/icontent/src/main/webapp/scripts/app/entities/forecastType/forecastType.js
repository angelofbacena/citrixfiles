'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('forecastType', {
                parent: 'entity',
                url: '/forecastTypes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.forecastType.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/forecastType/forecastTypes.html',
                        controller: 'ForecastTypeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('forecastType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('forecastType.detail', {
                parent: 'entity',
                url: '/forecastType/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.forecastType.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/forecastType/forecastType-detail.html',
                        controller: 'ForecastTypeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('forecastType');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ForecastType', function($stateParams, ForecastType) {
                        return ForecastType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('forecastType.new', {
                parent: 'forecastType',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/forecastType/forecastType-dialog.html',
                        controller: 'ForecastTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    description: null,
                                    sumOfCodes: null,
                                    withHandicapA: null,
                                    withHandicapB: null,
                                    hasLimit: null,
                                    hasDoubleChance: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('forecastType', null, { reload: true });
                    }, function() {
                        $state.go('forecastType');
                    })
                }]
            })
            .state('forecastType.edit', {
                parent: 'forecastType',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/forecastType/forecastType-dialog.html',
                        controller: 'ForecastTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ForecastType', function(ForecastType) {
                                return ForecastType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('forecastType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('forecastType.delete', {
                parent: 'forecastType',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/forecastType/forecastType-delete-dialog.html',
                        controller: 'ForecastTypeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ForecastType', function(ForecastType) {
                                return ForecastType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('forecastType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
