'use strict';

angular.module('icontentApp')
    .controller('SubscriberDetailController', function ($scope, $rootScope, $stateParams, entity, Subscriber) {
        $scope.subscriber = entity;
        $scope.load = function (id) {
            Subscriber.get({id: id}, function(result) {
                $scope.subscriber = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:subscriberUpdate', function(event, result) {
            $scope.subscriber = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
