'use strict';

angular.module('icontentApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('translationObj', {
                parent: 'entity',
                url: '/translationObjs',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.translationObj.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/translationObj/translationObjs.html',
                        controller: 'TranslationObjController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('translationObj');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('translationObj.detail', {
                parent: 'entity',
                url: '/translationObj/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'icontentApp.translationObj.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/translationObj/translationObj-detail.html',
                        controller: 'TranslationObjDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('translationObj');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'TranslationObj', function($stateParams, TranslationObj) {
                        return TranslationObj.get({id : $stateParams.id});
                    }]
                }
            })
            .state('translationObj.new', {
                parent: 'translationObj',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/translationObj/translationObj-dialog.html',
                        controller: 'TranslationObjDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    kickOff: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('translationObj', null, { reload: true });
                    }, function() {
                        $state.go('translationObj');
                    })
                }]
            })
            .state('translationObj.edit', {
                parent: 'translationObj',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/translationObj/translationObj-dialog.html',
                        controller: 'TranslationObjDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['TranslationObj', function(TranslationObj) {
                                return TranslationObj.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('translationObj', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('translationObj.delete', {
                parent: 'translationObj',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/translationObj/translationObj-delete-dialog.html',
                        controller: 'TranslationObjDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['TranslationObj', function(TranslationObj) {
                                return TranslationObj.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('translationObj', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
