'use strict';

angular.module('icontentApp')
	.controller('TournamentTemplateDeleteController', function($scope, $uibModalInstance, entity, TournamentTemplate) {

        $scope.tournamentTemplate = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            TournamentTemplate.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
