'use strict';

angular.module('icontentApp')
    .controller('TranslationObjController', function ($scope, $state, TranslationObj, ParseLinks) {

        $scope.translationObjs = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            TranslationObj.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.translationObjs = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        

        $scope.clear = function () {
            $scope.translationObj = {
                kickOff: null,
                id: null
            };
        };
    });
