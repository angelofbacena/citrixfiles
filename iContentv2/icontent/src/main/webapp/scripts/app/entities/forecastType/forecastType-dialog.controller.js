'use strict';

angular.module('icontentApp').controller('ForecastTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ForecastType',
        function($scope, $stateParams, $uibModalInstance, entity, ForecastType) {

        $scope.forecastType = entity;
        $scope.load = function(id) {
            ForecastType.get({id : id}, function(result) {
                $scope.forecastType = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:forecastTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.forecastType.id != null) {
                ForecastType.update($scope.forecastType, onSaveSuccess, onSaveError);
            } else {
                ForecastType.save($scope.forecastType, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
