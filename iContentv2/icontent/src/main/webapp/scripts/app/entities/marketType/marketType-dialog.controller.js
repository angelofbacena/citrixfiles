'use strict';

angular.module('icontentApp').controller('MarketTypeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'MarketType', 'ForecastType', 'ParseLinks', '$location','$uibModal',
        function($scope, $stateParams, $uibModalInstance, entity, MarketType, ForecastType, ParseLinks, $location, $uibModal) {

    	
    	$scope.editCT = false;
    	$scope.newCT = false;
    	
    	 ForecastType.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
             $scope.links = ParseLinks.parse(headers('link'));
             $scope.totalItems = headers('X-Total-Count');
             $scope.forecastTypes = result;
         });
    	 
        $scope.marketType = entity;
        $scope.load = function(id) {
            MarketType.get({id : id}, function(result) {
                $scope.marketType = result;
            });
        };

        
        $scope.getCodeTypeState = function (){
    		
    		var url = $location.path();
	    	if(url.indexOf("/edit") > -1){
	    		$scope.stateUrl = 'marketType.edit.codeTypeNew({id:marketType.id})';
	    	}else{
	    		$scope.stateUrl = 'marketType.new.codeTypeNew({id:marketType.id})';
	    	}
	    	return $scope.stateUrl;
    	};
    	
    	$scope.openEditCodeTypeWindow = function (codeType){
    		
    		$scope.editCT = true;
    		console.log(codeType);
    		 $uibModal.open({
                 templateUrl: 'scripts/app/entities/marketType/marketType-codeType-dialog.html',
                 controller: 'MarketTypeCodeTypeDialogController',
                 size: 'lg',
                 resolve: {
                     entity: function () {
	                    return $scope.marketType;
	                 },
                     codeType: function () {
	                    return codeType;
	                 }
                 }
    		 }).result.then(function(result) {
    			 $scope.editCT = false;
    		    	$scope.newCT = false;
             }, function() {
            	 $scope.editCT = false;
             	$scope.newCT = false;
             })
    	}
    	
        var onSaveSuccess = function (result) {
            $scope.$emit('icontentApp:marketTypeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            
            if(!$scope.newCT && !$scope.editCT){
            	$scope.isSaving = true;
            	if ($scope.marketType.id != null) {
                    MarketType.update($scope.marketType, onSaveSuccess, onSaveError);
                } else {
                    MarketType.save($scope.marketType, onSaveSuccess, onSaveError);
                }
            }
            
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
