'use strict';

angular.module('icontentApp')
    .controller('TournamentTemplateDetailController', function ($scope, $rootScope, $stateParams, entity, TournamentTemplate) {
        $scope.tournamentTemplate = entity;
        $scope.load = function (id) {
            TournamentTemplate.get({id: id}, function(result) {
                $scope.tournamentTemplate = result;
            });
        };
        var unsubscribe = $rootScope.$on('icontentApp:tournamentTemplateUpdate', function(event, result) {
            $scope.tournamentTemplate = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
