'use strict';

angular.module('icontentApp')
    .controller('MainController', function ($scope, Principal, Echo) {

    	//console.log(Echo);
    	Echo.connect();
		Echo.subscribe();

		$scope.sendMessage = function() {
			console.log('sending...');
			Echo.sendMessage();	
			console.log('sent!');
		}

        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
        });
    });
