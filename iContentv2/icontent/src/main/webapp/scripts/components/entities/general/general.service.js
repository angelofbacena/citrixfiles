'use strict';

angular.module('icontentApp')
    .factory('General', function ($resource, DateUtils) {
        return $resource('api/marketSides', {}, {
        	'query': { method: 'GET', isArray: true}
        });
    });
