'use strict';

angular.module('icontentApp')
    .factory('Subscriber', function ($resource, DateUtils) {
        return $resource('api/subscribers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
