'use strict';

angular.module('icontentApp')
    .factory('TranslationObj', function ($resource, DateUtils) {
        return $resource('api/translationObjs/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.kickOff = DateUtils.convertDateTimeFromServer(data.kickOff);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
