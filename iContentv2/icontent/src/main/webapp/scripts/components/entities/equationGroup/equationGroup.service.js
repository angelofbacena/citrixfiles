'use strict';

angular.module('icontentApp')
    .factory('EquationGroup', function ($resource, DateUtils) {
        return $resource('api/equationGroups/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
