'use strict';

angular.module('icontentApp')
    .factory('TournamentTemplate', function ($resource, DateUtils) {
        return $resource('api/tournamentTemplates/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
