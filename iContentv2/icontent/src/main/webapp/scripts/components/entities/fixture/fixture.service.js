(function() {
    'use strict';
    angular
        .module('icontentApp')
        .factory('Fixture', Fixture);

    Fixture.$inject = ['$resource', 'DateUtils'];

    function Fixture($resource, DateUtils) {

        function dataFormatter(dataType, rawData) {
            var dataSize = rawData.length;
            
            switch(dataType) {
                case 'sports':
                    for(var i = 0; i < dataSize; i++) {
                        delete rawData[i].literals;
                    }
                    break;
                case 'categories':
                    for(var i = 0; i < dataSize; i++) {
                        delete rawData[i].literals;
                        delete rawData[i].sport;
                    }
                    break;
                case 'tournaments':
                    for(var i = 0; i < dataSize; i++) {
                        delete rawData[i].literals;
                        delete rawData[i].category;
                        delete rawData[i].phase;
                        delete rawData[i].part;
                        delete rawData[i].genericDescription;
                    }
                    break;
                case 'tournaments_by_sport':
                    for(var i = 0; i < dataSize; i++) {
                        delete rawData[i].literals;
                        delete rawData[i].category.sport;
                        delete rawData[i].category.literals;
                        delete rawData[i].phase;
                        delete rawData[i].part;
                        delete rawData[i].genericDescription;
                    }
                    break;
                default:
                    console.log("Unsupported data based on case");
                    break;
            }
            return rawData;

        };
        
        return $resource('api/fixtures/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.activationDate = DateUtils.convertDateTimeFromServer(data.activationDate);
                    data.kickOff = DateUtils.convertDateTimeFromServer(data.kickOff);
                    data.endDate = DateUtils.convertDateTimeFromServer(data.endDate);
                    return data;
                }
            },
            'update': { method:'PUT' },
            'fetchSports': {
                method: 'GET',
                url: '/api/lexicon/sports',
                isArray: true,
                transformResponse: function (data) {
                    var formattedData = dataFormatter('sports', angular.fromJson(data));
                    console.log("Fetch Sports");
                    console.log(formattedData);
                    return formattedData;
                }
            },
            'fetchCategories': {
                method: 'GET',
                url: '/api/lexicon/categories',
                isArray: true,
                transformResponse: function (data) {
                    var formattedData = dataFormatter('categories', angular.fromJson(data));
                    console.log("Fetch Categories");
                    console.log(formattedData);
                    return formattedData;
                }
            },
            'fetchTemplates': {
                method: 'GET',
                url: 'scripts/app/entities/dummy_data/templates.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'fetchTournaments': {
                method: 'GET',
                url: '/api/lexicon/tournaments',
                isArray: true,
                transformResponse: function (data) {
                    var formattedData = dataFormatter('tournaments', angular.fromJson(data));
                    console.log("Fetch Tournaments");
                    console.log(formattedData);
                    return formattedData;
                }
            },
            'fetchTournamentsBySportId': {
                method: 'GET',
                url: '/api/lexicon/tournaments_bysportid',
                isArray: true,
                transformResponse: function (data) {
                    // console.log(angular.fromJson(data));
                    var formattedData = dataFormatter('tournaments_by_sport', angular.fromJson(data));
                    console.log("Fetch Tournaments by sport id");
                    console.log(formattedData);
                    return formattedData;
                }
            }
        });
    }
})();
