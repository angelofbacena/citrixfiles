'use strict';

angular.module('icontentApp')
    .factory('MarketType', function ($resource, DateUtils) {
        return $resource('api/marketTypes/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
