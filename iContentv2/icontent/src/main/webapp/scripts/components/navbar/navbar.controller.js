'use strict';

angular.module('icontentApp')
    .controller('NavbarController', function ($scope, $location, $state, Auth, Principal,  ENV) {
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';
        
		Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
        });

        $scope.logout = function () {
            Auth.logout();
            $state.go('home');
        };
    });
