package com.intralot.domain.util;

import com.intralot.lexicon.model.utils.Properties;
import com.intralot.utils.Period;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.Set;

/**
 * Global dependency domain object used by all systems that
 * need to receive event information from the system. Wraps
 * any type of event.
 * Created by KPentaris on 18-Aug-16.
 */
public class PublishableEvent implements Serializable {

    private static final long serialVersionUID = 6799219269178042788L;

    private String iContentId;
    private Long eventVersion;
    private EventType eventType;
    private ZonedDateTime initialPricingDate;
    private ZonedDateTime kickOff;
    private String eventStatus;
    private SimpleEntry<Integer, String> sport;
    private SimpleEntry<Integer, String> category;
    private SimpleEntry<Integer, String> tournament;
    private Set<EventMarket> markets;

    //****FIXTURE SPECIFIC****//
    private SimpleEntry<Integer, String> fHomeTeam;
    private SimpleEntry<Integer, String> fAwayTeam;
    private Double fInputParameter1;
    private Double fInputParameter2;
    private Double fInputParameter3;
    private boolean fSupportsCorners;
    private Double fCornersLine;
    private Double fOddUnderCorners;
    private Double fOddOverCorners;
    private boolean fSupportsCards;
    private Double fYellowCardsLine;
    private Double fOddUnderYellowCards;
    private Double fOddOverYellowCards;
    private Double fP1180;
    private Double fP2180;
    private Integer fThrowsFirst;
    private Map<Period, EventResult> fYellowCardResults;
    private Map<Period, EventResult> fCornerResults;
    private Map<Period, EventResult> fScoreResults;

    //****ANTEPOST SPECIFIC****//
    private String aDescription;
    private ZonedDateTime aEndDate;

    public String getiContentId() {
        return iContentId;
    }

    public PublishableEvent setiContentId(String iContentId) {
        this.iContentId = iContentId;
        return this;
    }

    public Long getEventVersion() {
        return eventVersion;
    }

    public PublishableEvent setEventVersion(Long eventVersion) {
        this.eventVersion = eventVersion;
        return this;
    }

    public EventType getEventType() {
        return eventType;
    }

    public PublishableEvent setEventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public ZonedDateTime getInitialPricingDate() {
        return initialPricingDate;
    }

    public PublishableEvent setInitialPricingDate(ZonedDateTime activationDate) {
        this.initialPricingDate = activationDate;
        return this;
    }

    public ZonedDateTime getKickOff() {
        return kickOff;
    }

    public PublishableEvent setKickOff(ZonedDateTime kickOff) {
        this.kickOff = kickOff;
        return this;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public PublishableEvent setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
        return this;
    }

    public SimpleEntry<Integer, String> getSport() {
        return sport;
    }

    public PublishableEvent setSport(SimpleEntry<Integer, String> sport) {
        this.sport = sport;
        return this;
    }

    public SimpleEntry<Integer, String> getCategory() {
        return category;
    }

    public PublishableEvent setCategory(SimpleEntry<Integer, String> category) {
        this.category = category;
        return this;
    }

    public SimpleEntry<Integer, String> getTournament() {
        return tournament;
    }

    public PublishableEvent setTournament(SimpleEntry<Integer, String> tournament) {
        this.tournament = tournament;
        return this;
    }

    public Set<EventMarket> getMarkets() {
        return markets;
    }

    public PublishableEvent setMarkets(Set<EventMarket> markets) {
        this.markets = markets;
        return this;
    }

    public SimpleEntry<Integer, String> getfHomeTeam() {
        return fHomeTeam;
    }

    public PublishableEvent setfHomeTeam(SimpleEntry<Integer, String> fHomeTeam) {
        this.fHomeTeam = fHomeTeam;
        return this;
    }

    public SimpleEntry<Integer, String> getfAwayTeam() {
        return fAwayTeam;
    }

    public PublishableEvent setfAwayTeam(SimpleEntry<Integer, String> fAwayTeam) {
        this.fAwayTeam = fAwayTeam;
        return this;
    }

    public Double getfInputParameter1() {
        return fInputParameter1;
    }

    public PublishableEvent setfInputParameter1(Double fInputParameter1) {
        this.fInputParameter1 = fInputParameter1;
        return this;
    }

    public Double getfInputParameter2() {
        return fInputParameter2;
    }

    public PublishableEvent setfInputParameter2(Double fInputParameter2) {
        this.fInputParameter2 = fInputParameter2;
        return this;
    }

    public Double getfInputParameter3() {
        return fInputParameter3;
    }

    public PublishableEvent setfInputParameter3(Double fInputParameter3) {
        this.fInputParameter3 = fInputParameter3;
        return this;
    }

    public boolean isfSupportsCorners() {
        return fSupportsCorners;
    }

    public PublishableEvent setfSupportsCorners(boolean fSupportsCorners) {
        this.fSupportsCorners = fSupportsCorners;
        return this;
    }

    public Double getfCornersLine() {
        return fCornersLine;
    }

    public PublishableEvent setfCornersLine(Double fCornersLine) {
        this.fCornersLine = fCornersLine;
        return this;
    }

    public Double getfOddUnderCorners() {
        return fOddUnderCorners;
    }

    public PublishableEvent setfOddUnderCorners(Double fOddUnderCorners) {
        this.fOddUnderCorners = fOddUnderCorners;
        return this;
    }

    public Double getfOddOverCorners() {
        return fOddOverCorners;
    }

    public PublishableEvent setfOddOverCorners(Double fOddOverCorners) {
        this.fOddOverCorners = fOddOverCorners;
        return this;
    }

    public boolean isfSupportsCards() {
        return fSupportsCards;
    }

    public PublishableEvent setfSupportsCards(boolean fSupportsCards) {
        this.fSupportsCards = fSupportsCards;
        return this;
    }

    public Double getfYellowCardsLine() {
        return fYellowCardsLine;
    }

    public PublishableEvent setfYellowCardsLine(Double fYellowCardsLine) {
        this.fYellowCardsLine = fYellowCardsLine;
        return this;
    }

    public Double getfOddUnderYellowCards() {
        return fOddUnderYellowCards;
    }

    public PublishableEvent setfOddUnderYellowCards(Double fOddUnderYellowCards) {
        this.fOddUnderYellowCards = fOddUnderYellowCards;
        return this;
    }

    public Double getfOddOverYellowCards() {
        return fOddOverYellowCards;
    }

    public PublishableEvent setfOddOverYellowCards(Double fOddOverYellowCards) {
        this.fOddOverYellowCards = fOddOverYellowCards;
        return this;
    }

    public Double getfP1180() {
        return fP1180;
    }

    public PublishableEvent setfP1180(Double fP1180) {
        this.fP1180 = fP1180;
        return this;
    }

    public Double getfP2180() {
        return fP2180;
    }

    public PublishableEvent setfP2180(Double fP2180) {
        this.fP2180 = fP2180;
        return this;
    }

    public int getfThrowsFirst() {
        return fThrowsFirst;
    }

    public PublishableEvent setfThrowsFirst(int fThrowsFirst) {
        this.fThrowsFirst = fThrowsFirst;
        return this;
    }

    public Map<Period, EventResult> getfYellowCardResults() {
        return fYellowCardResults;
    }

    public PublishableEvent setfYellowCardResults(Map<Period, EventResult> fYellowCardResults) {
        this.fYellowCardResults = fYellowCardResults;
        return this;
    }

    public Map<Period, EventResult> getfCornerResults() {
        return fCornerResults;
    }

    public PublishableEvent setfCornerResults(Map<Period, EventResult> fCornerResults) {
        this.fCornerResults = fCornerResults;
        return this;
    }

    public Map<Period, EventResult> getfScoreResults() {
        return fScoreResults;
    }

    public PublishableEvent setfScoreResults(Map<Period, EventResult> fScoreResults) {
        this.fScoreResults = fScoreResults;
        return this;
    }

    public String getaDescription() {
        return aDescription;
    }

    public PublishableEvent setaDescription(String aDescription) {
        this.aDescription = aDescription;
        return this;
    }

    public ZonedDateTime getaEndDate() {
        return aEndDate;
    }

    public PublishableEvent setaEndDate(ZonedDateTime aEndDate) {
        this.aEndDate = aEndDate;
        return this;
    }

    //****INNER CLASSES****//
    public enum EventType {
        antepost,
        rolling
    }

    public static class EventResult {
        private int homeScore;
        private int awayScore;
        private Properties.Side firstScoring;

        public int getHomeScore() {
            return homeScore;
        }

        public EventResult setHomeScore(int homeScore) {
            this.homeScore = homeScore;
            return this;
        }

        public int getAwayScore() {
            return awayScore;
        }

        public EventResult setAwayScore(int awayScore) {
            this.awayScore = awayScore;
            return this;
        }

        public Properties.Side getFirstScoring() {
            return firstScoring;
        }

        public EventResult setFirstScoring(Properties.Side firstScoring) {
            this.firstScoring = firstScoring;
            return this;
        }
    }

    public static class EventMarket {
        private String marketId;
        private String description;
        private double overround;
        private Double variableValue;
        private String marketStatus;
        private Map<String, MarketCode> codes;

        public String getMarketId() {
            return marketId;
        }

        public EventMarket setMarketId(String marketId) {
            this.marketId = marketId;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public EventMarket setDescription(String description) {
            this.description = description;
            return this;
        }

        public double getOverround() {
            return overround;
        }

        public EventMarket setOverround(double overround) {
            this.overround = overround;
            return this;
        }

        public Double getVariableValue() {
            return variableValue;
        }

        public EventMarket setVariableValue(Double variableValue) {
            this.variableValue = variableValue;
            return this;
        }

        public String getMarketStatus() {
            return marketStatus;
        }

        public EventMarket setMarketStatus(String marketStatus) {
            this.marketStatus = marketStatus;
            return this;
        }

        public Map<String, MarketCode> getCodes() {
            return codes;
        }

        public EventMarket setCodes(Map<String, MarketCode> codes) {
            this.codes = codes;
            return this;
        }
    }

    public static class MarketCode {
        private String codeId;
        private String description;
        private double odd;
        private String codeStatus;

        public String getCodeId() {
            return codeId;
        }

        public MarketCode setCodeId(String codeId) {
            this.codeId = codeId;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public MarketCode setDescription(String description) {
            this.description = description;
            return this;
        }

        public double getOdd() {
            return odd;
        }

        public MarketCode setOdd(double odd) {
            this.odd = odd;
            return this;
        }

        public String getCodeStatus() {
            return codeStatus;
        }

        public MarketCode setCodeStatus(String codeStatus) {
            this.codeStatus = codeStatus;
            return this;
        }
    }
}
