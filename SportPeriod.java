package mainTests;


public class SportPeriod {
	
	public enum Period {
		first, second, third, fourth, fifth, sixth, seventh, eigth, ninth, tenth, overtime, penalties, finalScore;
	}
	
	public static void main(String[] args) {
		
		Period[] periods = getPeriodsBySport(15);
		
		for(Period p: periods) {
			System.out.println(p.toString());
		}
		
		System.out.println("_____________________");
		System.out.println(periods[0]);
		
		
	}
	
	private static Period[] getPeriodsBySport(int sportId) {
		
		switch(sportId) {
			case 15: // Bandy
				return new Period[]{
						Period.first, Period.second, Period.finalScore
				};
				break;
			case 3: // Baseball
				return new Period[]{
						Period.finalScore
				};
				break;
			case 2 : // Basketball
				return new Period[]{
						Period.first, Period.second, Period.third, Period.fourth, Period.overtime
						};
				break;
			case 34: // Beach Volley
				return new Period[]{
						Period.first, Period.second, Period.third
				};
				break;
			case 7: // Floorball
				return new Period[]{
						Period.finalScore
				};
				break;
			case 16: // Football
				return new Period[] {
						Period.first, Period.second, Period.third, Period.fourth, Period.overtime
						};
				break;
			case 29: // Futsal
				return new Period[]{
						Period.finalScore
				};
				break;
			case 6: // Handball
				return new Period[]{
						Period.first, Period.second
				};
				break;
			case 4: // Ice Hockey
				return new Period[]{
						Period.first, Period.second, Period.third, Period.overtime, Period.penalties
						};
				break;
			case 1: // Soccer
				return new Period[]{
						Period.first, Period.second
				};
				break;
			case 23: // Volleyball
				return new Period[]{
						Period.first, Period.second, Period.third, Period.fourth, Period.fifth
				};
				break;
			case 26: // Waterpolo
				return new Period[]{
						Period.first, Period.second
				};
				break;
			case 22: // Darts
				return new Period[]{
						Period.finalScore
				};
				break;
			case 9: // Golf
				return new Period[]{
						Period.finalScore
				};
				break;
			case 19: // Snooker
				return new Period[]{
						Period.finalScore
				};
				break;
			case 5: // Tennis
				return new Period[]{
						Period.first, Period.second, Period.third, Period.fourth, Period.fifth
				};
				break;
			default:
				System.out.println("Not supported sport.");
				return null;
				break;
		}
		
	}
}
