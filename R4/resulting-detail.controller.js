(function() {
    'use strict';

    angular
        .module('jsTestGroundApp')
        .controller('ResultingDetailController', ResultingDetailController);

    ResultingDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Resulting'];

    function ResultingDetailController($scope, $rootScope, $stateParams, entity, Resulting) {
        var vm = this;

        vm.resulting = entity;

        var unsubscribe = $rootScope.$on('jsTestGroundApp:resultingUpdate', function(event, result) {
            vm.resulting = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
