/**
 * Data Access Objects used by WebSocket services.
 */
package com.intralot.web.websocket.dto;
