// package com.intralot.jms;
//
// import javax.inject.Inject;
// import javax.jms.ConnectionFactory;
//
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.context.annotation.Bean;
// import org.springframework.jms.annotation.JmsListener;
// import org.springframework.jms.config.JmsListenerContainerFactory;
// import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
// import org.springframework.stereotype.Component;
//
// import com.intralot.config.ApplicationProperties;
// import com.intralot.iflex.commons.provider.betradar.BetradarBetData;
//
// @Component
// public class JMSListener {
// 	private final Logger log = LoggerFactory.getLogger(JMSListener.class);
//
//     @Inject
//     private ApplicationProperties applicationProperties;
//
// 	@Bean
// 	JmsListenerContainerFactory<?> jmsContainerFactory(ConnectionFactory connectionFactory) {
//
// 		SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
// 		factory.setConnectionFactory(connectionFactory);
// 		factory.setClientId(applicationProperties.getJms().getClientId());
// 		factory.setPubSubDomain(applicationProperties.getJms().isPubSubDomain());
// 		factory.setSubscriptionDurable(applicationProperties.getJms().isSubscriptionDurable());
//
// 		return factory;
// 	}
//
// 	@JmsListener(id = "iContent",  destination = "betradarFixtures", containerFactory = "jmsContainerFactory", subscription = "iContent")
// 	public void receiveOrder(Object obj) {
// 		BetradarBetData bbd = (BetradarBetData)obj;
// 		log.info(bbd.toString());
// 	}
//
// }
