package com.intralot.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Application "injected" properties
 *
 * <p>
 * Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "icontent", ignoreUnknownFields = false)
public class ApplicationProperties {

	private final Async async = new Async();

	private final Http http = new Http();

	private final Datasource datasource = new Datasource();

	private final Cache cache = new Cache();

	private final Mail mail = new Mail();

	private final Security security = new Security();

	private final Swagger swagger = new Swagger();

	private final Metrics metrics = new Metrics();

	private final CorsConfiguration cors = new CorsConfiguration();
	
	private final JMS jms = new JMS();
	
	private final LexiconRest lexiconRest = new LexiconRest();
	
	public Async getAsync() {
		return async;
	}

	public Http getHttp() {
		return http;
	}

	public Datasource getDatasource() {
		return datasource;
	}

	public Cache getCache() {
		return cache;
	}

	public Mail getMail() {
		return mail;
	}

	public Security getSecurity() {
		return security;
	}

	public Swagger getSwagger() {
		return swagger;
	}

	public Metrics getMetrics() {
		return metrics;
	}

	public CorsConfiguration getCors() {
		return cors;
	}

	public JMS getJms() {
		return jms;
	}
	
	public LexiconRest getLexiconRest() {
		return lexiconRest;
	}

	public static class LexiconRest {
		private int connectTimeoutInSeconds = 20;
		private int readTimeoutInSeconds 	= 20 ;
		
		private String baseUrl = "http://localhost:8090/lexicon/rest";
		
		private String betradarLang = "betradar";
		
		private String sportByExtIdEndPoint = "/sport/getSportByExtId";
		
		
		public int getConnectTimeoutInSeconds() {
			return connectTimeoutInSeconds;
		}
		public void setConnectTimeoutInSeconds(int connectTimeoutInSeconds) {
			this.connectTimeoutInSeconds = connectTimeoutInSeconds;
		}
		public int getReadTimeoutInSeconds() {
			return readTimeoutInSeconds;
		}
		public void setReadTimeoutInSeconds(int readTimeoutInSeconds) {
			this.readTimeoutInSeconds = readTimeoutInSeconds;
		}
		public String getBaseUrl() {
			return baseUrl;
		}
		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}
		public String getSportByExtIdEndPoint() {
			return sportByExtIdEndPoint;
		}
		public void setSportByExtIdEndPoint(String sportByExtIdEndPoint) {
			this.sportByExtIdEndPoint = sportByExtIdEndPoint;
		}
		public String getBetradarLang() {
			return betradarLang;
		}
		public void setBetradarLang(String betradarLang) {
			this.betradarLang = betradarLang;
		}
		
		
	}

	public static class Async {

		private int corePoolSize = 2;

		private int maxPoolSize = 50;

		private int queueCapacity = 10000;

		public int getCorePoolSize() {
			return corePoolSize;
		}

		public void setCorePoolSize(int corePoolSize) {
			this.corePoolSize = corePoolSize;
		}

		public int getMaxPoolSize() {
			return maxPoolSize;
		}

		public void setMaxPoolSize(int maxPoolSize) {
			this.maxPoolSize = maxPoolSize;
		}

		public int getQueueCapacity() {
			return queueCapacity;
		}

		public void setQueueCapacity(int queueCapacity) {
			this.queueCapacity = queueCapacity;
		}
	}

	public static class Http {

		private final Cache cache = new Cache();

		public Cache getCache() {
			return cache;
		}

		public static class Cache {

			private long timeToLiveInDays = 31L;

			public long getTimeToLiveInDays() {
				return timeToLiveInDays;
			}

			public void setTimeToLiveInDays(long timeToLiveInDays) {
				this.timeToLiveInDays = timeToLiveInDays;
			}
		}
	}

	public static class Datasource {

		private boolean cachePrepStmts = true;

		private int prepStmtCacheSize = 250;

		private int prepStmtCacheSqlLimit = 2048;

		private boolean useServerPrepStmts = true;

		public boolean isCachePrepStmts() {
			return cachePrepStmts;
		}

		public void setCachePrepStmts(boolean cachePrepStmts) {
			this.cachePrepStmts = cachePrepStmts;
		}

		public int getPrepStmtCacheSize() {
			return prepStmtCacheSize;
		}

		public void setPrepStmtCacheSize(int prepStmtCacheSize) {
			this.prepStmtCacheSize = prepStmtCacheSize;
		}

		public int getPrepStmtCacheSqlLimit() {
			return prepStmtCacheSqlLimit;
		}

		public void setPrepStmtCacheSqlLimit(int prepStmtCacheSqlLimit) {
			this.prepStmtCacheSqlLimit = prepStmtCacheSqlLimit;
		}

		public boolean isUseServerPrepStmts() {
			return useServerPrepStmts;
		}

		public void setUseServerPrepStmts(boolean useServerPrepStmts) {
			this.useServerPrepStmts = useServerPrepStmts;
		}
	}

	public static class Cache {

		private int timeToLiveSeconds = 3600;

		public int getTimeToLiveSeconds() {
			return timeToLiveSeconds;
		}

		public void setTimeToLiveSeconds(int timeToLiveSeconds) {
			this.timeToLiveSeconds = timeToLiveSeconds;
		}
	}

	public static class Mail {

		private String from = "icontent@localhost";

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}
	}

	public static class Security {

		private final RememberMe rememberMe = new RememberMe();

		public RememberMe getRememberMe() {
			return rememberMe;
		}

		public static class RememberMe {

			@NotNull
			private String key;

			public String getKey() {
				return key;
			}

			public void setKey(String key) {
				this.key = key;
			}
		}
	}

	public static class Swagger {

		private String title = "icontent API";

		private String description = "icontent API documentation";

		private String version = "0.0.1";

		private String termsOfServiceUrl;
		 private String contactName;

	        private String contactUrl;

	        private String contactEmail;

		private String contact;

		private String license;

		private String licenseUrl;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getTermsOfServiceUrl() {
			return termsOfServiceUrl;
		}

		public void setTermsOfServiceUrl(String termsOfServiceUrl) {
			this.termsOfServiceUrl = termsOfServiceUrl;
		}
		
		public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactUrl() {
            return contactUrl;
        }

        public void setContactUrl(String contactUrl) {
            this.contactUrl = contactUrl;
        }

        public String getContactEmail() {
            return contactEmail;
        }

        public void setContactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
        }

		public String getContact() {
			return contact;
		}

		public void setContact(String contact) {
			this.contact = contact;
		}

		public String getLicense() {
			return license;
		}

		public void setLicense(String license) {
			this.license = license;
		}

		public String getLicenseUrl() {
			return licenseUrl;
		}

		public void setLicenseUrl(String licenseUrl) {
			this.licenseUrl = licenseUrl;
		}
	}

	public static class Metrics {

		private final Jmx jmx = new Jmx();

		private final Spark spark = new Spark();

		private final Graphite graphite = new Graphite();
		private final Logs logs = new Logs();

		public Jmx getJmx() {
			return jmx;
		}

		public Spark getSpark() {
			return spark;
		}

		public Graphite getGraphite() {
			return graphite;
		}
		
		public Logs getLogs() {
            return logs;
        }

		public static class Jmx {

			private boolean enabled = true;

			public boolean isEnabled() {
				return enabled;
			}

			public void setEnabled(boolean enabled) {
				this.enabled = enabled;
			}
		}

		public static class Spark {

			private boolean enabled = false;

			private String host = "localhost";

			private int port = 9999;

			public boolean isEnabled() {
				return enabled;
			}

			public void setEnabled(boolean enabled) {
				this.enabled = enabled;
			}

			public String getHost() {
				return host;
			}

			public void setHost(String host) {
				this.host = host;
			}

			public int getPort() {
				return port;
			}

			public void setPort(int port) {
				this.port = port;
			}
		}

		public static class Graphite {

			private boolean enabled = false;

			private String host = "localhost";

			private int port = 2003;

			private String prefix = "icontent";

			public boolean isEnabled() {
				return enabled;
			}

			public void setEnabled(boolean enabled) {
				this.enabled = enabled;
			}

			public String getHost() {
				return host;
			}

			public void setHost(String host) {
				this.host = host;
			}

			public int getPort() {
				return port;
			}

			public void setPort(int port) {
				this.port = port;
			}

			public String getPrefix() {
				return prefix;
			}

			public void setPrefix(String prefix) {
				this.prefix = prefix;
			}
		}
		
		public static  class Logs {

            private boolean enabled = false;

            private long reportFrequency = 60;

            public long getReportFrequency() {
                return reportFrequency;
            }

            public void setReportFrequency(int reportFrequency) {
                this.reportFrequency = reportFrequency;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
        }

	}

	public static class JMS {
		private boolean pubSubDomain = false;
		private boolean subscriptionDurable = false;
		private String clientId = "";

		public boolean isPubSubDomain() {
			return pubSubDomain;
		}

		public void setPubSubDomain(boolean pubSubDomain) {
			this.pubSubDomain = pubSubDomain;
		}

		public boolean isSubscriptionDurable() {
			return subscriptionDurable;
		}

		public void setSubscriptionDurable(boolean subscriptionDurable) {
			this.subscriptionDurable = subscriptionDurable;
		}

		public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

	}
	
	private final Logging logging = new Logging();

    public Logging getLogging() { return logging; }

    public static class Logging {

        private final Logstash logstash = new Logstash();

        public Logstash getLogstash() { return logstash; }

        public static class Logstash {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 5000;

            private int queueSize = 512;

            public boolean isEnabled() { return enabled; }

            public void setEnabled(boolean enabled) { this.enabled = enabled; }

            public String getHost() { return host; }

            public void setHost(String host) { this.host = host; }

            public int getPort() { return port; }

            public void setPort(int port) { this.port = port; }

            public int getQueueSize() { return queueSize; }

            public void setQueueSize(int queueSize) { this.queueSize = queueSize; }
        }
    }
}
