(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('FixtureDialogController', FixtureDialogController);

    FixtureDialogController.$inject = ['$scope', '$rootScope', '$stateParams', '$uibModalInstance', 'entity','Fixture'];

    function FixtureDialogController ($scope, $rootScope, $stateParams, $uibModalInstance, entity, Fixture) {
        var vm = this;
        var array = [];
        var dataChanged = [];
        vm.load = load;
        vm.eventListing = entity;
        vm.load();
 
        function load() { 
            var data = ($rootScope.listScope.length > 0) ? $rootScope.listScope:$rootScope.changedData;
            if($rootScope.changedData.length == 0){
                angular.forEach($rootScope.listScope, function (item) { 
                    if (item.Selected) {
                        array.push(item);
                    }
                });
            }else {
                angular.forEach($rootScope.changedData, function (item) { 
                    dataChanged.push(item); 
                });
                angular.forEach($rootScope.listScope, function (item, key) {  
                    angular.forEach($rootScope.changedData, function (changedItem) {  
                        if(item.id==changedItem.id) {
                            array.push(item); 
                        }
                    });
                }); 
                vm.changedData=dataChanged;
            }
 
        }

        var onSaveSuccess = function (result) {
            $scope.$emit('fixtureApp:fixture  Update', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {  
            if (array.length != 0) {
                angular.forEach(array, function (item,key) { 
                    if($rootScope.changedData.length == 0){
                        if (item.Selected) {
                            item.template = vm.fixtures.text;
                            item.oddFd    = vm.fixtures.oddFd;
                            item.dataFd   = vm.fixtures.dataFd;
                            item.status   = vm.fixtures.status
                            Fixture.update(array[key], onSaveSuccess, onSaveError);
                        }
                    }else { 
                        Fixture.update(array[key], onSaveSuccess, onSaveError);
                    }
                });
            } else {
                alert("Please select items to update first.");
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };



    }

})();
