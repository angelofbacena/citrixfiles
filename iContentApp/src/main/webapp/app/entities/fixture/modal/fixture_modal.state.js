(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fixture-modal', {
            parent: 'fixture-listing',
            url: '/action',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fixture/modal/fixture_modal.html',
                    controller: 'FixtureDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: '',
                    resolve: {
                        entity: function () {
                            return {
                                box: false,
                                event: null,
                                homeTeam: null,
                                awayTeam: null,
                                tournament: null,
                                kickOff: null,
                                initialPricingDate: null,
                                status: null,
                                inputParameters1: null,
                                inputParameters2: null,
                                bFinalResult: null,
                                bUnderOver: null,
                                oddFd: null,
                                dataFd: null,
                                template: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('fixture-listing', null, { reload: false });
                }, function() {
                    $state.go('fixture-listing');
                });
            }]
        });
    }

})();
