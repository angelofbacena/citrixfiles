(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('FixtureCreatorController', FixtureCreatorController);

    FixtureCreatorController.$inject = ['$scope', '$state', '$stateParams', 'fixture', 'Fixture', 'AlertService', 'lexiconSports', 'lexiconTeams'];

    function FixtureCreatorController ($scope, $state, $stateParams, fixture, Fixture, AlertService, lexiconSports, lexiconTeams) {
        var vm = this;
        vm.fixture = fixture;
        vm.lexiconSports = lexiconSports;
        vm.lexiconTeams = lexiconTeams;
        vm.fetchCategory = fetchCategory;
        vm.fetchTournament = fetchTournament;
        vm.fetchTemplate = fetchTemplate;
        vm.validationTeam = validationTeam;
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.kickOff = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };

        function fetchCategory(sport){
            Fixture.fetchCategory(null,fetchCategorySuccess,fetchCategoryError);
            function fetchCategorySuccess(data, headers) {
                vm.fixture.category = null;
                vm.fixture.tournament = null;
                vm.fixture.template = null;
                vm.lexiconSportsCategory = data.response;
            }
            function fetchCategoryError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function fetchTournament(category){
            Fixture.fetchTournament({category:category},fetchTournamentSuccess,fetchTournamentError);
            function fetchTournamentSuccess(data, headers) {
                vm.fixture.tournament = null;
                vm.fixture.template = null;
                vm.tournamentList = data.response;
            }
            function fetchTournamentError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function fetchTemplate(tournament){
            Fixture.fetchTemplate({tournament:tournament},fetchTemplateSuccess,fetchTemplateError);
            function fetchTemplateSuccess(data, headers) {
                vm.fixture.template = null;
                vm.templateList = data.response;
            }
            function fetchTemplateError(error) {
                AlertService.error(error.data.message);
            }            
        }

        function validationTeam(type,value){
            if (type == "home") {
                if (vm.fixture.home == vm.fixture.away) {
                    vm.fixture.away = null;
                }

                if (vm.lexiconTeams.indexOf(value) < 0) {
                    $scope.editForm.home.$invalid = true;
                    $scope.editForm.$invalid = true;
                } else {
                    $scope.editForm.home.$invalid = false;
                }
            } else if (type == "away") {
                if (vm.fixture.home == vm.fixture.away) {
                    vm.fixture.home = null;
                }

                if (vm.lexiconTeams.indexOf(value) < 0) {
                    $scope.editForm.away.$invalid = true;
                    $scope.editForm.$invalid = true;
                } else {
                    $scope.editForm.away.$invalid = false;
                }
            }    
        }

        var onSaveSuccess = function (result) {
            vm.isSaving = false;
            vm.fixture = {
                kickOff: new Date(),
                sports: null,
                category: null,
                tournament: null,
                template: null,
                home: null,
                away: null,
                id: null
            }
            $scope.editForm.home.$pristine = true;
            $scope.editForm.away.$pristine = true;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.saveFixture = function () {
            vm.isSaving = true;
            if (vm.fixture.id !== null) {
                Fixture.update(vm.fixture, onSaveSuccess, onSaveError);
            } else {
                Fixture.save(vm.fixture, onSaveSuccess, onSaveError);
            }
        };

    }
})();
