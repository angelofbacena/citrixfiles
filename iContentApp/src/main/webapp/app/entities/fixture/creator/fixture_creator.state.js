(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fixture-new', {
            parent: 'fixture',
            url: '/fixture/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Create Fixture'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fixture/creator/fixture_creator.html',
                    controller: 'FixtureCreatorController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                lexiconSports: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchSports({});
                    }
                ],
                lexiconTeams: ['Fixture',
                    function(Fixture) {
                        return Fixture.fetchTeams({});
                    }
                ],
                fixture: function () {
                    return {
                        kickOff: new Date(),
                        sports: null,
                        category: null,
                        tournament: null,
                        template: null,
                        home: null,
                        away: null,
                        id: null
                    };
                }
            }
        });
    }

})();
