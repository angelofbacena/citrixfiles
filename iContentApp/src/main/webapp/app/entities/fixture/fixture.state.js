(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('fixture', {
            abstract: true,
            parent: 'entity'
        });
    }
})();
