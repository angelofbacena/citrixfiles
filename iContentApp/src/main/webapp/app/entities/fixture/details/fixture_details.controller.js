(function() {
    'use strict';

    angular
        .module('icontentApp')
        .controller('FixtureDetailsController', FixtureDetailsController);

    FixtureDetailsController.$inject = ['$scope', '$stateParams', 'Fixture'];

    function FixtureDetailsController ($scope, $stateParams, Fixture) {
        var vm = this;
        vm.fixture = {};
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.kickOff = false;
        vm.isEditMode = false;
        vm.status='Active';
        vm.OddFd='oddFd';
        vm.dataFd='dataFd';
        vm.status_new = vm.status;
        vm.dataFd_new = vm.dataFd;
        vm.OddFd_new = vm.OddFd;
        vm.kickOff_new = "";

        vm.loadFixture = loadFixture;
        vm.loadFixture();
        vm.kickOff_new = vm.fixture.kickOff;

        vm.toggleEditMode = function() {
            if(vm.isEditMode == false) {
                vm.isEditMode = true;
            }
            else if (vm.isEditMode == true) {
                vm.isEditMode = false;
            }
            vm.kickOff_new = vm.fixture.kickOff;
            vm.dataFd_new = vm.dataFd;
            vm.OddFd_new = vm.OddFd;
            vm.status_new=vm.status;

        };

        //Functions
        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };


        vm.updateFixture = function () {
            vm.isSaving = true;    
            if (vm.fixture.id !== null) {
                vm.fixture.kickOff = vm.kickOff_new;
                Fixture.update(vm.fixture,onUpdateSuccess,onUpdateError);
                vm.dataFd = vm.dataFd_new ;
                vm.OddFd = vm.OddFd_new ;
                vm.status = vm.status_new ;
            }   
        };

        function loadFixture() {
            Fixture.get({id: $stateParams.id}, function(result) {
                vm.fixture = result;
            });
        }
        
        var onUpdateError = function () {
            vm.isSaving = false;
        };

        var onUpdateSuccess = function () {
            vm.isSaving = false;
            vm.isEditMode = false;
        };
    }
})();
