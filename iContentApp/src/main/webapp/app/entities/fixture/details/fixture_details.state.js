(function() {
    'use strict';

    angular
        .module('icontentApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fixture-details', {
            parent: 'fixture',
            url: '/fixture-details/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Fixture Details'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fixture/details/fixture_details.html',
                    controller: 'FixtureDetailsController',
                    controllerAs: 'vm'
                }
            }
        })
    }

})();
