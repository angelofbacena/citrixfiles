(function() {
    'use strict';
    angular
        .module('icontentApp')
        .factory('Fixture', Fixture);

    Fixture.$inject = ['$resource','$http', 'DateUtils'];

    function Fixture ($resource,$http,DateUtils) {
        var resourceUrl =  'api/fixtures/:id';

        return $resource(resourceUrl, {}, {
        	'query': { 
        		method: 'GET', 
        	    url: 'app/entities/dummy_data/fixture.json',
        	    isArray: true,
        	    transformResponse: function (data) { 
        	        data = angular.fromJson(data); 
        	        data.kickOff = DateUtils.convertDateTimeFromServer(data.kickOff);
        	        return data;
        	    }
        	},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.kickOff = DateUtils.convertDateTimeFromServer(data.kickOff);
                    return data;
                }
            },
            'update': { method:'PUT' },
            'fetchSports': {
                method: 'GET',
                url: 'app/entities/dummy_data/sports.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'fetchCategory': {
                method: 'GET',
                url: 'app/entities/dummy_data/categories.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'fetchTemplate': {
                method: 'GET',
                url: 'app/entities/dummy_data/templates.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'fetchTournament': {
                method: 'GET',
                url: 'app/entities/dummy_data/tournaments.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'fetchTeams': {
                method: 'GET',
                url: 'app/entities/dummy_data/teams.json',
                isArray: false,
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            }
        });
    }
})();
