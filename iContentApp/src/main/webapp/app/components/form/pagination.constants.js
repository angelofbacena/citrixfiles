(function() {
    'use strict';

    angular
        .module('icontentApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20,
            'filterStatus': null,
            'dateRange': {
            	startDate: null,
            	endDate: null
            },
            'filterSports': null,
            'filterTournament': null
        });
})();
