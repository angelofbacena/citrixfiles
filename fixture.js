'use strict';

describe('Fixture e2e test', function () {

    var username = element(by.id('username'));
    var password = element(by.id('password'));
    var entityMenu = element(by.id('entity-menu'));
    var accountMenu = element(by.id('account-menu'));
    var login = element(by.id('login'));
    var logout = element(by.id('logout'));
	
	var loginAction = function () {
		accountMenu.click();
		login.click();
		
		username.sendKeys('testuser');
		password.sendKeys('1234abc');
		element(by.css('button[type=submit]')).click();
	};

    beforeAll(function () {
        browser.get('/');
        browser.driver.wait(protractor.until.elementIsVisible(element(by.css('h1'))));

        accountMenu.click();
        login.click();

        username.sendKeys('testuser');
        password.sendKeys('1234abc');
        element(by.css('button[type=submit]')).click();
    });

    //Fixture Create
	it('should open the create fixture page', function() {
		element(by.id('entity-menu')).click();
        element(by.css('[ui-sref="fixture-new"]')).click();
		
		var pageTitle = element(by.id('myFixtureLabel')).getText();
		
		expect(pageTitle).toEqual("Create or edit a Fixture");
	});
	
	it('should check if fields are disabled', function() 
		expect(element(by.id('field_sports')).isEnabled()).toBe(true);
		expect(element(by.id('field_category')).isEnabled()).toBe(false);
		expect(element(by.css('button[type=submit]')).isEnabled()).toBe(false);
	});
	
    it('should save a Fixture', function () {		
		element(by.id('field_sports')).element(by.cssContainingText('option', 'Alabama')).click();
		element(by.id('field_category')).element(by.cssContainingText('option', 'Alabama')).click();
		element(by.id('field_tournament')).element(by.cssContainingText('option', 'Alabama')).click();
		element(by.id('field_template')).element(by.cssContainingText('option', 'Alabama')).click();
		element(by.id('field_kickOff')).clear().sendKeys('2016-05-02 17:23');
		element(by.id('field_home')).sendKeys('bahay');
		element(by.id('field_away')).sendKeys('lakwatsa');
		element(by.css('button[type=submit]')).click();
		
		var alertMsg = element(by.model('ctrl.alerts')).last().getText();
		
		expect(alertMsg).toContain('created');
		
    });
	
	//Listing
	it('should load the fixture listing page', function() {
		entityMenu.click();
		element(by.css('[ui-sref="fixture-listing"]')).click();
		
		var pageHeading = element(by.css('h2')).getText();
		
		expect(pageHeading).toContain("Fixture Listings");
	});
	
	it('should filter the listing by status', function() {
		element(by.css('[ng-click="isCollapsed = isCollapsed ? false : true"]')).click();
		element(by.id('filter_status')).sendKeys('Active');
		element(by.css('button[type=submit]')).click();
		
		var filterStatus = element(by.id('field_sports'));
		
		expect(filterStatus.getText()).toContain('Active');
	});
	
	it('should filter the listing by date range', function() {
		element(by.css('input[ng-model="vm.datePicker"]')).clear().sendKeys('2016-05-01 - 2016-06-30'));
		element(by.css('button[type=submit]')).click();
		
		var filterDate = element(by.css('input[ng-model="vm.datePicker"]'));
		
		expect(filterDate.getText()).toContain('2016-05-01 - 2016-06-30');
	});
	
	it('should filter the listing by sport', function() {
		element(by.id('field_sports')).element(by.cssContainingText('option', 'Alabama')).click();
		element(by.css('button[type=submit]')).click();
		
		var filterSport = element(by.id('field_sports'));
		
		expect(filterSport.getText()).toContain('Alabama');
	});
	
	it('should filter the listing by tournament', function() {
		element(by.css('input[ng-model="vm.datePicker"]')).clear();
		element(by.id('field_tournament')).element(by.cssContainingText('option', 'Alabama')).click()
		
		var filterTournament = element(by.id('field_tournament'));
		
		expect(filterTournament.getText()).toContain('Alabama');
	});

	
	it('should list fixtures', function() {
		element(by.css('button[type=submit]')).click();
		element(by.css('[ui-sref-="home"]')).click();
		entityMenu.click();
		element(by.css('[ui-sref="fixture-listing"]')).click();
		
		expect(fixturesList.first().getText()).toContain('Arkansas');
	});
	
	//Fixture Detail
	it('should open fixture detail', function() {
		element(by.css('a[href="#/fixture-details/5718edd48ef4cfa9996c043e"]')).click();
		
		var detailHeading = element(by.css('h2[class="ng-binding"]'));
		
		expect(detailHeading.getText()).toEqual('Fixture');
	});
	

    afterAll(function () {
        accountMenu.click();
        logout.click();
    });
});
